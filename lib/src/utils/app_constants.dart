class AppConstants {
  static const String appName = 'Freshup';
  static const String currencySymbol = '€';
  static const String currency = 'EUR';

  /// all currency are not supported by stripe and paypal
  static const String stripeCurrency = 'EUR';
  static const String paypalCurrency = 'EUR';

  static const String stripePublishableKey = 'Your_Publishable_Key';
  static const String stripeSecretKey = 'Your_Secret_Key';

  static const String paypalClientId = 'Your_Client_Id';
  static const String paypalSecretKey = 'Your_Secret_Key';

}
