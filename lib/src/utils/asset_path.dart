class AssetPath{

  /// images
  static const String barber = 'assets/images/barber.png';
  static const String customer = 'assets/images/customer.png';
  static const String placeholder = 'assets/images/placeholder.png';
  static const String people = 'assets/images/people.png';
  static const String logo = 'assets/images/logo.png';
  static const String noDataFound = 'assets/images/no_data_found.jpg';
  static const String barberShopLoginBg = 'assets/images/barber_shop_login_bg.png';

  /// icons
  static const String facebook = 'assets/icons/facebook.png';
  static const String instagram = 'assets/icons/instagram.png';
  static const String website = 'assets/icons/website.png';
  static const String share = 'assets/icons/share.png';
  static const String franceFlag = 'assets/icons/flag_france.png';
  static const String usaFlag = 'assets/icons/flag_united-states.png';
  static const String googleIcon = 'assets/icons/google.png';
  static const String paypalIcon = 'assets/icons/paypal.png';
  static const String stripeIcon = 'assets/icons/stripe.png';


  /// lottie
  static const String confirm = 'assets/lottie/confirm.json';

}