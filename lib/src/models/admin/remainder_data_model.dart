class RemainderData {
  List<Appointments>? appointments;
  String? message;

  RemainderData({this.appointments, this.message});

  RemainderData.fromJson(Map<String, dynamic> json) {
    if (json['appointments'] != null) {
      appointments = <Appointments>[];
      json['appointments'].forEach((v) {
        appointments!.add(Appointments.fromJson(v));
      });
    }
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (appointments != null) {
      data['appointments'] = appointments!.map((v) => v.toJson()).toList();
    }
    data['message'] = message;
    return data;
  }
}

class Appointments {
  int? id;
  int? timeSlotId;
  int? serviceId;
  int? barberId;
  int? customerId;
  String? date;
  String? status;
  TimeSlot? timeSlot;
  Barber? barber;
  Service? service;
  Customer? customer;

  Appointments(
      {this.id,
        this.timeSlotId,
        this.serviceId,
        this.barberId,
        this.customerId,
        this.date,
        this.status,
        this.timeSlot,
        this.barber,
        this.service,
        this.customer});

  Appointments.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    timeSlotId = json['time_slot_id'];
    serviceId = json['service_id'];
    barberId = json['barber_id'];
    customerId = json['customer_id'];
    date = json['date'];
    status = json['status'];
    timeSlot = json['time_slot'] != null
        ? TimeSlot.fromJson(json['time_slot'])
        : null;
    barber =
    json['barber'] != null ? Barber.fromJson(json['barber']) : null;
    service =
    json['service'] != null ? Service.fromJson(json['service']) : null;
    customer = json['customer'] != null
        ? Customer.fromJson(json['customer'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['time_slot_id'] = timeSlotId;
    data['service_id'] = serviceId;
    data['barber_id'] = barberId;
    data['customer_id'] = customerId;
    data['date'] = date;
    data['status'] = status;
    if (timeSlot != null) {
      data['time_slot'] = timeSlot!.toJson();
    }
    if (barber != null) {
      data['barber'] = barber!.toJson();
    }
    if (service != null) {
      data['service'] = service!.toJson();
    }
    if (customer != null) {
      data['customer'] = customer!.toJson();
    }
    return data;
  }
}

class TimeSlot {
  int? id;
  String? start;
  String? end;

  TimeSlot({this.id, this.start, this.end});

  TimeSlot.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    start = json['start'];
    end = json['end'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['start'] = start;
    data['end'] = end;
    return data;
  }
}

class Barber {
  int? id;
  String? name;
  String? avatar;

  Barber({this.id, this.name, this.avatar});

  Barber.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    avatar = json['avatar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['avatar'] = avatar;
    return data;
  }
}

class Service {
  int? id;
  String? title;
  int? price;

  Service({this.id, this.title, this.price});

  Service.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    price = json['price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['price'] = price;
    return data;
  }
}

class Customer {
  int? id;
  String? name;
  String? avatar;
  String? phone;

  Customer({this.id, this.name, this.avatar, this.phone});

  Customer.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    avatar = json['avatar'];
    phone = json['phone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['avatar'] = avatar;
    data['phone'] = phone;
    return data;
  }
}
