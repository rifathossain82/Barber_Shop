import 'package:barber_shop/src/models/common/service_data_model.dart';

/// i used this model for barber and admin too

class BarberAppointmentData {
  List<AppointmentSchedules>? appointmentSchedules;
  String? message;

  BarberAppointmentData({this.appointmentSchedules, this.message});

  BarberAppointmentData.fromJson(Map<String, dynamic> json) {
    if (json['appointmentSchedules'] != null) {
      appointmentSchedules = <AppointmentSchedules>[];
      json['appointmentSchedules'].forEach((v) {
        appointmentSchedules!.add(AppointmentSchedules.fromJson(v));
      });
    }
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (appointmentSchedules != null) {
      data['appointmentSchedules'] =
          appointmentSchedules!.map((v) => v.toJson()).toList();
    }
    data['message'] = message;
    return data;
  }
}

class AppointmentSchedules {
  int? id;
  String? start;
  String? end;
  Appointment? appointment;

  AppointmentSchedules({this.id, this.start, this.end, this.appointment});

  AppointmentSchedules.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    start = json['start'];
    end = json['end'];
    appointment = json['appointment'] != null
        ? Appointment.fromJson(json['appointment'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['start'] = start;
    data['end'] = end;
    if (appointment != null) {
      data['appointment'] = appointment!.toJson();
    }
    return data;
  }
}

class Appointment {
  int? id;
  int? timeSlotId;
  int? serviceId;
  int? barberId;
  int? customerId;
  String? date;
  String? status;
  String? createdAt;
  String? updatedAt;
  num? serviceCharge;
  num? tipAmount;
  num? taxAmount;
  num? total;
  num? paid;
  num? dueAmount;
  String? paymentMethod;
  dynamic tnxId;
  Customer? customer;
  ServiceData? service;

  Appointment(
      {this.id,
        this.timeSlotId,
        this.serviceId,
        this.barberId,
        this.customerId,
        this.date,
        this.status,
        this.createdAt,
        this.updatedAt,
        this.serviceCharge,
        this.tipAmount,
        this.taxAmount,
        this.total,
        this.paid,
        this.dueAmount,
        this.paymentMethod,
        this.tnxId,
        this.customer,
        this.service});

  Appointment.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    timeSlotId = json['time_slot_id'];
    serviceId = json['service_id'];
    barberId = json['barber_id'];
    customerId = json['customer_id'];
    date = json['date'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    serviceCharge = json['service_charge'];
    tipAmount = json['tip_amount'];
    taxAmount = json['tax_amount'];
    total = json['total'];
    paid = json['paid'];
    dueAmount = json['due_amount'];
    paymentMethod = json['payment_method'];
    tnxId = json['tnx_id'];
    customer = json['customer'] != null
        ? Customer.fromJson(json['customer'])
        : null;
    service =
    json['service'] != null ? ServiceData.fromJson(json['service']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['time_slot_id'] = timeSlotId;
    data['service_id'] = serviceId;
    data['barber_id'] = barberId;
    data['customer_id'] = customerId;
    data['date'] = date;
    data['status'] = status;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['service_charge'] = serviceCharge;
    data['tip_amount'] = tipAmount;
    data['tax_amount'] = taxAmount;
    data['total'] = total;
    data['paid'] = paid;
    data['due_amount'] = dueAmount;
    data['payment_method'] = paymentMethod;
    data['tnx_id'] = tnxId;
    if (customer != null) {
      data['customer'] = customer!.toJson();
    }
    if (service != null) {
      data['service'] = service!.toJson();
    }
    return data;
  }
}

class Customer {
  int? id;
  String? name;
  String? avatar;

  Customer({this.id, this.name, this.avatar});

  Customer.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    avatar = json['avatar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['avatar'] = avatar;
    return data;
  }
}
