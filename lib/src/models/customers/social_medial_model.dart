class SocialMedia{
  final String name;
  final String link;
  final String assetIconPath;

  SocialMedia({required this.name, required this.link, required this.assetIconPath,});
}