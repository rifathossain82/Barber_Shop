class TimeSlotData {
  List<TimeSlots>? timeSlots;
  String? message;
  int? status;

  TimeSlotData({this.timeSlots, this.message, this.status});

  TimeSlotData.fromJson(Map<String, dynamic> json) {
    if (json['time_slots'] != null) {
      timeSlots = <TimeSlots>[];
      json['time_slots'].forEach((v) {
        timeSlots!.add(TimeSlots.fromJson(v));
      });
    }
    message = json['message'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (timeSlots != null) {
      data['time_slots'] = timeSlots!.map((v) => v.toJson()).toList();
    }
    data['message'] = message;
    data['status'] = status;
    return data;
  }
}

class TimeSlots {
  int? id;
  String? start;
  String? end;

  TimeSlots({this.id, this.start, this.end});

  TimeSlots.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    start = json['start'];
    end = json['end'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['start'] = start;
    data['end'] = end;
    return data;
  }
}
