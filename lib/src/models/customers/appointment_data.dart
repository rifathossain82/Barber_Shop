import 'package:barber_shop/src/models/common/service_data_model.dart';
import 'package:barber_shop/src/models/customers/time_slot_data.dart';

class AppointmentData {
  int? id;
  int? timeSlotId;
  int? serviceId;
  int? barberId;
  int? customerId;
  String? date;
  String? status;
  num? serviceCharge;
  num? tipAmount;
  num? taxAmount;
  num? total;
  num? paid;
  num? dueAmount;
  String? paymentMethod;
  dynamic tnxId;
  TimeSlots? timeSlot;
  AppointmentsBarber? barber;
  ServiceData? service;

  AppointmentData(
      {this.id,
        this.timeSlotId,
        this.serviceId,
        this.barberId,
        this.customerId,
        this.date,
        this.status,
        this.serviceCharge,
        this.tipAmount,
        this.taxAmount,
        this.total,
        this.paid,
        this.dueAmount,
        this.paymentMethod,
        this.tnxId,
        this.timeSlot,
        this.barber,
        this.service});

  AppointmentData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    timeSlotId = json['time_slot_id'];
    serviceId = json['service_id'];
    barberId = json['barber_id'];
    customerId = json['customer_id'];
    date = json['date'];
    status = json['status'];
    serviceCharge = json['service_charge'];
    tipAmount = json['tip_amount'];
    taxAmount = json['tax_amount'];
    total = json['total'];
    paid = json['paid'];
    dueAmount = json['due_amount'];
    paymentMethod = json['payment_method'];
    tnxId = json['tnx_id'];
    timeSlot = json['time_slot'] != null
        ? TimeSlots.fromJson(json['time_slot'])
        : null;
    barber =
    json['barber'] != null ? AppointmentsBarber.fromJson(json['barber']) : null;
    service =
    json['service'] != null ? ServiceData.fromJson(json['service']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['time_slot_id'] = timeSlotId;
    data['service_id'] = serviceId;
    data['barber_id'] = barberId;
    data['customer_id'] = customerId;
    data['date'] = date;
    data['status'] = status;
    data['service_charge'] = serviceCharge;
    data['tip_amount'] = tipAmount;
    data['tax_amount'] = taxAmount;
    data['total'] = total;
    data['paid'] = paid;
    data['due_amount'] = dueAmount;
    data['payment_method'] = paymentMethod;
    data['tnx_id'] = tnxId;
    if (timeSlot != null) {
      data['time_slot'] = timeSlot!.toJson();
    }
    if (barber != null) {
      data['barber'] = barber!.toJson();
    }
    if (service != null) {
      data['service'] = service!.toJson();
    }
    return data;
  }
}

class AppointmentsBarber {
  int? id;
  String? name;
  String? avatar;

  AppointmentsBarber({this.id, this.name, this.avatar});

  AppointmentsBarber.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    avatar = json['avatar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['avatar'] = avatar;
    return data;
  }
}