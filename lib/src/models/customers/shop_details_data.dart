class ShopDetailsData {
  Info? info;
  List<ShopTimeSlots>? timeSlots;
  Facilities? facilities;
  Links? links;
  String? message;
  int? status;

  ShopDetailsData(
      {this.info,
        this.timeSlots,
        this.facilities,
        this.links,
        this.message,
        this.status});

  ShopDetailsData.fromJson(Map<String, dynamic> json) {
    info = json['info'] != null ? Info.fromJson(json['info']) : null;
    if (json['time_slots'] != null) {
      timeSlots = <ShopTimeSlots>[];
      json['time_slots'].forEach((v) {
        timeSlots!.add(ShopTimeSlots.fromJson(v));
      });
    }
    facilities = json['facilities'] != null
        ? Facilities.fromJson(json['facilities'])
        : null;
    links = json['links'] != null ? Links.fromJson(json['links']) : null;
    message = json['message'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (info != null) {
      data['info'] = info!.toJson();
    }
    if (timeSlots != null) {
      data['time_slots'] = timeSlots!.map((v) => v.toJson()).toList();
    }
    if (facilities != null) {
      data['facilities'] = facilities!.toJson();
    }
    if (links != null) {
      data['links'] = links!.toJson();
    }
    data['message'] = message;
    data['status'] = status;
    return data;
  }
}

class Info {
  String? logo;
  String? shopName;
  String? location;
  String? banner;
  String? phone;

  Info({this.logo, this.shopName, this.location, this.banner, this.phone});

  Info.fromJson(Map<String, dynamic> json) {
    logo = json['logo'];
    shopName = json['shop_name'];
    location = json['location'];
    banner = json['banner'];
    phone = json['phone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['logo'] = logo;
    data['shop_name'] = shopName;
    data['location'] = location;
    data['banner'] = banner;
    data['phone'] = phone;
    return data;
  }
}

class ShopTimeSlots {
  String? day;
  String? startTime;
  String? endTime;

  ShopTimeSlots({this.day, this.startTime, this.endTime});

  ShopTimeSlots.fromJson(Map<String, dynamic> json) {
    day = json['day'];
    startTime = json['start_time'];
    endTime = json['end_time'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['day'] = day;
    data['start_time'] = startTime;
    data['end_time'] = endTime;
    return data;
  }
}

class Facilities {
  bool? parkingSpace;
  bool? wifi;
  bool? acceptCreditCard;
  bool? accessible;

  Facilities(
      {this.parkingSpace, this.wifi, this.acceptCreditCard, this.accessible});

  Facilities.fromJson(Map<String, dynamic> json) {
    parkingSpace = json['parking_space'] ?? false;
    wifi = json['wifi'] ?? false;
    acceptCreditCard = json['accept_credit_card'] ?? false;
    accessible = json['accessible'] ?? false;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['parking_space'] = parkingSpace;
    data['wifi'] = wifi;
    data['accept_credit_card'] = acceptCreditCard;
    data['accessible'] = accessible;
    return data;
  }
}

class Links {
  String? facebookLink;
  String? instagramLink;
  String? websiteLink;
  String? appLink;

  Links(
      {this.facebookLink, this.instagramLink, this.websiteLink, this.appLink});

  Links.fromJson(Map<String, dynamic> json) {
    facebookLink = json['facebook_link'] ?? "";
    instagramLink = json['instagram_link'] ?? "";
    websiteLink = json['website_link'] ?? "";
    appLink = json['app_link'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['facebook_link'] = facebookLink;
    data['instagram_link'] = instagramLink;
    data['website_link'] = websiteLink;
    data['app_link'] = appLink;
    return data;
  }
}
