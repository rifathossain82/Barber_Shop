class CustomerData {
  int? id;
  String? name;
  String? email;
  String? userType;
  String? phone;
  int? status;
  String? createdAt;
  String? updatedAt;
  String? otp;
  String? avatar;

  CustomerData(
      {this.id,
        this.name,
        this.email,
        this.userType,
        this.phone,
        this.status,
        this.createdAt,
        this.updatedAt,
        this.otp,
        this.avatar});

  CustomerData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    userType = json['user_type'];
    phone = json['phone'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    otp = json['otp'];
    avatar = json['avatar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['email'] = email;
    data['user_type'] = userType;
    data['phone'] = phone;
    data['status'] = status;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['otp'] = otp;
    data['avatar'] = avatar;
    return data;
  }
}