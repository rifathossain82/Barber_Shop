class BarberData {
  List<Barber>? barbers;
  String? message;

  BarberData({this.barbers, this.message});

  BarberData.fromJson(Map<String, dynamic> json) {
    if (json['barbers'] != null) {
      barbers = <Barber>[];
      json['barbers'].forEach((v) {
        barbers!.add(Barber.fromJson(v));
      });
    }
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (barbers != null) {
      data['barbers'] = barbers!.map((v) => v.toJson()).toList();
    }
    data['message'] = message;
    return data;
  }
}

class Barber {
  int? id;
  String? name;
  String? email;
  String? userType;
  String? phone;
  int? status;
  String? avatar;

  Barber(
      {this.id,
        this.name,
        this.email,
        this.userType,
        this.phone,
        this.status,
        this.avatar});

  Barber.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'] ?? "";
    email = json['email'];
    userType = json['user_type'];
    phone = json['phone'] ?? "";
    status = json['status'];
    avatar = json['avatar'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['email'] = email;
    data['user_type'] = userType;
    data['phone'] = phone;
    data['status'] = status;
    data['avatar'] = avatar;
    return data;
  }
}
