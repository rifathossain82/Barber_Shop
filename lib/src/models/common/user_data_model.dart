import 'package:barber_shop/src/views/base/helper.dart';

class UserData {
  int? id;
  String? name;
  String? email;
  UserType? userType;
  String? phone;
  int? status;
  String? avatar;
  String? paymentSystem;
  int? minimumPaymentAmount;

  UserData(
      {this.id,
        this.name,
        this.email,
        this.userType,
        this.phone,
        this.status,
        this.avatar,
        this.paymentSystem,
        this.minimumPaymentAmount});

  UserData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    userType = _getUserType(json['user_type']);
    phone = json['phone'];
    status = json['status'];
    avatar = json['avatar'];
    paymentSystem = json['payment_system'];
    minimumPaymentAmount = json['minimum_payment_amount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['email'] = email;
    data['user_type'] = userType;
    data['phone'] = phone;
    data['status'] = status;
    data['avatar'] = avatar;
    data['payment_system'] = paymentSystem;
    data['minimum_payment_amount'] = minimumPaymentAmount;
    return data;
  }

  UserType _getUserType(String userType) {
    return userType == 'customer'
        ? UserType.customer
        : userType == 'barber'
        ? UserType.barber
        : UserType.admin;
  }
}
