class CountryCodeData {
  List<Countries>? countries;
  String? message;
  int? status;

  CountryCodeData({this.countries, this.message, this.status});

  CountryCodeData.fromJson(Map<String, dynamic> json) {
    if (json['countries'] != null) {
      countries = <Countries>[];
      json['countries'].forEach((v) {
        countries!.add(Countries.fromJson(v));
      });
    }
    message = json['message'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (countries != null) {
      data['countries'] = countries!.map((v) => v.toJson()).toList();
    }
    data['message'] = message;
    data['status'] = status;
    return data;
  }
}

class Countries {
  String? iso;
  String? name;
  String? niceName;
  String? iso3;
  int? numCode;
  int? phoneCode;

  Countries(
      {this.iso,
        this.name,
        this.niceName,
        this.iso3,
        this.numCode,
        this.phoneCode});

  Countries.fromJson(Map<String, dynamic> json) {
    iso = json['iso'];
    name = json['name'];
    niceName = json['nicename'];
    iso3 = json['iso3'];
    numCode = json['numcode'];
    phoneCode = json['phonecode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['iso'] = iso;
    data['name'] = name;
    data['nicename'] = niceName;
    data['iso3'] = iso3;
    data['numcode'] = numCode;
    data['phonecode'] = phoneCode;
    return data;
  }
}
