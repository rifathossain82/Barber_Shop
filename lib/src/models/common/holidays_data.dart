class HolidaysData {
  List<String>? weeklyHoliday;
  List<String>? publicHolidays;
  String? message;
  int? status;

  HolidaysData({
    this.weeklyHoliday,
    this.publicHolidays,
    this.message,
    this.status,
  });

  HolidaysData.fromJson(Map<String, dynamic> json) {
    weeklyHoliday = json['weekly_holiday'].cast<String>();
    publicHolidays = json['public_holidays'].cast<String>();
    message = json['message'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['weekly_holiday'] = weeklyHoliday;
    data['public_holidays'] = publicHolidays;
    data['message'] = message;
    data['status'] = status;
    return data;
  }
}
