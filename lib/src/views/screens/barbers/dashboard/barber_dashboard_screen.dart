import 'package:barber_shop/src/utils/asset_path.dart';
import 'package:barber_shop/src/views/screens/barbers/home/barber_home_screen.dart';
import 'package:barber_shop/src/views/screens/barbers/services/services_screen_for_barber.dart';
import 'package:barber_shop/src/views/screens/common/profile/profile_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../utils/color.dart';

class BarberDashboardScreen extends StatefulWidget {
  const BarberDashboardScreen({Key? key}) : super(key: key);

  @override
  State<BarberDashboardScreen> createState() => _BarberDashboardScreenState();
}

class _BarberDashboardScreenState extends State<BarberDashboardScreen> {
  int currentIndex = 0;

  final screens = [
    const BarberHomeScreen(),
    const ServicesScreenForBarber(),
    ProfileScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: screens[currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: kWhite,
        elevation: 0,
        currentIndex: currentIndex,
        onTap: (index) {
          setState(() {
            currentIndex = index;
          });
        },
        items: [
          BottomNavigationBarItem(
            icon: const Icon(Icons.home),
            label: 'Home'.tr,
            tooltip: 'Home'.tr,
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
              AssetPath.barber,
              color: currentIndex == 1 ? mainColor : kBlackLight,
              height: 25,
            ),
            label: 'Services'.tr,
            tooltip: 'Services'.tr,
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.person),
            label: 'Profile'.tr,
            tooltip: 'Profile'.tr,
          ),
        ],
      ),
    );
  }
}
