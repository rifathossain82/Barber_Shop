import 'package:barber_shop/src/controllers/customers/customer_dashboard_controller.dart';
import 'package:barber_shop/src/utils/asset_path.dart';
import 'package:barber_shop/src/utils/color.dart';
import 'package:barber_shop/src/utils/dimensions.dart';
import 'package:barber_shop/src/views/base/helper.dart';
import 'package:barber_shop/src/views/base/k_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';

import '../../../../utils/styles.dart';

class AppointmentConfirmationScreenForBarber extends StatelessWidget {
  final DateTime date;
  final String time;

  const AppointmentConfirmationScreenForBarber({
    Key? key,
    required this.date,
    required this.time,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        margin: EdgeInsets.all(Dimensions.marginSizeDefault),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Spacer(),
            Lottie.asset(
              AssetPath.confirm,
              height: size.height * 0.4,
              alignment: Alignment.center,
            ),
            Text(
              "Appointment Booked".tr +
                  "\n ${DateFormat('MMM dd').format(date)}, $time"
                      .toUpperCase(),
              textAlign: TextAlign.center,
              style: h1.copyWith(
                fontWeight: FontWeight.w600,
                color: kGreen,
              ),
            ),
            const Spacer(),
            Text(
              "appointmentConfirmationDes".tr,
              textAlign: TextAlign.center,
              style: h4.copyWith(
                fontWeight: FontWeight.w500,
                color: kBlackLight.withOpacity(0.8),
              ),
            ),
            addVerticalSpace(Dimensions.paddingSizeSmall),
            KButton(
              onPressed: () {
                Get
                  ..back()
                  ..back()
                  ..back();
              },
              bgColor: kGreen,
              child: Text(
                'Ok, Got It'.tr,
                style: h3.copyWith(
                  fontWeight: FontWeight.bold,
                  color: kWhite,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
