import 'package:barber_shop/src/controllers/barbers/barber_appointment_controller.dart';
import 'package:barber_shop/src/network/api.dart';
import 'package:barber_shop/src/services/extensions/date_time_extension.dart';
import 'package:barber_shop/src/services/extensions/string_extension.dart';
import 'package:barber_shop/src/utils/app_constants.dart';
import 'package:barber_shop/src/utils/color.dart';
import 'package:barber_shop/src/utils/dimensions.dart';
import 'package:barber_shop/src/views/base/appointment_status_widget.dart';
import 'package:barber_shop/src/views/base/k_profile_image.dart';
import 'package:barber_shop/src/views/base/row_text.dart';
import 'package:barber_shop/src/views/screens/customers/appointments/components/cancel_button.dart';
import 'package:barber_shop/src/views/screens/customers/appointments/components/confirm_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../../models/barbers/barber_appointment_data.dart';
import '../../../../utils/styles.dart';
import '../../../base/barber_info.dart';
import '../../../base/helper.dart';
import '../../../base/k_divider.dart';

class BarberAppointmentDetails extends StatelessWidget {
  final AppointmentSchedules data;

  BarberAppointmentDetails({
    Key? key,
    required this.data,
  }) : super(key: key);

  final barberAppointmentController = Get.find<BarberAppointmentController>();

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "${data.start.toString().minuteAndSecond}, ${DateFormat('MMM dd').format(DateTime.parse(data.appointment!.date!))}",
        ),
      ),
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          SizedBox(
            height: size.height,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  addVerticalSpace(Dimensions.paddingSizeDefault),
                  AppointmentStatusWidget(
                    status: data.appointment!.status ==
                        AppointmentStatus.Confirmed.name
                        ? AppointmentStatus.Confirmed
                        : data.appointment!.status ==
                        AppointmentStatus.Booked.name
                        ? AppointmentStatus.Booked
                        : AppointmentStatus.Canceled,
                  ),
                  const BarberInfo(),
                  addVerticalSpace(Dimensions.paddingSizeSmall),
                  _buildAppointmentListTile(),
                  addVerticalSpace(Dimensions.paddingSizeSmall),
                  _buildAppointmentBills(),
                  addVerticalSpace(70),
                ],
              ),
            ),
          ),
          _buildFooterButtons(size),
        ],
      ),
    );
  }

  Widget _buildAppointmentListTile() {
    return ListTile(
      title: Text(
        data.appointment!.service!.title ?? "",
        style: h3.copyWith(
          fontWeight: FontWeight.w500,
        ),
      ),
      subtitle: Row(
        children: [
          KProfileImage(
            height: 30,
            width: 30,
            imgURL: Api.getImageURL(data.appointment!.customer!.avatar ?? ""),
          ),
          addHorizontalSpace(Dimensions.paddingSizeSmall),
          Text(data.appointment!.customer!.name ?? ""),
        ],
      ),
      trailing: Text(
        '${data.start!.minuteAndSecond} - ${data.end!.minuteAndSecond}',
        style: h4.copyWith(
          color: kBlackLight.withOpacity(0.8),
        ),
      ),
    );
  }

  Widget _buildAppointmentBills() {
    return Container(
      padding: EdgeInsets.all(Dimensions.paddingSizeDefault),
      margin: EdgeInsets.all(Dimensions.paddingSizeDefault),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: BorderRadius.circular(Dimensions.radiusSmall),
        boxShadow: [
          BoxShadow(
            offset: const Offset(0, 0),
            blurRadius: 4,
            spreadRadius: 0,
            color: kItemShadowColor,
          ),
        ],
      ),
      child: Column(
        children: [
          RowText(
            title: 'Service Charge'.tr,
            value: '${AppConstants.currencySymbol} ${data.appointment!.service!.price}',
          ),
          RowText(
            title: 'Tip'.tr,
            value: '${AppConstants.currencySymbol} ${data.appointment!.tipAmount}',
          ),
          const KDivider(height: 0),
          RowText(
            title: 'Total'.tr,
            titleStyle: h3.copyWith(
              color: kGreyTextColor,
              fontWeight: FontWeight.w700,
            ),
            value: '${AppConstants.currencySymbol} ${data.appointment!.total}',
          ),
          RowText(
            title: '${'Paid by'.tr} ${data.appointment!.paymentMethod ?? 'Cash'}',
            value: '${AppConstants.currencySymbol} ${data.appointment!.paid}',
          ),
          const KDivider(height: 0),
          RowText(
            title: 'Due'.tr,
            value: '${AppConstants.currencySymbol} ${data.appointment!.dueAmount}',
          ),
        ],
      ),
    );
  }

  Widget _buildFooterButtons(Size size) {
    return Positioned(
      child: Container(
        height: 70,
        color: kWhite,
        padding: EdgeInsets.symmetric(
          vertical: Dimensions.paddingSizeDefault,
        ),
        child: data.appointment!.status == AppointmentStatus.Confirmed.name
            ? CancelButton(
                onCancelled: () {
                  changeStatus(AppointmentStatus.Canceled);
                },
                width: size.width - 30,
              )
            : data.appointment!.status == AppointmentStatus.Canceled.name
                ? Container()
                : Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      CancelButton(
                        onCancelled: () {
                          changeStatus(AppointmentStatus.Canceled);
                        },
                        width: size.width * 0.42,
                      ),
                      ConfirmButton(
                        onConfirmed: () {
                          changeStatus(AppointmentStatus.Confirmed);
                        },
                        width: size.width * 0.42,
                      ),
                    ],
                  ),
      ),
    );
  }

  void changeStatus(AppointmentStatus status) async {
    await barberAppointmentController.changeBarberAppointmentStatus(
      id: data.appointment!.id!,
      status: status.name,
    );
    barberAppointmentController.getBarberAppointmentList(
      date: DateTime.parse(data.appointment!.date ?? "").yyyyMMddWithSlash,
    );
  }
}
