import 'package:barber_shop/src/controllers/barbers/barber_appointment_controller.dart';
import 'package:barber_shop/src/controllers/common/holiday_controller.dart';
import 'package:barber_shop/src/services/extensions/date_time_extension.dart';
import 'package:barber_shop/src/utils/dimensions.dart';
import 'package:barber_shop/src/views/base/k_custom_loader.dart';
import 'package:barber_shop/src/views/base/no_data_found.dart';
import 'package:barber_shop/src/views/screens/barbers/appointment_details/barber_appointment_details.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:table_calendar/table_calendar.dart';

import '../../../base/booked_appointment_item.dart';
import '../../../base/calendar_widget.dart';
import '../../../base/helper.dart';
import '../../../base/unbooked_appointment_item.dart';

class BarberHomeScreen extends StatefulWidget {
  const BarberHomeScreen({Key? key}) : super(key: key);

  @override
  State<BarberHomeScreen> createState() => _BarberHomeScreenState();
}

class _BarberHomeScreenState extends State<BarberHomeScreen> {
  late DateTime selectedDay;
  late DateTime focusDay;
  List<int> weekendDays = [];
  List<DateTime> holidays = [];

  final barberAppointmentController = Get.put(BarberAppointmentController());
  final holidayController = Get.put(HolidayController());

  @override
  void initState() {
    selectedDay = DateTime.now();
    focusDay = DateTime.now();
    setHolidaysAndWeekend();
    super.initState();
  }

  void setHolidaysAndWeekend()async{
    await holidayController.getHolidayList();
    /// set weekend day
    for (var element in holidayController.holidays.value.weeklyHoliday!) {
      weekendDays.add(int.parse(element));
    }

    /// set holiday
    for (var element in holidayController.holidays.value.publicHolidays!) {
      var day = DateTime.parse(element);
      holidays.add(DateTime.utc(day.year, day.month, day.day));
    }

    /// set selected day in the next day of all weekend day and holiday
    for (var element in holidays) {
      if(DateFormat('yyyy-MM-dd').format(element) == DateFormat('yyyy-MM-dd').format(selectedDay)){
        selectedDay = selectedDay.add(const Duration(days: 1));
        focusDay = focusDay.add(const Duration(days: 1));
      }
    }

    for(int i=1; i<7; i++){
      if(weekendDays.contains(selectedDay.weekday)){
        selectedDay = selectedDay.add(const Duration(days: 1));
        focusDay = focusDay.add(const Duration(days: 1));
      }
    }

    /// fetch appointment list based on selected date
    await barberAppointmentController.getBarberAppointmentList(
      date: selectedDay.yyyyMMddWithSlash,
    );

    if(!mounted){
      return;
    }
    setState(() {});

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Appointments'.tr),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Obx(() {
            return Column(
              children: [
                _buildCalender(),
                _buildBookingList(),
              ],
            );
          }),
        ),
      ),
    );
  }

  Widget _buildCalender() {
    return Container(
      margin: EdgeInsets.all(Dimensions.paddingSizeDefault),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(Dimensions.radiusSmall),
      ),
      child: CalendarWidget(
        firstDay: DateTime(2010),
        focusDay: focusDay,
        selectedDay: selectedDay,
        weekendDays: weekendDays,
        holidays: holidays,
        calendarFormat: CalendarFormat.twoWeeks,
        onDaySelected: (currentDay, focusDay_) {
          if (currentDay != selectedDay) {
            setState(() {
              selectedDay = currentDay;
              focusDay = focusDay_;
              barberAppointmentController.getBarberAppointmentList(
                date: selectedDay.yyyyMMddWithSlash,
              );
            });
          }
        },
      ),
    );
  }

  Widget _buildBookingList() {
    return barberAppointmentController.isLoading.value
        ? const KCustomLoader()
        : barberAppointmentController
                .appointmentData.value.appointmentSchedules!.isEmpty
            ? const NoDataFound()
            : ListView.separated(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: barberAppointmentController
                    .appointmentData.value.appointmentSchedules!.length,
                padding: EdgeInsets.all(
                  Dimensions.paddingSizeDefault,
                ),
                itemBuilder: (context, index) {
                  if (barberAppointmentController.appointmentData.value
                          .appointmentSchedules![index].appointment ==
                      null) {
                    return UnBookedAppointmentItem(
                      data: barberAppointmentController
                          .appointmentData.value.appointmentSchedules![index],
                    );
                  } else {
                    var appointmentSchedule = barberAppointmentController
                        .appointmentData.value.appointmentSchedules![index];
                    return BookedAppointmentItem(
                      data: appointmentSchedule,
                      onTap: () {
                        Get.to(
                          () => BarberAppointmentDetails(
                            data: appointmentSchedule,
                          ),
                        );
                      },
                    );
                  }
                },
                separatorBuilder: (context, index) =>
                    addVerticalSpace(Dimensions.paddingSizeLarge),
              );
  }
}
