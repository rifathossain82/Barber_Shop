import 'package:barber_shop/src/utils/asset_path.dart';
import 'package:barber_shop/src/views/screens/admin/home/admin_home_screen.dart';
import 'package:barber_shop/src/views/screens/admin/remainder/remainder_screen.dart';
import 'package:barber_shop/src/views/screens/admin/services/services_screen_for_admin.dart';
import 'package:barber_shop/src/views/screens/common/profile/profile_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../utils/color.dart';
import '../customer_list/customer_list_screen.dart';

class AdminDashboardScreen extends StatefulWidget {
  const AdminDashboardScreen({Key? key}) : super(key: key);

  @override
  State<AdminDashboardScreen> createState() => _AdminDashboardScreenState();
}

class _AdminDashboardScreenState extends State<AdminDashboardScreen> {
  int currentIndex = 0;

  final screens = [
    const AdminHomeScreen(),
    const ServicesScreenForAdmin(),
    const RemainderScreen(),
    const CustomerListScreen(),
    ProfileScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: screens[currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: kWhite,
        type: BottomNavigationBarType.fixed,
        elevation: 0,
        currentIndex: currentIndex,
        onTap: (index) {
          setState(() {
            currentIndex = index;
          });
        },
        items: [
          BottomNavigationBarItem(
            icon: const Icon(Icons.home),
            label: 'Home'.tr,
            tooltip: 'Home'.tr,
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
              AssetPath.barber,
              color: currentIndex == 1 ? mainColor : kBlackLight,
              height: 25,
            ),
            label: 'Services'.tr,
            tooltip: 'Services'.tr,
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.notifications_active_sharp),
            label: 'Remainder'.tr,
            tooltip: 'Remainder'.tr,
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.people),
            label: 'Customers'.tr,
            tooltip: 'Customers'.tr,
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.person),
            label: 'Profile'.tr,
            tooltip: 'Profile'.tr,
          ),
        ],
      ),
    );
  }
}
