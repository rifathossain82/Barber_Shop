import 'package:barber_shop/src/utils/color.dart';
import 'package:barber_shop/src/utils/dimensions.dart';
import 'package:barber_shop/src/utils/styles.dart';
import 'package:barber_shop/src/views/base/helper.dart';
import 'package:barber_shop/src/views/base/k_button.dart';
import 'package:barber_shop/src/views/base/k_custom_loader.dart';
import 'package:barber_shop/src/views/base/k_outlined_button.dart';
import 'package:barber_shop/src/views/base/k_text_form_field.dart';
import 'package:barber_shop/src/views/base/no_data_found.dart';
import 'package:barber_shop/src/views/base/customer_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sms/flutter_sms.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../../controllers/common/customer_list_controller.dart';

class CustomerListScreen extends StatefulWidget {
  const CustomerListScreen({Key? key}) : super(key: key);

  @override
  State<CustomerListScreen> createState() => _CustomerListScreenState();
}

class _CustomerListScreenState extends State<CustomerListScreen> {

  // to filter customer list
  final nameController = TextEditingController();
  FocusNode nameFocusNode = FocusNode();
  final fromIndexController = TextEditingController();
  final toIndexController = TextEditingController();
  final filterFormKey = GlobalKey<FormState>();

  final customerListController = Get.put(CustomerListController());
  final ScrollController _scrollController = ScrollController();

  // to send sms
  final messageTextController = TextEditingController();
  FocusNode messageTextFocusNode = FocusNode();
  final messageFormKey = GlobalKey<FormState>();

  bool isFilter = false;
  bool isAllSelected = false;

  final doesNotBookStatus = ['All', '1', '2', '3', '4', '5'];
  String selectedBookStatus = 'All';

  @override
  void initState() {
    customerListController.pageNumber.value = 1;
    customerListController.getCustomerList();
    if (!isFilter) {
      scrollIndicator();
    }
    super.initState();
  }

  @override
  void dispose() {
    nameController.dispose();
    nameFocusNode.dispose();
    fromIndexController.dispose();
    toIndexController.dispose();
    messageTextController.dispose();
    messageTextFocusNode.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  void filterMethod() {
    customerListController.filterCustomerList(
      searchText: nameController.text,
      doesNotBookStatus: selectedBookStatus,
      startIndex: fromIndexController.text,
      endIndex: toIndexController.text,
    );
  }

  void scrollIndicator() {
    _scrollController.addListener(
      () {
        if (_scrollController.offset >=
                _scrollController.position.maxScrollExtent &&
            !_scrollController.position.outOfRange) {
          kPrint('reach to bottom');
          if (!customerListController.loadedCompleted.value) {
            ++customerListController.pageNumber.value;
            customerListController.getCustomerList();
          }
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('Customers'.tr),
        actions: [
          isAllSelected
              ? IconButton(
                  onPressed: () {
                    messageTextFocusNode = FocusNode();
                    messageTextFocusNode.requestFocus();
                    openSendSMSDialog(context);
                  },
                  icon: const Icon(Icons.send),
                )
              : Container(),
          addHorizontalSpace(Dimensions.paddingSizeExtraSmall),
          isFilter
              ? IconButton(
                  onPressed: () {
                    setState(() {
                      isAllSelected = !isAllSelected;
                    });
                  },
                  icon: const Icon(Icons.select_all),
                )
              : Container(),
          addHorizontalSpace(Dimensions.paddingSizeExtraSmall),
          IconButton(
            onPressed: () {
              nameFocusNode = FocusNode();
              nameFocusNode.requestFocus();
              openFilterDialog(context);
            },
            icon: const Icon(Icons.filter_list_sharp),
          ),
          addHorizontalSpace(Dimensions.paddingSizeExtraSmall),
        ],
      ),
      body: Obx(() {
        return Column(
          children: [
            isFilter ? _buildFilteredHint(size) : Container(),
            addVerticalSpace(Dimensions.paddingSizeDefault),
            Expanded(
              child: _buildCustomerList(),
            ),
          ],
        );
      }),
    );
  }

  Widget _buildFilteredHint(Size size) {
    return Container(
      height: 110,
      alignment: Alignment.center,
      margin: EdgeInsets.symmetric(
        horizontal: Dimensions.marginSizeDefault,
      ),
      padding: EdgeInsets.symmetric(
        horizontal: Dimensions.paddingSizeSmall,
      ),
      width: size.width,
      decoration: BoxDecoration(
        color: kBlueGrey.withOpacity(0.12),
        borderRadius: BorderRadius.circular(
          Dimensions.radiusSmall,
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                RichText(
                  maxLines: 1,
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text: '${'Name/First Word'.tr}: ',
                        style: h4.copyWith(
                          color: kBlackLight.withOpacity(0.8),
                        ),
                      ),
                      TextSpan(
                        text: nameController.text,
                        style: h4.copyWith(
                          fontWeight: FontWeight.w700,
                          color: kBlackLight.withOpacity(0.8),
                        ),
                      ),
                    ],
                  ),
                ),
                RichText(
                  maxLines: 1,
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text: '${'From'.tr}: ',
                        style: h4.copyWith(
                          color: kBlackLight.withOpacity(0.8),
                        ),
                      ),
                      TextSpan(
                        text: fromIndexController.text,
                        style: h4.copyWith(
                          fontWeight: FontWeight.w700,
                          color: kBlackLight.withOpacity(0.8),
                        ),
                      ),
                    ],
                  ),
                ),
                RichText(
                  maxLines: 1,
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text: '${'To'.tr}: ',
                        style: h4.copyWith(
                          color: kBlackLight.withOpacity(0.8),
                        ),
                      ),
                      TextSpan(
                        text: toIndexController.text,
                        style: h4.copyWith(
                          fontWeight: FontWeight.w700,
                          color: kBlackLight.withOpacity(0.8),
                        ),
                      ),
                    ],
                  ),
                ),
                RichText(
                  maxLines: 1,
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text: "${"Doesn't book (weeks)".tr}: ",
                        style: h4.copyWith(
                          color: kBlackLight.withOpacity(0.8),
                        ),
                      ),
                      TextSpan(
                        text: selectedBookStatus,
                        style: h4.copyWith(
                          fontWeight: FontWeight.w700,
                          color: kBlackLight.withOpacity(0.8),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          IconButton(
            onPressed: removeFilter,
            icon: Icon(
              Icons.close,
              color: kBlackLight.withOpacity(0.8),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildCustomerList() {
    return customerListController.isLoading.value
        ? const KCustomLoader()
        : customerListController.customerList.isEmpty
            ? const NoDataFound()
            : isFilter
                ? ListView.separated(
                    controller: _scrollController,
                    physics: const BouncingScrollPhysics(),
                    padding: EdgeInsets.symmetric(
                      horizontal: Dimensions.paddingSizeDefault,
                    ),
                    itemCount: customerListController.customerList.length,
                    itemBuilder: (context, index) {
                      return CustomerWidget(
                        customerData:
                            customerListController.customerList[index],
                        isSelected: isAllSelected,
                      );
                    },
                    separatorBuilder: (context, index) =>
                        addVerticalSpace(Dimensions.paddingSizeLarge),
                  )
                : ListView.separated(
                    controller: _scrollController,
                    physics: const BouncingScrollPhysics(),
                    padding: EdgeInsets.symmetric(
                      horizontal: Dimensions.paddingSizeDefault,
                    ),
                    itemCount: customerListController.customerList.length + 1,
                    itemBuilder: (context, index) {
                      if (index == customerListController.customerList.length &&
                          !customerListController.loadedCompleted.value) {
                        return const Center(child: CircularProgressIndicator());
                      } else if (index ==
                              customerListController.customerList.length &&
                          customerListController.loadedCompleted.value) {
                        return Container();
                      } else {
                        return CustomerWidget(
                          customerData:
                              customerListController.customerList[index],
                        );
                      }
                    },
                    separatorBuilder: (context, index) =>
                        addVerticalSpace(Dimensions.paddingSizeLarge),
                  );
  }

  openFilterDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        return StatefulBuilder(
          builder: (context, setState) {
            return AlertDialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(Dimensions.radiusDefault),
              ),
              insetPadding: EdgeInsets.all(
                Dimensions.paddingSizeDefault,
              ),
              alignment: Alignment.bottomCenter,
              actionsAlignment: MainAxisAlignment.spaceEvenly,
              title: Text(
                'Filter Customer'.tr,
                textAlign: TextAlign.center,
                style: h2.copyWith(
                  fontWeight: FontWeight.w500,
                ),
              ),
              content: SizedBox(
                width: MediaQuery.of(context).size.width,
                child: Form(
                  key: filterFormKey,
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        KTextFormFiled(
                          controller: nameController,
                          hintText: 'A',
                          labelText: 'Name/First Word'.tr,
                          inputType: TextInputType.name,
                          focusNode: nameFocusNode,
                          inputAction: TextInputAction.next,
                        ),
                        addVerticalSpace(Dimensions.paddingSizeDefault),
                        IntrinsicHeight(
                          child: Row(
                            children: [
                              Expanded(
                                child: KTextFormFiled(
                                  controller: fromIndexController,
                                  hintText: '1',
                                  labelText: '${'From'.tr} (${'Index'.tr})',
                                  inputType: TextInputType.number,
                                  inputAction: TextInputAction.next,
                                  validator: (value) {
                                    if (value.toString().isEmpty) {
                                      return Message.emptyNumber.tr;
                                    } else if (int.parse(value) < 0) {
                                      return Message.invalidNumber.tr;
                                    }
                                    return null;
                                  },
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(
                                  vertical: Dimensions.paddingSizeSmall,
                                  horizontal: Dimensions.paddingSizeSmall,
                                ),
                                child: VerticalDivider(
                                    color: kDividerColor, thickness: 1),
                              ),
                              Expanded(
                                child: KTextFormFiled(
                                  controller: toIndexController,
                                  hintText: '100',
                                  labelText: '${'To'.tr} (${'Index'.tr})',
                                  inputType: TextInputType.number,
                                  inputAction: TextInputAction.next,
                                  validator: (value) {
                                    if (value.toString().isEmpty) {
                                      return Message.emptyNumber.tr;
                                    } else if (int.parse(value) < 0) {
                                      return Message.invalidNumber.tr;
                                    }
                                    return null;
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                        addVerticalSpace(Dimensions.paddingSizeDefault),
                        DropdownButtonFormField(
                          items: doesNotBookStatus
                              .map(
                                (e) => DropdownMenuItem(
                              value: e,
                              child: Text(
                                e,
                                style: h3,
                              ),
                            ),
                          )
                              .toList(),
                          decoration: InputDecoration(
                            labelText: "Doesn't book (weeks)".tr,
                            border: const UnderlineInputBorder()
                          ),
                          value: selectedBookStatus,
                          isExpanded: true,
                          onChanged: (value) {
                            if (value != null) {
                              setState(() {
                                selectedBookStatus = value;
                              });
                            }
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              actions: [
                /// cancel button
                KOutlinedButton(
                  onPressed: () => Navigator.pop(context),
                  width: MediaQuery.of(context).size.width * 0.4,
                  borderColor: mainColor,
                  bgColor: kWhite,
                  child: Text(
                    'Cancel'.tr,
                    style: h3.copyWith(
                      fontWeight: FontWeight.bold,
                      color: mainColor,
                    ),
                  ),
                ),

                /// apply button
                KButton(
                  onPressed: () async {
                    if (filterFormKey.currentState!.validate()) {
                      applyFilter();
                      Navigator.pop(context);
                    }
                  },
                  width: MediaQuery.of(context).size.width * 0.4,
                  child: Text(
                    'Apply'.tr,
                    style: h3.copyWith(
                      fontWeight: FontWeight.bold,
                      color: kWhite,
                    ),
                  ),
                ),
              ],
            );
          },
        );
      },
    );
  }

  openSendSMSDialog(BuildContext context) {
    customDialog(
      context: context,
      title: "Send Message".tr,
      content: Form(
        key: messageFormKey,
        child: KTextFormFiled(
          controller: messageTextController,
          focusNode: messageTextFocusNode,
          labelText: 'Message'.tr,
          minLines: 1,
          maxLines: 10,
          inputAction: TextInputAction.newline,
          inputType: TextInputType.multiline,
          validator: (value) {
            if (value.toString().isEmpty) {
              return Message.emptyMessage.tr;
            }
            return null;
          },
        ),
      ),
      actions: [
        KButton(
          width: MediaQuery.of(context).size.width * 0.9,
          onPressed: _sendSMS,
          child: Text(
            'Send SMS'.tr,
            style: GoogleFonts.roboto(
              textStyle: h3.copyWith(
                color: kWhite,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
        )
      ],
      dialogPosition: Alignment.bottomCenter,
    );
  }

  void removeFilter() {
    isFilter = false;
    customerListController.pageNumber.value = 1;
    customerListController.getCustomerList();

    /// clear filters data
    nameController.clear();
    fromIndexController.clear();
    toIndexController.clear();
    selectedBookStatus = doesNotBookStatus.first;

    /// if user remove filter then set isAllSelected is false
    isAllSelected = false;
    setState(() {});
  }

  void applyFilter() {
    setState(() {
      isFilter = true;
      filterMethod();
    });
  }

  void _sendSMS() async {
    if (messageFormKey.currentState!.validate()) {
      String result = await sendSMS(
        message: messageTextController.text,
        recipients: customerListController.customersPhoneList.toSet().toList(),
      ).catchError((onError) {
        kSnackBar(message: onError, bgColor: failedColor);
      });

      kSnackBar(message: result, bgColor: successColor);
      messageTextController.clear();
      if (mounted) Navigator.pop(context);
    }
  }
}
