import 'package:barber_shop/src/services/extensions/date_time_extension.dart';
import 'package:barber_shop/src/utils/dimensions.dart';
import 'package:barber_shop/src/views/base/helper.dart';
import 'package:barber_shop/src/views/base/k_custom_loader.dart';
import 'package:barber_shop/src/views/base/no_data_found.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sms/flutter_sms.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

import '../../../../controllers/admin/remainder_controller.dart';
import '../../../../utils/color.dart';
import '../../../../utils/styles.dart';
import '../../../base/k_button.dart';
import '../../../base/k_text_form_field.dart';

class RemainderScreen extends StatefulWidget {
  const RemainderScreen({Key? key}) : super(key: key);

  @override
  State<RemainderScreen> createState() => _RemainderScreenState();
}

class _RemainderScreenState extends State<RemainderScreen> {
  final remainderController = Get.put(RemainderController());
  final messageTextController = TextEditingController();
  final formKey = GlobalKey<FormState>();

  DateTime selectedDate = DateTime.now();
  AppointmentStatus selectedStatus = AppointmentStatus.Booked;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      getRemainderData();
    });
    super.initState();
  }

  void getRemainderData() {
    remainderController.getRemainderDataList(
      date: selectedDate.yyyyMMddWithSlash,
      status: selectedStatus.name,
    );
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2010),
        lastDate: DateTime(2100));
    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
        getRemainderData();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Remainder'.tr),
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          children: [
            _buildDateWidget(),
            _buildStatus(),
            Obx(() {
              return remainderController.isLoading.value
                  ? const KCustomLoader()
                  : remainderController.phoneNumberList.isEmpty
                      ? Container(
                          height: 50,
                          alignment: Alignment.center,
                          child: Text(
                            'No customers Found!'.tr,
                            style: h3,
                          ),
                        )
                      : Form(
                          key: formKey,
                          child: Column(
                            children: [
                              Text(
                                '${'Total Customer'.tr}: ${remainderController.phoneNumberList.toSet().toList().length}',
                                textAlign: TextAlign.center,
                                style: h2.copyWith(
                                  fontWeight: FontWeight.w700,
                                  color: kBlackLight.withOpacity(0.7),
                                ),
                              ),
                              _buildMessageTextFormField(),
                              _buildSendSMSButton(context),
                            ],
                          ),
                        );
            }),
          ],
        ),
      ),
    );
  }

  Widget _buildDateWidget() {
    return Container(
      padding: EdgeInsets.all(Dimensions.paddingSizeDefault),
      margin: EdgeInsets.symmetric(horizontal: Dimensions.paddingSizeDefault),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: BorderRadius.circular(Dimensions.radiusDefault),
        boxShadow: [
          BoxShadow(
            offset: const Offset(0, 2),
            blurRadius: 4,
            spreadRadius: 0,
            color: kItemShadowColor,
          ),
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "${'Date'.tr}: ",
            style: h4,
          ),
          const Spacer(),
          GestureDetector(
            onTap: () => _selectDate(context),
            child: Text(
              DateFormat("yyyy-MM-dd").format(selectedDate),
              style: h4,
            ),
          ),
          addHorizontalSpace(Dimensions.paddingSizeSmall),
          Icon(
            Icons.arrow_forward_ios,
            color: kBlackLight.withOpacity(0.8),
            size: 16,
          ),
        ],
      ),
    );
  }

  Widget _buildStatus() {
    return Container(
      padding: EdgeInsets.all(Dimensions.paddingSizeDefault),
      margin: EdgeInsets.symmetric(
        horizontal: Dimensions.paddingSizeDefault,
        vertical: Dimensions.paddingSizeLarge,
      ),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: BorderRadius.circular(Dimensions.radiusDefault),
        boxShadow: [
          BoxShadow(
            offset: const Offset(0, 2),
            blurRadius: 4,
            spreadRadius: 0,
            color: kItemShadowColor,
          ),
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "${'Status'.tr}: ",
            style: h4,
          ),
          const Spacer(),
          DropdownButton(
            icon: Padding(
              padding: EdgeInsets.only(left: Dimensions.paddingSizeSmall),
              child: Icon(
                Icons.arrow_forward_ios,
                color: kBlackLight.withOpacity(0.8),
                size: 16,
              ),
            ),
            underline: Container(),
            dropdownColor: kWhite,
            isDense: true,
            alignment: Alignment.centerRight,
            value: selectedStatus,
            onChanged: (value) {
              setState(() {
                if (value != null) {
                  selectedStatus = value;
                  getRemainderData();
                }
              });
            },
            items: AppointmentStatus.values
                .map(
                  (e) => DropdownMenuItem(
                    value: e,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      e.name,
                      style: h4,
                    ),
                  ),
                )
                .toList(),
          ),
        ],
      ),
    );
  }

  Widget _buildMessageTextFormField() {
    return Container(
      padding: EdgeInsets.all(Dimensions.paddingSizeDefault),
      margin: EdgeInsets.symmetric(
        horizontal: Dimensions.paddingSizeDefault,
        vertical: Dimensions.paddingSizeLarge,
      ),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: BorderRadius.circular(Dimensions.radiusDefault),
        boxShadow: [
          BoxShadow(
            offset: const Offset(0, 2),
            blurRadius: 4,
            spreadRadius: 0,
            color: kItemShadowColor,
          ),
        ],
      ),
      child: KTextFormFiled(
        controller: messageTextController,
        labelText: 'Message'.tr,
        minLines: 1,
        maxLines: 10,
        inputAction: TextInputAction.newline,
        inputType: TextInputType.multiline,
        validator: (value) {
          if (value.toString().isEmpty) {
            return Message.emptyMessage.tr;
          }
          return null;
        },
      ),
    );
  }

  Widget _buildSendSMSButton(BuildContext context) => KButton(
        width: MediaQuery.of(context).size.width * 0.9,
        onPressed: _sendSMS,
        child: Text(
          'Send SMS'.tr,
          style: GoogleFonts.roboto(
            textStyle: h3.copyWith(
              color: kWhite,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
      );

  void _sendSMS() async {
    if (formKey.currentState!.validate()) {
      String result = await sendSMS(
        message: messageTextController.text,
        recipients: remainderController.phoneNumberList.toSet().toList(),
      ).catchError((onError) {
        kSnackBar(message: onError, bgColor: failedColor);
      });

      kSnackBar(message: result, bgColor: successColor);
    }
  }
}
