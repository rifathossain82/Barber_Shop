import 'package:barber_shop/src/controllers/admin/admin_appointment_controller.dart';
import 'package:barber_shop/src/controllers/common/barber_list_controller.dart';
import 'package:barber_shop/src/controllers/common/customer_list_controller.dart';
import 'package:barber_shop/src/controllers/customers/time_slot_controller.dart';
import 'package:barber_shop/src/models/common/customer_data_model.dart';
import 'package:barber_shop/src/models/common/barber_data.dart';
import 'package:barber_shop/src/models/common/service_data_model.dart';
import 'package:barber_shop/src/models/customers/time_slot_data.dart';
import 'package:barber_shop/src/network/api.dart';
import 'package:barber_shop/src/services/extensions/date_time_extension.dart';
import 'package:barber_shop/src/services/extensions/string_extension.dart';
import 'package:barber_shop/src/utils/app_constants.dart';
import 'package:barber_shop/src/utils/color.dart';
import 'package:barber_shop/src/utils/dimensions.dart';
import 'package:barber_shop/src/utils/styles.dart';
import 'package:barber_shop/src/views/base/barber_image.dart';
import 'package:barber_shop/src/views/base/calendar_widget.dart';
import 'package:barber_shop/src/views/base/helper.dart';
import 'package:barber_shop/src/views/base/k_button.dart';
import 'package:barber_shop/src/views/base/k_custom_loader.dart';
import 'package:barber_shop/src/views/base/k_divider.dart';
import 'package:barber_shop/src/views/base/k_profile_image.dart';
import 'package:barber_shop/src/views/base/k_text_form_field.dart';
import 'package:barber_shop/src/views/base/time_slot_item.dart';
import 'package:barber_shop/src/views/screens/admin/book_appointment/components/admin_appointment_booking_sheet.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import '../../../../controllers/common/holiday_controller.dart';
import '../../../base/k_outlined_button.dart';

class BookAppointmentScreenForAdmin extends StatefulWidget {
  final ServiceData service;

  const BookAppointmentScreenForAdmin({
    Key? key,
    required this.service,
  }) : super(key: key);

  @override
  State<BookAppointmentScreenForAdmin> createState() =>
      _BookAppointmentScreenState();
}

class _BookAppointmentScreenState extends State<BookAppointmentScreenForAdmin> {
  final barberController = Get.put(BarberListController());
  final timeSlotController = Get.put(TimeSlotController());
  final appointmentController = Get.put(AdminAppointmentController());
  final holidayController = Get.put(HolidayController());

  final customerListController = Get.put(CustomerListController());
  final customerNameController = TextEditingController();
  FocusNode customerNameFocusNode = FocusNode();
  var addCustomerFormKey = GlobalKey<FormState>();
  CustomerData? selectedCustomer;

  // Initially stripe is selected
  int paymentMethod = 1;

  int? selectedBarberIndex;
  int? selectedTimeIndex;
  Barber? selectedBarber;
  TimeSlots? selectedTimeSlot;

  late DateTime selectedDay;
  late DateTime focusDay;
  late DateTime lastDay;
  List<int> weekendDays = [];
  List<DateTime> holidays = [];


  @override
  void initState() {
    selectedDay = DateTime.now();
    focusDay = DateTime.now();
    lastDay = DateTime.now().add(const Duration(days: 21));
    setHolidaysAndWeekend();

    /// fetch all customer
    customerListController.getCustomerListLite();
    super.initState();
  }

  void setHolidaysAndWeekend() async {
    await holidayController.getHolidayList();

    /// set weekend day
    for (var element in holidayController.holidays.value.weeklyHoliday!) {
      weekendDays.add(int.parse(element));
    }

    /// set holiday
    for (var element in holidayController.holidays.value.publicHolidays!) {
      var day = DateTime.parse(element);
      holidays.add(DateTime.utc(day.year, day.month, day.day));
    }

    /// set selected day in the next day of all weekend day and holiday
    for (var element in holidays) {
      if (DateFormat('yyyy-MM-dd').format(element) ==
          DateFormat('yyyy-MM-dd').format(selectedDay)) {
        selectedDay = selectedDay.add(const Duration(days: 1));
        focusDay = focusDay.add(const Duration(days: 1));
      }
    }
    for(int i=1; i<7; i++){
      if(weekendDays.contains(selectedDay.weekday)){
        selectedDay = selectedDay.add(const Duration(days: 1));
        focusDay = focusDay.add(const Duration(days: 1));
      }
    }

    /// fetch barber list based on selected day
    barberController.getBarberList(date: selectedDay.yyyyMMddWithSlash);
    if (!mounted) {
      return;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('Book an Appointment'.tr),
      ),
      body: Obx(() {
        return appointmentController.isLoading.value
            ? const KCustomLoader()
            : Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  _buildBody(size),
                  Positioned(
                    child: _buildBottomSection(size),
                  ),
                ],
              );
      }),
    );
  }

  Widget _buildBody(Size size) {
    return SizedBox(
      height: size.height,
      child: SingleChildScrollView(
        child: Column(
          children: [
            _buildCalender(),
            const KDivider(),
            _buildBarber(),
            const KDivider(),
            _buildTimeSlot(),
            const KDivider(),
            _buildCustomerDropdown(),
            const KDivider(),
            _buildFinalAppointment(),
            // _buildPaymentMethods(),
            addVerticalSpace(90),
          ],
        ),
      ),
    );
  }

  Widget _buildBottomSection(Size size) {
    return Container(
      height: 90,
      width: size.width,
      padding: EdgeInsets.all(Dimensions.paddingSizeDefault),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: const BorderRadius.only(
          topRight: Radius.circular(Dimensions.radiusLarge),
          topLeft: Radius.circular(Dimensions.radiusLarge),
        ),
      ),
      child: Row(
        children: [
          Expanded(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "${widget.service.price} ${AppConstants.currencySymbol}",
                  style: h1.copyWith(fontWeight: FontWeight.w700),
                ),
                // Text(
                //   "45min",
                //   style: h4,
                // ),
              ],
            ),
          ),
          Expanded(
            child: KButton(
              height: 50,
              onPressed: () {
                if (selectedBarber == null) {
                  kSnackBar(
                    message: "Please, Select a barber!".tr,
                    bgColor: failedColor,
                  );
                } else if (selectedTimeSlot == null) {
                  kSnackBar(
                    message: "Please, Select a Time-Slot!".tr,
                    bgColor: failedColor,
                  );
                } else if (selectedCustomer == null) {
                  kSnackBar(
                    message: "Please, Select a Customer!".tr,
                    bgColor: failedColor,
                  );
                } else {
                  openBookingConfirmDialog(context);
                }
              },
              child: Text(
                'Book'.tr,
                style: h3.copyWith(
                  fontWeight: FontWeight.bold,
                  color: kWhite,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildCalender() {
    return CalendarWidget(
      focusDay: focusDay,
      selectedDay: selectedDay,
      lastDay: lastDay,
      weekendDays: weekendDays,
      holidays: holidays,
      onDaySelected: (currentDay, focusDay_) {
        if (currentDay != selectedDay) {
          setState(() {
            selectedDay = currentDay;
            focusDay = focusDay_;
            selectedBarberIndex = null;
            selectedTimeIndex = null;
            barberController.getBarberList(
              date: selectedDay.yyyyMMddWithSlash,
            );
          });
        }
      },
    );
  }

  Widget _buildBarber() {
    return Obx(() {
      if (barberController.isLoading.value) {
        return const KCustomLoader();
      } else if (barberController.barberData.value.barbers!.isEmpty) {
        return Container(
          height: 50,
          alignment: Alignment.center,
          child: Text(
            'No Barber Found!'.tr,
            style: h3,
          ),
        );
      } else {
        final barberData = barberController.barberData.value;
        return Container(
          height: 80,
          alignment: Alignment.centerLeft,
          margin: EdgeInsets.symmetric(
            horizontal: Dimensions.marginSizeDefault,
            vertical: Dimensions.marginSizeSmall,
          ),
          child: ListView.separated(
            itemCount: barberData.barbers!.length,
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) => GestureDetector(
              onTap: () {
                setState(() {
                  if (selectedBarberIndex != index) {
                    selectedBarberIndex = index;
                    selectedBarber = barberData.barbers![index];
                    timeSlotController.getTimeSlotList(
                      date: selectedDay.yyyyMMddWithSlash,
                      barberId: selectedBarber!.id!,
                    );
                  }
                });
              },
              child: BarberProfileItem(
                isSelected: selectedBarberIndex == index ? true : false,
                imgURL: Api.getImageURL(barberData.barbers![index].avatar),
                name: barberData.barbers![index].name ?? "",
              ),
            ),
            separatorBuilder: (context, index) => addHorizontalSpace(
              Dimensions.paddingSizeExtraLarge,
            ),
          ),
        );
      }
    });
  }

  Widget _buildTimeSlot() {
    return selectedBarberIndex == null
        ? Container(
            height: 50,
            alignment: Alignment.center,
            child: Text(
              'Select a Barber!'.tr,
              style: h3,
            ),
          )
        : Obx(() {
            if (timeSlotController.isLoading.value) {
              return const KCustomLoader();
            } else if (timeSlotController
                .timeSlotData.value.timeSlots!.isEmpty) {
              return Container(
                height: 50,
                alignment: Alignment.center,
                child: Text(
                  'No Time Slot Found!'.tr,
                  style: h4,
                ),
              );
            } else {
              var timeSlotData = timeSlotController.timeSlotData.value;
              return Container(
                height: 35,
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.symmetric(
                  horizontal: Dimensions.marginSizeDefault,
                  vertical: Dimensions.marginSizeSmall,
                ),
                child: ListView.separated(
                  itemCount: timeSlotData.timeSlots!.length,
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) => GestureDetector(
                    onTap: () {
                      setState(() {
                        selectedTimeIndex = index;
                        selectedTimeSlot = timeSlotData.timeSlots![index];
                      });
                    },
                    child: TimeSlotItem(
                      isSelected: selectedTimeIndex == index ? true : false,
                      starTime: timeSlotData.timeSlots![index].start ?? "",
                      endTime: timeSlotData.timeSlots![index].end ?? "",
                    ),
                  ),
                  separatorBuilder: (context, index) => addHorizontalSpace(
                    Dimensions.paddingSizeDefault,
                  ),
                ),
              );
            }
          });
  }

  Widget _buildCustomerDropdown() {
    return Obx(() {
      if (customerListController.isLoading.value) {
        return const KCustomLoader();
      } else if (customerListController.customerListLite.isEmpty) {
        return Container(
          height: 50,
          alignment: Alignment.center,
          child: Text(
            'No customers Found!'.tr,
            style: h4,
          ),
        );
      } else {
        return Padding(
          padding: EdgeInsets.all(Dimensions.paddingSizeDefault),
          child: Row(
            children: [
              Expanded(
                child: DropdownSearch<CustomerData>(
                  popupBarrierColor: Colors.transparent,
                  popupTitle: Container(
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.all(10),
                    alignment: Alignment.center,
                    child: Text(
                      'Select Customer'.tr,
                      style: h3.copyWith(
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  dropdownSearchDecoration: InputDecoration(
                    hintText: 'Select Customer'.tr,
                    prefixIcon: const Icon(Icons.account_circle_rounded),
                    border: const OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.teal),
                    ),
                  ),
                  showSearchBox: true,
                  mode: Mode.BOTTOM_SHEET,
                  showSelectedItems: false,
                  items: customerListController.customerListLite,
                  itemAsString: (CustomerData? customerData) =>
                  customerData!.name!,
                  onChanged: (value) async {
                    setState(() {
                      selectedCustomer = value;
                    });
                  },
                  selectedItem: selectedCustomer,
                ),
              ),
              addHorizontalSpace(Dimensions.paddingSizeSmall),
              GestureDetector(
                onTap: () {

                  /// clear controller and focusNode
                  customerNameController.clear();
                  customerNameFocusNode = FocusNode();
                  customerNameFocusNode.requestFocus();

                  customDialog(
                    context: context,
                    title: 'Add new customer'.tr,
                    content: Form(
                      key: addCustomerFormKey,
                      child: KTextFormFiled(
                        controller: customerNameController,
                        focusNode: customerNameFocusNode,
                        labelText: 'Customer Name'.tr,
                        inputAction: TextInputAction.done,
                        inputType: TextInputType.text,
                        validator: (value) {
                          if (value.toString().isEmpty) {
                            return Message.emptyName.tr;
                          }
                          return null;
                        },
                      ),
                    ),
                    actions: [
                      KOutlinedButton(
                        onPressed: () => Navigator.pop(context),
                        width: MediaQuery.of(context).size.width * 0.4,
                        borderColor: kGrey,
                        child: Text(
                          'Cancel'.tr,
                          style: h3.copyWith(
                            fontWeight: FontWeight.bold,
                            color: kGrey,
                          ),
                        ),
                      ),
                      KButton(
                        onPressed: () async{
                          if(addCustomerFormKey.currentState!.validate()){
                            await customerListController.addNewCustomer(name: customerNameController.text);
                            await customerListController.getCustomerListLite();
                            if(mounted) Navigator.pop(context);
                          }
                        },
                        width: MediaQuery.of(context).size.width * 0.4,
                        child: Text(
                          'Save'.tr,
                          style: GoogleFonts.roboto(
                            textStyle: h3.copyWith(
                              color: kWhite,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ),
                    ],
                    dialogPosition: Alignment.bottomCenter,
                  );
                },
                child: Container(
                  decoration: BoxDecoration(
                    color: mainColor,
                    shape: BoxShape.circle,
                  ),
                  height: 50,
                  width: 50,
                  child: Icon(Icons.add, color: kWhite),
                ),
              ),
            ],
          ),
        );
      }
    });
  }

  Widget _buildFinalAppointment() {
    return selectedBarberIndex == null && selectedTimeIndex == null
        ? Container()
        : selectedTimeIndex == null
            ? Container(
                height: 50,
                alignment: Alignment.center,
                child: Text(
                  'Select a time slot!'.tr,
                  style: h3,
                ),
              )
            : Container(
                margin: EdgeInsets.symmetric(
                  horizontal: Dimensions.marginSizeDefault,
                  vertical: Dimensions.marginSizeSmall,
                ),
                padding: EdgeInsets.all(
                  Dimensions.paddingSizeDefault,
                ),
                decoration: BoxDecoration(
                    color: kGreyMedium.withOpacity(0.08),
                    borderRadius: BorderRadius.circular(12)),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Text(
                            widget.service.title ?? "",
                            style: h2.copyWith(
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                        addHorizontalSpace(Dimensions.paddingSizeSmall),
                        Column(
                          children: [
                            Text(
                              "${widget.service.price} ${AppConstants.currencySymbol}",
                              style: h2.copyWith(
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            Text(
                              selectedTimeSlot!.start!.substring(0, 5),
                              style: h4,
                            ),
                          ],
                        ),
                      ],
                    ),
                    const KDivider(),
                    Row(
                      children: [
                        Text(
                          "${'Staff'.tr}:",
                          style: h3.copyWith(
                            color: kGreyTextColor,
                          ),
                        ),
                        KProfileImage(
                          height: 30,
                          width: 30,
                          imgURL: Api.getImageURL(selectedBarber!.avatar),
                          margin: EdgeInsets.symmetric(
                            horizontal: Dimensions.marginSizeSmall,
                          ),
                        ),
                        Text(
                          selectedBarber!.name ?? "",
                          style: h4,
                        ),
                      ],
                    ),
                  ],
                ),
              );
  }

  void openBookingConfirmDialog(BuildContext context) {
    AdminAppointmentBookingSheet appointmentBookingSheet = AdminAppointmentBookingSheet(
      date: selectedDay,
      time: selectedTimeSlot!.start.toString().minuteAndSecond,
      barberId: selectedBarber!.id!,
      timeSlotId: selectedTimeSlot!.id!,
      serviceId: widget.service.id!,
      serviceCharge: widget.service.price!,
      customerId: selectedCustomer!.id,
    );

    appointmentBookingSheet.open(context);
  }
}
