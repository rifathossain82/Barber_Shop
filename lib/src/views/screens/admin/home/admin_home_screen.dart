import 'package:barber_shop/src/controllers/admin/admin_appointment_controller.dart';
import 'package:barber_shop/src/controllers/common/barber_list_controller.dart';
import 'package:barber_shop/src/services/extensions/date_time_extension.dart';
import 'package:barber_shop/src/utils/dimensions.dart';
import 'package:barber_shop/src/utils/styles.dart';
import 'package:barber_shop/src/views/base/barber_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:table_calendar/table_calendar.dart';

import '../../../../controllers/common/holiday_controller.dart';
import '../../../../models/common/barber_data.dart';
import '../../../../network/api.dart';
import '../../../base/booked_appointment_item.dart';
import '../../../base/calendar_widget.dart';
import '../../../base/helper.dart';
import '../../../base/k_custom_loader.dart';
import '../../../base/no_data_found.dart';
import '../../../base/unbooked_appointment_item.dart';
import '../appointment_details/admin_appointment_details.dart';

class AdminHomeScreen extends StatefulWidget {
  const AdminHomeScreen({Key? key}) : super(key: key);

  @override
  State<AdminHomeScreen> createState() => _AdminHomeScreenState();
}

class _AdminHomeScreenState extends State<AdminHomeScreen> {
  int? selectedBarberIndex;
  Barber? selectedBarber;

  late DateTime selectedDay;
  late DateTime focusDay;
  List<int> weekendDays = [];
  List<DateTime> holidays = [];

  final barberController = Get.put(BarberListController());
  final adminAppointmentController = Get.put(AdminAppointmentController());
  final holidayController = Get.put(HolidayController());

  @override
  void initState() {
    selectedDay = DateTime.now();
    focusDay = DateTime.now();
    setHolidaysAndWeekend();
    super.initState();
  }

  void setHolidaysAndWeekend()async{
    await holidayController.getHolidayList();
    /// set weekend
    for (var element in holidayController.holidays.value.weeklyHoliday!) {
      weekendDays.add(int.parse(element));
    }

    /// set holiday
    for (var element in holidayController.holidays.value.publicHolidays!) {
      var day = DateTime.parse(element);
      holidays.add(DateTime.utc(day.year, day.month, day.day));
    }

    /// set selected day in the next day of all weekend day and holiday
    for (var element in holidays) {
      if(DateFormat('yyyy-MM-dd').format(element) == DateFormat('yyyy-MM-dd').format(selectedDay)){
        selectedDay = selectedDay.add(const Duration(days: 1));
        focusDay = focusDay.add(const Duration(days: 1));
      }
    }

    for(int i=1; i<7; i++){
      if(weekendDays.contains(selectedDay.weekday)){
        selectedDay = selectedDay.add(const Duration(days: 1));
        focusDay = focusDay.add(const Duration(days: 1));
      }
    }

    /// fetch barber list based on selected day then fetch appointment list
    await barberController.getBarberList(date: selectedDay.yyyyMMddWithSlash);
    if(barberController.barberData.value.barbers!.isNotEmpty){
      selectedBarber = barberController.barberData.value.barbers!.first;
      selectedBarberIndex = 0;
      adminAppointmentController.getAdminAppointmentList(
        barberId: selectedBarber!.id!,
        date: selectedDay.yyyyMMddWithSlash,
      );
    }

    if(!mounted){
      return;
    }
    setState(() {});

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Appointments'.tr),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Obx(() {
            return Column(
              children: [
                _buildCalender(),
                _buildBarbers(),
                _buildBookingList(),
              ],
            );
          }),
        ),
      ),
    );
  }

  Widget _buildCalender() {
    return Container(
      margin: EdgeInsets.all(Dimensions.paddingSizeDefault),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(Dimensions.radiusSmall),
      ),
      child: CalendarWidget(
        firstDay: DateTime(2010),
        focusDay: focusDay,
        selectedDay: selectedDay,
        weekendDays: weekendDays,
        holidays: holidays,
        calendarFormat: CalendarFormat.twoWeeks,
        onDaySelected: (currentDay, focusDay_) async{
          if (currentDay != selectedDay) {
            setState(() {
              selectedDay = currentDay;
              focusDay = focusDay_;
              selectedBarber = null;
              selectedBarberIndex = null;
            });

            /// fetch barber list based on selected day then fetch appointment list
            await barberController.getBarberList(date: selectedDay.yyyyMMddWithSlash);
            if(barberController.barberData.value.barbers!.isNotEmpty){
              selectedBarber = barberController.barberData.value.barbers!.first;
              selectedBarberIndex = 0;
              adminAppointmentController.getAdminAppointmentList(
                barberId: selectedBarber!.id!,
                date: selectedDay.yyyyMMddWithSlash,
              );
            }
          }
        },
      ),
    );
  }

  Widget _buildBarbers() {
    if (barberController.isLoading.value) {
      return const KCustomLoader();
    } else if (barberController.barberData.value.barbers!.isEmpty) {
      return Container(
        height: 50,
        alignment: Alignment.center,
        child: Text(
          'No Barber Found!'.tr,
          style: h3,
        ),
      );
    } else {
      final barberData = barberController.barberData.value;
      return Container(
        height: 80,
        alignment: Alignment.centerLeft,
        margin: EdgeInsets.symmetric(
          horizontal: Dimensions.marginSizeDefault,
          vertical: Dimensions.marginSizeSmall,
        ),
        child: ListView.separated(
          itemCount: barberData.barbers!.length,
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) => GestureDetector(
            onTap: () {
              setState(() {
                if (selectedBarberIndex != index) {
                  selectedBarberIndex = index;
                  selectedBarber = barberData.barbers![index];

                  if (selectedBarber != null) {
                    adminAppointmentController.getAdminAppointmentList(
                      barberId: selectedBarber!.id!,
                      date: selectedDay.yyyyMMddWithSlash,
                    );
                  }
                }
              });
            },
            child: BarberProfileItem(
              isSelected: selectedBarberIndex == index ? true : false,
              imgURL: Api.getImageURL(barberData.barbers![index].avatar),
              name: barberData.barbers![index].name ?? "",
            ),
          ),
          separatorBuilder: (context, index) => addHorizontalSpace(
            Dimensions.paddingSizeExtraLarge,
          ),
        ),
      );
    }
  }

  Widget _buildBookingList() {
    return adminAppointmentController.isLoading.value
        ? const KCustomLoader()
        : selectedBarber == null
            ? Container(
                alignment: Alignment.center,
                height: 100,
                child: Text(
                  'Please, Select a barber!'.tr,
                  style: h3,
                ),
              )
            : adminAppointmentController
                    .appointmentData.value.appointmentSchedules!.isEmpty
                ? const NoDataFound()
                : ListView.separated(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: adminAppointmentController
                        .appointmentData.value.appointmentSchedules!.length,
                    padding: EdgeInsets.all(
                      Dimensions.paddingSizeDefault,
                    ),
                    itemBuilder: (context, index) {
                      if (adminAppointmentController.appointmentData.value
                              .appointmentSchedules![index].appointment ==
                          null) {
                        return UnBookedAppointmentItem(
                          data: adminAppointmentController.appointmentData.value
                              .appointmentSchedules![index],
                        );
                      } else {
                        var appointmentSchedules = adminAppointmentController
                            .appointmentData.value.appointmentSchedules![index];
                        return BookedAppointmentItem(
                          data: appointmentSchedules,
                          onTap: () => Get.to(
                            () => AdminAppointmentDetails(
                              data: appointmentSchedules,
                            ),
                          ),
                        );
                      }
                    },
                    separatorBuilder: (context, index) =>
                        addVerticalSpace(Dimensions.paddingSizeLarge),
                  );
  }
}
