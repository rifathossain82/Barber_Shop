import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../controllers/common/service_controller.dart';
import '../../../../utils/color.dart';
import '../../../../utils/dimensions.dart';
import '../../../base/helper.dart';
import '../../../base/k_custom_loader.dart';
import '../../../base/k_search_field.dart';
import '../../../base/no_data_found.dart';
import '../../../base/service_item.dart';

class ServicesScreenForAdmin extends StatefulWidget {
  const ServicesScreenForAdmin({Key? key}) : super(key: key);

  @override
  State<ServicesScreenForAdmin> createState() => _ServicesScreenState();
}

class _ServicesScreenState extends State<ServicesScreenForAdmin> {
  final searchController = TextEditingController();
  final serviceController = Get.put(ServiceController());
  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    serviceController.pageNumber.value = 1;
    serviceController.getServiceList();
    scrollIndicator();
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _searchMethod(String value) {
    DeBouncer.run(() {
      if (value.isNotEmpty) {
        setState(() {
          serviceController.getServiceList(
            serviceName: searchController.text,
          );
        });
      } else {
        serviceController.getServiceList();
      }
    });
  }

  void scrollIndicator() {
    _scrollController.addListener(
      () {
        if (_scrollController.offset >=
                _scrollController.position.maxScrollExtent &&
            !_scrollController.position.outOfRange) {
          kPrint('reach to bottom');
          if (!serviceController.loadedCompleted.value) {
            ++serviceController.pageNumber.value;
            serviceController.getServiceList();
          }
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Services'),
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(50),
          child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: Dimensions.paddingSizeDefault,
            ),
            child: KSearchField(
              controller: searchController,
              hintText: 'Search for service'.tr,
              onChanged: _searchMethod,
            ),
          ),
        ),
      ),
      body: RefreshIndicator(
        onRefresh: ()async{
          searchController.clear();
          _searchMethod("");
        },
        child: Obx(() {
          return serviceController.isLoading.value
              ? const KCustomLoader()
              : serviceController.serviceList.isEmpty
                  ? const NoDataFound()
                  : ListView.separated(
                      controller: _scrollController,
                      physics: const BouncingScrollPhysics(),
                      itemCount: serviceController.serviceList.length + 1,
                      itemBuilder: (context, index) {
                        if (index == serviceController.serviceList.length &&
                            !serviceController.loadedCompleted.value) {
                          return const Center(child: CircularProgressIndicator());
                        } else if (index ==
                                serviceController.serviceList.length &&
                            serviceController.loadedCompleted.value) {
                          return Container();
                        }

                        return ServiceItem(
                          service: serviceController.serviceList[index],
                          userType: UserType.admin,
                        );
                      },
                      separatorBuilder: (context, index) => Divider(
                        color: kDividerColor,
                      ),
                    );
        }),
      ),
    );
  }
}
