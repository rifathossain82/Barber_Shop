import 'package:barber_shop/src/controllers/common/auth_controller.dart';
import 'package:barber_shop/src/services/extensions/string_extension.dart';
import 'package:barber_shop/src/views/base/k_button.dart';
import 'package:barber_shop/src/views/base/k_outlined_button.dart';
import 'package:barber_shop/src/views/base/language_selector_widget.dart';
import 'package:barber_shop/src/views/screens/common/profile/components/profile_image_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../../../routes/route.dart';
import '../../../../utils/color.dart';
import '../../../../utils/dimensions.dart';
import '../../../../utils/styles.dart';
import '../../../base/dashed_line_painter.dart';
import '../../../base/helper.dart';
import 'components/info_card.dart';

class ProfileScreen extends StatelessWidget {
  ProfileScreen({Key? key}) : super(key: key);
  final authController = Get.put(AuthController())..getUserData();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Profile'.tr),
        actions: [
          LanguageSelectorWidget(),
          // IconButton(
          //   onPressed: () {},
          //   icon: Icon(
          //     Icons.notifications,
          //     color: mainColor,
          //   ),
          // ),
        ],
      ),
      body: _buildProfileBody(context),
    );
  }

  Widget _buildProfileBody(BuildContext context) {
    return Obx(() {
      final user = authController.user.value;
      return SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          children: [
            /// profile image
            addVerticalSpace(Dimensions.paddingSizeSmall),
            ProfileImageWidget(authController: authController),

            /// user name
            addVerticalSpace(Dimensions.paddingSizeSmall),
            Text(
              user.name ?? "Not Set",
              style: GoogleFonts.roboto(
                textStyle: h2.copyWith(
                  fontWeight: FontWeight.w700,
                  height: 1.2,
                ),
              ),
            ),

            /// user status
            Text(
              getUserTypeValue(user.userType!).capitalizedFirst,
              style: GoogleFonts.roboto(
                textStyle: h3.copyWith(
                  color: mainColor,
                ),
              ),
            ),
            addVerticalSpace(Dimensions.paddingSizeDefault),

            /// dashedLine
            addVerticalSpace(Dimensions.paddingSizeLarge),
            SizedBox(
              width: Get.width,
              child: CustomPaint(
                painter: DashedLinePainter(),
              ),
            ),

            /// profile info card
            addVerticalSpace(Dimensions.paddingSizeExtraLarge),
            ProfileInfoCard(user: user, authController: authController),

            /// change password button
            addVerticalSpace(25),
            _buildChangePasswordButton(),

            /// logout button
            addVerticalSpace(Dimensions.paddingSizeDefault),
            _buildLogoutButton(context),

            /// padding in bottom
            addVerticalSpace(40),
          ],
        ),
      );
    });
  }

  Widget _buildChangePasswordButton() => Padding(
        padding: EdgeInsets.symmetric(
          horizontal: Dimensions.paddingSizeDefault,
        ),
        child: KOutlinedButton(
          onPressed: () => Get.toNamed(RouteGenerator.changePassword),
          borderColor: mainColor,
          child: Text(
            'Change Password'.tr,
            style: GoogleFonts.roboto(
              textStyle: h2.copyWith(
                color: mainColor,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
        ),
      );

  Widget _buildLogoutButton(BuildContext context) => Padding(
        padding: EdgeInsets.symmetric(
          horizontal: Dimensions.paddingSizeDefault,
        ),
        child: KButton(
          onPressed: () async {
            bool? result = await kConfirmDialog(
              context: context,
              title: 'Are you sure you want to logout?'.tr,
              negativeActionText: 'Cancel'.tr,
              positiveActionText: 'Logout'.tr,
            );

            if (result!) {
              authController.logout();
            }
          },
          child: authController.isLoading.value
              ? Container(
                  height: 20,
                  width: 20,
                  alignment: Alignment.center,
                  child: CircularProgressIndicator(
                    color: kWhite,
                  ),
                )
              : Text(
                  'Logout'.tr,
                  style: GoogleFonts.roboto(
                    textStyle: h2.copyWith(
                      color: kWhite,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
        ),
      );
}
