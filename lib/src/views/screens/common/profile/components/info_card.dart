import 'package:barber_shop/src/controllers/common/auth_controller.dart';
import 'package:barber_shop/src/services/extensions/build_context_extension.dart';
import 'package:barber_shop/src/views/base/k_divider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../models/common/user_data_model.dart';
import '../../../../../utils/color.dart';
import '../../../../../utils/dimensions.dart';
import '../../../../../utils/styles.dart';
import '../../../../base/helper.dart';
import '../../../../base/k_button.dart';
import '../../../../base/k_text_form_field.dart';
import 'info_item.dart';

class ProfileInfoCard extends StatelessWidget {
  final UserData user;
  final AuthController authController;

  ProfileInfoCard({
    Key? key,
    required this.user,
    required this.authController,
  }) : super(key: key);

  final nameController = TextEditingController();
  final phoneController = TextEditingController();
  final nameFocusNode = FocusNode();
  final phoneFocusNode = FocusNode();
  final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: Dimensions.paddingSizeDefault,
      ),
      child: Container(
        width: Get.width,
        padding: EdgeInsets.all(Dimensions.paddingSizeDefault),
        decoration: BoxDecoration(
          color: kWhite,
          borderRadius: BorderRadius.circular(Dimensions.radiusSmall),
          boxShadow: [
            BoxShadow(
              offset: const Offset(0, 0),
              blurRadius: 4,
              spreadRadius: 0,
              color: kItemShadowColor,
            ),
          ],
        ),
        child: Column(
          children: [
            ProfileInfoItem(
              title: 'Name'.tr,
              data: user.name ?? 'Not Set',
              onPressed: () {
                openNameEditingDialog(context);
                nameFocusNode.requestFocus();
              },
              isEditable: true,
            ),
            KDivider(height: 26, color: kGreyMedium),
            ProfileInfoItem(
              title: 'Phone Number'.tr,
              data: user.phone ?? 'Not Set',
              onPressed: () {
                openPhoneEditingDialog(context);
                phoneFocusNode.requestFocus();
              },
              isEditable: true,
            ),
            KDivider(height: 26, color: kGreyMedium),
            ProfileInfoItem(
              title: 'Email'.tr,
              data: user.email ?? 'Not Set',
              onPressed: null,
              isEditable: false,
            ),
          ],
        ),
      ),
    );
  }

  void openNameEditingDialog(BuildContext context) {
    customDialog(
      context: context,
      title: "Update your name".tr,
      content: Form(
        key: formKey,
        child: _buildNameTextFormField(),
      ),
      dialogPosition: Alignment.bottomCenter,
      actions: [
        KButton(
          onPressed: () {
            /// to unFocus keyboard
            context.unFocusKeyboard;
            if (formKey.currentState!.validate()) {
              Navigator.pop(context);
              authController.updateUserData(name: nameController.text);
              nameController.clear();
            }
          },
          width: MediaQuery.of(context).size.width * 0.8,
          bgColor: mainColor,
          child: Text(
            'Save'.tr,
            style: h3.copyWith(
              fontWeight: FontWeight.bold,
              color: kWhite,
            ),
          ),
        ),
      ],
    );
  }

  void openPhoneEditingDialog(BuildContext context) {
    customDialog(
      context: context,
      title: "Update you phone".tr,
      content: Form(
        key: formKey,
        child: _buildPhoneTextFormField(),
      ),
      dialogPosition: Alignment.bottomCenter,
      actions: [
        KButton(
          onPressed: () {
            /// to unFocus keyboard
            context.unFocusKeyboard;
            if (formKey.currentState!.validate()) {
              Navigator.pop(context);
              authController.updateUserData(phone: phoneController.text);
              phoneController.clear();
            }
          },
          width: MediaQuery.of(context).size.width * 0.8,
          bgColor: mainColor,
          child: Text(
            'Save'.tr,
            style: h3.copyWith(
              fontWeight: FontWeight.bold,
              color: kWhite,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildNameTextFormField() {
    return KTextFormFiled(
      controller: nameController,
      focusNode: nameFocusNode,
      labelText: 'Name'.tr,
      hintText: user.name,
      inputType: TextInputType.name,
      inputAction: TextInputAction.done,
      validator: (value) {
        if (value.toString().isEmpty) {
          return Message.emptyName.tr;
        }
        return null;
      },
    );
  }

  Widget _buildPhoneTextFormField() {
    return KTextFormFiled(
      controller: phoneController,
      focusNode: phoneFocusNode,
      labelText: 'Phone'.tr,
      hintText: user.phone,
      inputType: TextInputType.phone,
      inputAction: TextInputAction.done,
      validator: (value) {
        if (value.toString().isEmpty) {
          return Message.emptyName.tr;
        }
        return null;
      },
    );
  }

}
