import 'package:barber_shop/src/views/base/k_logo.dart';
import 'package:barber_shop/src/views/screens/common/auth/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../../../utils/asset_path.dart';
import '../../../../../utils/color.dart';
import '../../../../../utils/dimensions.dart';
import '../../../../../utils/styles.dart';
import '../../../../base/helper.dart';
import '../../../../base/k_button.dart';

Future loginTypeDialog(BuildContext context) {
  return showDialog(
    context: context,
    barrierColor: kWhite,
    builder: (context) {
      return AlertDialog(
        backgroundColor: kWhite,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(Dimensions.radiusDefault),
        ),
        icon: const KLogo(),
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Login as a',
              textAlign: TextAlign.center,
              style: GoogleFonts.poppins(
                textStyle: h1.copyWith(
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
            addHorizontalSpace(Dimensions.paddingSizeDefault),
            Icon(
              Icons.login,
              color: kBlackLight,
              size: 30,
            ),
          ],
        ),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            KButton(
              onPressed: () => Get.offAll(
                () => const LoginScreen(userType: UserType.customer),
              ),
              borderRadius: 100,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    AssetPath.customer,
                    color: kWhite,
                  ),
                  addHorizontalSpace(Dimensions.paddingSizeDefault),
                  Text(
                    'Customer',
                    style: h4.copyWith(
                      color: kWhite,
                    ),
                  ),
                ],
              ),
            ),
            addVerticalSpace(Dimensions.paddingSizeExtraSmall),
            Text(
              'OR',
              style: h4.copyWith(
                color: kGreyTextColor,
              ),
            ),
            addVerticalSpace(Dimensions.paddingSizeExtraSmall),
            KButton(
              onPressed: () => Get.offAll(
                () => const LoginScreen(userType: UserType.barber),
              ),
              borderRadius: 100,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    AssetPath.barber,
                    color: kWhite,
                  ),
                  addHorizontalSpace(Dimensions.paddingSizeDefault),
                  Text(
                    'Barber',
                    style: h4.copyWith(
                      color: kWhite,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    },
  );
}


/*
 return Stack(
        alignment: Alignment.center,
        children: [
          AlertDialog(
            backgroundColor: kWhite,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(Dimensions.radiusDefault),
            ),
            // icon: const KLogo(),
            titlePadding: EdgeInsets.only(top: 80, bottom: 15),
            title: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Login as a',
                  textAlign: TextAlign.center,
                  style: GoogleFonts.poppins(
                    textStyle: h1.copyWith(
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
                addHorizontalSpace(Dimensions.paddingSizeDefault),
                Icon(
                  Icons.login,
                  color: kBlackLight,
                  size: 30,
                ),
              ],
            ),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                KButton(
                  onPressed: () => Get.offAll(
                    () => LoginScreen(userType: UserType.customer),
                  ),
                  borderRadius: 100,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        AssetPath.customer,
                        color: kWhite,
                      ),
                      addHorizontalSpace(Dimensions.paddingSizeDefault),
                      Text(
                        'Customer',
                        style: h4.copyWith(
                          color: kWhite,
                        ),
                      ),
                    ],
                  ),
                ),
                addVerticalSpace(Dimensions.paddingSizeExtraSmall),
                Text(
                  'OR',
                  style: h4.copyWith(
                    color: kGreyTextColor,
                  ),
                ),
                addVerticalSpace(Dimensions.paddingSizeExtraSmall),
                KButton(
                  onPressed: () => Get.offAll(
                    () => LoginScreen(userType: UserType.barber),
                  ),
                  borderRadius: 100,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        AssetPath.barber,
                        color: kWhite,
                      ),
                      addHorizontalSpace(Dimensions.paddingSizeDefault),
                      Text(
                        'Barber',
                        style: h4.copyWith(
                          color: kWhite,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            top: MediaQuery.of(context).size.height * 0.18,
            child: const KLogo(),
          )
        ],
      );
 */
