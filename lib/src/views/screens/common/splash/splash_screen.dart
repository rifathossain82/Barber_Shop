import 'dart:async';

import 'package:barber_shop/routes/route.dart';
import 'package:barber_shop/src/services/local_storage.dart';
import 'package:barber_shop/src/views/base/helper.dart';
import 'package:barber_shop/src/views/base/k_logo.dart';
import 'package:barber_shop/src/views/screens/common/auth/login_screen_2.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../controllers/customers/shop_details_controller.dart';
import '../auth/login_screen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  late AnimationController animationController;
  late Animation<double> animation;

  setSplashDuration() async {
    return Timer(
      const Duration(seconds: 3),
      () => pageNavigation(),
    );
  }

  void pageNavigation() async {
    final token = LocalStorage.getData(key: LocalStorageKey.token);
    final userType = LocalStorage.getData(key: LocalStorageKey.userType);

    if (token != null && userType != null) {
      if (userType == UserType.customer.name) {
        Get.offAllNamed(RouteGenerator.customerDashboard);
      } else if (userType == UserType.barber.name) {
        Get.put(ShopDetailsController());
        Get.offAllNamed(RouteGenerator.barberDashboard);
      } else {
        Get.put(ShopDetailsController());
        Get.offAllNamed(RouteGenerator.adminDashboard);
      }
    } else {
      Get.offAll(() => const LoginScreen());
    }
  }

  @override
  void initState() {
    setSplashDuration();

    animationController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 2),
    );

    animation = CurvedAnimation(
      parent: animationController,
      curve: Curves.fastOutSlowIn,
    );

    animation.addListener(() => setState(() {}));
    animationController.forward();

    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: [
          // Column(
          //   mainAxisAlignment: MainAxisAlignment.end,
          //   mainAxisSize: MainAxisSize.min,
          //   children: [
          //     Padding(
          //       padding: EdgeInsets.only(
          //         bottom: Dimensions.paddingSizeExtraLarge,
          //       ),
          //       child: RichText(
          //         text: TextSpan(
          //           children: [
          //             TextSpan(
          //               text: 'Powered by ',
          //               style: GoogleFonts.roboto(
          //                 textStyle: h3.copyWith(
          //                   color: kGrey,
          //                 ),
          //               ),
          //             ),
          //             TextSpan(
          //               text: AppConstants.appName,
          //               style: GoogleFonts.roboto(
          //                 textStyle: h3.copyWith(
          //                   color: kGrey,
          //                   fontWeight: FontWeight.w500,
          //                 ),
          //               ),
          //             ),
          //           ],
          //         ),
          //       ),
          //     ),
          //   ],
          // ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              KLogo(
                height: animation.value * 150,
                width: animation.value * 150,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
