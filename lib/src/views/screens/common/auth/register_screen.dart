import 'package:barber_shop/src/controllers/common/auth_controller.dart';
import 'package:barber_shop/src/models/common/country_code_data.dart';
import 'package:barber_shop/src/services/extensions/build_context_extension.dart';
import 'package:barber_shop/src/services/extensions/string_extension.dart';
import 'package:barber_shop/src/views/screens/common/auth/login_screen.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../../utils/color.dart';
import '../../../../utils/dimensions.dart';
import '../../../../utils/styles.dart';
import '../../../base/helper.dart';
import '../../../base/k_button.dart';
import '../../../base/k_logo.dart';
import '../../../base/k_text_form_field.dart';
import '../../../base/select_country_code.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen>
    with SingleTickerProviderStateMixin {
  final TextEditingController nameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  final _registerFormKey = GlobalKey<FormState>();
  final authController = Get.put(AuthController());
  bool passwordVisibility = true;
  Countries selectedCountry = Countries(
    iso: "FR",
    name: "FRANCE",
    niceName: "France",
    iso3: "FRA",
    numCode: 250,
    phoneCode: 33,
  );

  var deviceTokenToSendPushNotification = '';

  Future<void> getDeviceTokenToSendNotification() async {
    final FirebaseMessaging fcm = FirebaseMessaging.instance;
    final token = await fcm.getToken();
    deviceTokenToSendPushNotification = token.toString();
    kPrint("Token Value $deviceTokenToSendPushNotification");
  }

  @override
  void initState() {
    getDeviceTokenToSendNotification();
    super.initState();
  }

  @override
  void dispose() {
    nameController.dispose();
    emailController.dispose();
    phoneController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: Dimensions.paddingSizeDefault,
          ),
          child: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Form(
              key: _registerFormKey,
              child: Column(
                children: [
                  /// logo and loginWithPassword text
                  addVerticalSpace(Get.height * 0.03),
                  const KLogo(),
                  Text(
                    'Create A New Account'.tr,
                    style: GoogleFonts.roboto(
                      textStyle: h4.copyWith(
                        fontWeight: FontWeight.w300,
                        color: kBlackLight,
                      ),
                    ),
                  ),

                  /// name textField
                  addVerticalSpace(Get.height * 0.03),
                  _buildNameTextFiled(),

                  /// email textField
                  addVerticalSpace(Get.height * 0.02),
                  _buildEmailTextFiled(),

                  /// phone textField
                  addVerticalSpace(Get.height * 0.02),
                  _buildPhoneTextFiled(),

                  /// password textField
                  addVerticalSpace(Get.height * 0.02),
                  _buildPasswordTextFiled(),

                  /// login button
                  addVerticalSpace(Get.height * 0.05),
                  _buildRegisterButton(context),

                  /// already have an account , login text
                  addVerticalSpace(Get.height * 0.02),
                  _buildGoToLoginText(),
                  addVerticalSpace(Get.height * 0.03),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildNameTextFiled() => KTextFormFiled(
        controller: nameController,
        labelText: 'Name'.tr,
        inputAction: TextInputAction.next,
        inputType: TextInputType.name,
        validator: (value) {
          if (value.toString().isEmpty) {
            return Message.emptyName.tr;
          }
          return null;
        },
      );

  Widget _buildEmailTextFiled() => KTextFormFiled(
        controller: emailController,
        labelText: 'Email'.tr,
        inputAction: TextInputAction.next,
        inputType: TextInputType.emailAddress,
        validator: (value) {
          if (value.toString().isEmpty) {
            return Message.emptyEmail.tr;
          } else if (!value.toString().isValidEmail) {
            return Message.invalidEmail.tr;
          }
          return null;
        },
      );

  Widget _buildPhoneTextFiled() => KTextFormFiled(
        controller: phoneController,
        labelText: 'Phone'.tr,
        inputAction: TextInputAction.next,
        inputType: TextInputType.phone,
        validator: (value) {
          if (value.toString().isEmpty) {
            return Message.emptyPhone.tr;
          } else if (!value.toString().isValidPhone) {
            return Message.invalidPhone.tr;
          }
          return null;
        },
        prefixIcon: GestureDetector(
          onTap: () async {
            Countries countries =
                await selectCountryCode(context) ?? Countries();
            if (countries.phoneCode != null) {
              setState(() {
                selectedCountry = countries;
                kPrint(selectedCountry.iso);
              });
            }
          },
          child: Container(
            padding: const EdgeInsets.only(left: 2, right: 8),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text('${selectedCountry.iso} ${selectedCountry.phoneCode}'),
                const SizedBox(
                  width: 4,
                ),
                Icon(
                  Icons.arrow_drop_down,
                  color: Colors.black.withOpacity(0.8),
                ),
              ],
            ),
          ),
        ),
      );

  Widget _buildPasswordTextFiled() => KTextFormFiled(
        controller: passwordController,
        labelText: 'Password'.tr,
        inputAction: TextInputAction.done,
        inputType: TextInputType.visiblePassword,
        obscureValue: passwordVisibility,
        suffix: GestureDetector(
          onTap: () {
            setState(() {
              passwordVisibility = !passwordVisibility;
            });
          },
          child: Icon(
            passwordVisibility ? Icons.visibility_off : Icons.visibility,
            color: kBlackLight,
          ),
        ),
        validator: (value) {
          if (value.toString().isEmpty) {
            return Message.emptyPassword.tr;
          } else if (value.toString().length < 6) {
            return Message.invalidPassword.tr;
          }
          return null;
        },
      );

  Widget _buildRegisterButton(BuildContext context) => KButton(
        onPressed: () => _registerMethod(context),
        child: Obx(
          () {
            return authController.isLoading.value
                ? Container(
                    height: 20,
                    width: 20,
                    alignment: Alignment.center,
                    child: CircularProgressIndicator(
                      color: kWhite,
                    ),
                  )
                : Text(
                    'Register'.tr,
                    style: GoogleFonts.roboto(
                      textStyle: h3.copyWith(
                        color: kWhite,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  );
          },
        ),
      );

  Widget _buildGoToLoginText() => InkWell(
        borderRadius: BorderRadius.circular(50),
        onTap: () {
          Get.offAll(() => const LoginScreen(userType: UserType.customer));
        },
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: Dimensions.paddingSizeSmall,
            vertical: Dimensions.paddingSizeExtraSmall,
          ),
          child: Column(
            children: [
              Text(
                "Already have an Account?".tr,
                style: h5,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    "Login".tr,
                    style: h4.copyWith(
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  addHorizontalSpace(5),
                  const Icon(
                    Icons.arrow_forward,
                    size: 20,
                  )
                ],
              ),
            ],
          ),
        ),
      );

  void _registerMethod(BuildContext context) {
    context.unFocusKeyboard();
    if (_registerFormKey.currentState!.validate()) {
      authController.register(
        name: nameController.text.trim(),
        email: emailController.text.trim(),
        phone: phoneController.text.trim(),
        password: passwordController.text.trim(),
        userType: UserType.customer.name,
        deviceToken: deviceTokenToSendPushNotification,
      );
    }
  }
}
