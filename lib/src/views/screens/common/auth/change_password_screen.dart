import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../../controllers/common/auth_controller.dart';
import '../../../../utils/color.dart';
import '../../../../utils/dimensions.dart';
import '../../../../utils/styles.dart';
import '../../../base/helper.dart';
import '../../../base/k_button.dart';
import '../../../base/k_logo.dart';
import '../../../base/k_text_form_field.dart';

class ChangePasswordScreen extends StatefulWidget {
  const ChangePasswordScreen({Key? key}) : super(key: key);

  @override
  State<ChangePasswordScreen> createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {
  final TextEditingController oldPassword = TextEditingController();
  final TextEditingController newPassword = TextEditingController();
  final TextEditingController confirmPassword = TextEditingController();

  final formKey = GlobalKey<FormState>();
  final authController = Get.put(AuthController());

  bool oldPasswordVisibility = true;
  bool passwordVisibility = true;
  bool confirmPasswordVisibility = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Padding(
        padding: EdgeInsets.all(Dimensions.paddingSizeDefault),
        child: SingleChildScrollView(
          child: Obx(() {
            return Form(
              key: formKey,
              child: Column(
                children: [
                  /// logo and loginWithPassword text
                  const KLogo(),
                  Text(
                    'Change Password',
                    style: GoogleFonts.roboto(
                      textStyle: h4.copyWith(
                        fontWeight: FontWeight.w300,
                        color: kBlackLight,
                      ),
                    ),
                  ),

                  /// old textField
                  addVerticalSpace(Get.height * 0.05),
                  _buildOldPasswordTextFiled(),

                  /// new password textField
                  addVerticalSpace(Get.height * 0.03),
                  _buildNewPasswordTextFiled(),

                  /// confirm password textField
                  addVerticalSpace(Get.height * 0.03),
                  _buildConfirmPasswordTextFiled(),

                  /// change password button
                  addVerticalSpace(Get.height * 0.05),
                  _buildChangePasswordButton(context),
                ],
              ),
            );
          }),
        ),
      ),
    );
  }

  Widget _buildOldPasswordTextFiled() => KTextFormFiled(
        controller: oldPassword,
        validator: (value) {
          if (value.toString().isEmpty) {
            return Message.emptyOldPassword;
          } else if (value.toString().length < 6) {
            return Message.invalidPassword;
          }
          return null;
        },
        labelText: 'Old Password',
        inputAction: TextInputAction.next,
        obscureValue: oldPasswordVisibility,
        suffix: GestureDetector(
          onTap: () {
            setState(() {
              oldPasswordVisibility = !oldPasswordVisibility;
            });
          },
          child: Icon(
            oldPasswordVisibility ? Icons.visibility_off : Icons.visibility,
            color: kBlackLight,
          ),
        ),
      );

  Widget _buildNewPasswordTextFiled() => KTextFormFiled(
        controller: newPassword,
        validator: (value) {
          if (value.toString().isEmpty) {
            return Message.emptyNewPassword;
          } else if (value.toString().length < 6) {
            return Message.invalidPassword;
          }
          return null;
        },
        labelText: 'New Password',
        inputAction: TextInputAction.next,
        obscureValue: passwordVisibility,
        suffix: GestureDetector(
          onTap: () {
            setState(() {
              passwordVisibility = !passwordVisibility;
            });
          },
          child: Icon(
            passwordVisibility ? Icons.visibility_off : Icons.visibility,
            color: kBlackLight,
          ),
        ),
      );

  Widget _buildConfirmPasswordTextFiled() => KTextFormFiled(
        controller: confirmPassword,
        validator: (value) {
          if (value.toString().isEmpty) {
            return Message.emptyConfirmPassword;
          } else if (value.toString().length < 6) {
            return Message.invalidPassword;
          }
          return null;
        },
        labelText: 'Confirm Password',
        inputAction: TextInputAction.done,
        obscureValue: confirmPasswordVisibility,
        suffix: GestureDetector(
          onTap: () {
            setState(() {
              confirmPasswordVisibility = !confirmPasswordVisibility;
            });
          },
          child: Icon(
            confirmPasswordVisibility ? Icons.visibility_off : Icons.visibility,
            color: kBlackLight,
          ),
        ),
      );

  Widget _buildChangePasswordButton(BuildContext context) => KButton(
        onPressed: () => _changePassword(context),
        child: authController.isLoading.value
            ? Container(
                height: 20,
                width: 20,
                alignment: Alignment.center,
                child: CircularProgressIndicator(
                  color: kWhite,
                ),
              )
            : Text(
                'Change Password',
                style: GoogleFonts.roboto(
                  textStyle: h3.copyWith(
                    color: kWhite,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
      );

  void _changePassword(BuildContext context) async {
    if(formKey.currentState!.validate()){
      if (newPassword.text != confirmPassword.text) {
        kSnackBar(
          message: 'The password confirmation does not match!',
          bgColor: failedColor,
        );
      } else {
        bool? result = await kConfirmDialog(
          context: context,
          title: 'Are you sure you want to change password?',
          negativeActionText: 'Cancel',
          positiveActionText: 'Change',
        );

        if (result!) {
          authController.changePassword(
            oldPassword: oldPassword.text.trim(),
            newPassword: newPassword.text.trim(),
            confirmPassword: confirmPassword.text.trim(),
          );
        }
      }
    }
  }
}
