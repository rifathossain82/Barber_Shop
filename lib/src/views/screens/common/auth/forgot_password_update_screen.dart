import 'package:barber_shop/src/controllers/common/auth_controller.dart';
import 'package:barber_shop/src/services/extensions/build_context_extension.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../../utils/color.dart';
import '../../../../utils/dimensions.dart';
import '../../../../utils/styles.dart';
import '../../../base/helper.dart';
import '../../../base/k_button.dart';
import '../../../base/k_logo.dart';
import '../../../base/k_text_form_field.dart';

class ForgotPasswordUpdateScreen extends StatefulWidget {
  final String email;

  const ForgotPasswordUpdateScreen({Key? key, required this.email})
      : super(key: key);

  @override
  State<ForgotPasswordUpdateScreen> createState() =>
      _ForgotPasswordUpdateScreenState();
}

class _ForgotPasswordUpdateScreenState
    extends State<ForgotPasswordUpdateScreen> {
  final otpTextController = TextEditingController();
  final passwordTextController = TextEditingController();
  final confirmPasswordTextController = TextEditingController();

  bool passwordVisibility = true;
  bool confirmPasswordVisibility = true;

  final authController = Get.put(AuthController());
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: Dimensions.paddingSizeDefault,
        ),
        child: Obx(() {
          return SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  /// logo and text
                  const KLogo(),
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: Dimensions.paddingSizeLarge,
                    ),
                    child: Text(
                      "${'Check your mail. The code is already gone to'.tr} ${widget.email}",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.roboto(
                        textStyle: h4.copyWith(
                          fontWeight: FontWeight.w300,
                          color: kBlackLight,
                        ),
                      ),
                    ),
                  ),

                  /// otp textField
                  addVerticalSpace(Get.height * 0.03),
                  _buildOTPTextFiled(),

                  /// password textField
                  addVerticalSpace(Get.height * 0.02),
                  _buildPasswordTextFiled(),

                  /// confirm password textField
                  addVerticalSpace(Get.height * 0.02),
                  _buildConfirmPasswordTextFiled(),

                  /// send otp button
                  addVerticalSpace(Get.height * 0.05),
                  _buildSendOTPButton(context),
                ],
              ),
            ),
          );
        }),
      ),
    );
  }

  Widget _buildOTPTextFiled() => KTextFormFiled(
        controller: otpTextController,
        labelText: 'OTP'.tr,
        inputAction: TextInputAction.next,
        inputType: TextInputType.number,
        validator: (value) {
          if (value.toString().isEmpty) {
            return Message.emptyOTP.tr;
          } else if (value.toString().length != 6) {
            return Message.invalidOTP.tr;
          }
          return null;
        },
      );

  Widget _buildPasswordTextFiled() => KTextFormFiled(
        controller: passwordTextController,
        labelText: 'Password'.tr,
        inputAction: TextInputAction.next,
        inputType: TextInputType.visiblePassword,
        obscureValue: passwordVisibility,
        suffix: GestureDetector(
          onTap: () {
            setState(() {
              passwordVisibility = !passwordVisibility;
            });
          },
          child: Icon(
            passwordVisibility ? Icons.visibility_off : Icons.visibility,
            color: kBlackLight,
          ),
        ),
        validator: (value) {
          if (value.toString().isEmpty) {
            return Message.emptyPassword.tr;
          } else if (value.toString().length < 6) {
            return Message.invalidPassword.tr;
          }
          return null;
        },
      );

  Widget _buildConfirmPasswordTextFiled() => KTextFormFiled(
        controller: confirmPasswordTextController,
        labelText: 'Password'.tr,
        inputAction: TextInputAction.done,
        inputType: TextInputType.visiblePassword,
        obscureValue: confirmPasswordVisibility,
        suffix: GestureDetector(
          onTap: () {
            setState(() {
              confirmPasswordVisibility = !confirmPasswordVisibility;
            });
          },
          child: Icon(
            confirmPasswordVisibility ? Icons.visibility_off : Icons.visibility,
            color: kBlackLight,
          ),
        ),
        validator: (value) {
          if (value.toString().isEmpty) {
            return Message.emptyPassword.tr;
          } else if (value.toString().length < 6) {
            return Message.invalidPassword.tr;
          }
          return null;
        },
      );

  Widget _buildSendOTPButton(BuildContext context) => KButton(
        onPressed: () => _sendOTPMethod(context),
        child: authController.isLoading.value
            ? Container(
                height: 20,
                width: 20,
                alignment: Alignment.center,
                child: CircularProgressIndicator(
                  color: kWhite,
                ),
              )
            : Text(
                'Update'.tr,
                style: GoogleFonts.roboto(
                  textStyle: h3.copyWith(
                    color: kWhite,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
      );

  void _sendOTPMethod(BuildContext context) {
    context.unFocusKeyboard();
    if (passwordTextController.text != confirmPasswordTextController.text) {
      kSnackBar(
        message: 'The password confirmation does not match!',
        bgColor: failedColor,
      );
    } else {
      if (_formKey.currentState!.validate()) {
        authController.forgotPasswordUpdate(
          otp: otpTextController.text,
          email: widget.email,
          password: passwordTextController.text,
          confirmPassword: confirmPasswordTextController.text,
        );
      }
    }
  }
}
