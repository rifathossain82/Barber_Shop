import 'package:barber_shop/routes/route.dart';
import 'package:barber_shop/src/controllers/common/auth_controller.dart';
import 'package:barber_shop/src/controllers/common/locale_controller.dart';
import 'package:barber_shop/src/services/extensions/build_context_extension.dart';
import 'package:barber_shop/src/services/extensions/string_extension.dart';
import 'package:barber_shop/src/views/base/language_selector_widget.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../../utils/color.dart';
import '../../../../utils/dimensions.dart';
import '../../../../utils/styles.dart';
import '../../../base/helper.dart';
import '../../../base/k_button.dart';
import '../../../base/k_logo.dart';
import '../../../base/k_text_form_field.dart';
import 'forgot_password_screen.dart';

class LoginScreen extends StatefulWidget {
  final UserType userType;

  const LoginScreen({Key? key, this.userType = UserType.customer}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  bool passwordVisibility = true;
  final _loginFormKey = GlobalKey<FormState>();
  final authController = Get.put(AuthController());
  final localeController = Get.put(LocaleController());

  var deviceTokenToSendPushNotification = '';

  Future<void> getDeviceTokenToSendNotification() async {
    final FirebaseMessaging fcm = FirebaseMessaging.instance;
    final token = await fcm.getToken();
    deviceTokenToSendPushNotification = token.toString();
    kPrint("Token Value $deviceTokenToSendPushNotification");
  }

  @override
  void initState() {
    getDeviceTokenToSendNotification();
    super.initState();
  }

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          LanguageSelectorWidget(),
        ],
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: Dimensions.paddingSizeDefault,
        ),
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Form(
            key: _loginFormKey,
            child: Column(
              children: [
                /// logo and loginWithPassword text
                const KLogo(),
                Text(
                  widget.userType == UserType.customer
                      ? 'Login as a Customer'.tr
                      : 'Login as a Barber'.tr,
                  style: GoogleFonts.roboto(
                    textStyle: h4.copyWith(
                      fontWeight: FontWeight.w300,
                      color: kBlackLight,
                    ),
                  ),
                ),

                /// email textField
                addVerticalSpace(Get.height * 0.05),
                _buildEmailTextFiled(),

                /// password textField
                addVerticalSpace(Get.height * 0.02),
                _buildPasswordTextFiled(),

                /// login button
                addVerticalSpace(Get.height * 0.05),
                _buildLoginButton(context),

                /// forgot password text
                addVerticalSpace(Get.height * 0.02),
                _buildForgotPasswordText(),

                /// don't have an account and register text
                widget.userType == UserType.customer
                    ? _buildGoToRegisterText()
                    : Container(),

                /// login as a barber text
                widget.userType == UserType.customer
                    ? _buildLoginAsABarberText()
                    : Container(),
                addVerticalSpace(Get.height * 0.03),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildEmailTextFiled() => KTextFormFiled(
        controller: emailController,
        labelText: 'Email'.tr,
        inputAction: TextInputAction.next,
        inputType: TextInputType.emailAddress,
        validator: (value) {
          if (value.toString().isEmpty) {
            return Message.emptyEmail.tr;
          } else if (!value.toString().isValidEmail) {
            return Message.invalidEmail.tr;
          }
          return null;
        },
      );

  Widget _buildPasswordTextFiled() => KTextFormFiled(
        controller: passwordController,
        labelText: 'Password'.tr,
        inputAction: TextInputAction.done,
        inputType: TextInputType.visiblePassword,
        obscureValue: passwordVisibility,
        suffix: GestureDetector(
          onTap: () {
            setState(() {
              passwordVisibility = !passwordVisibility;
            });
          },
          child: Icon(
            passwordVisibility ? Icons.visibility_off : Icons.visibility,
            color: kBlackLight,
          ),
        ),
        validator: (value) {
          if (value.toString().isEmpty) {
            return Message.emptyPassword.tr;
          } else if (value.toString().length < 6) {
            return Message.invalidPassword.tr;
          }
          return null;
        },
      );

  Widget _buildLoginButton(BuildContext context) => KButton(
        onPressed: () => _loginMethod(context),
        child: Obx(
          () {
            return authController.isLoading.value
                ? Container(
                    height: 20,
                    width: 20,
                    alignment: Alignment.center,
                    child: CircularProgressIndicator(
                      color: kWhite,
                    ),
                  )
                : Text(
                    'Login'.tr,
                    style: GoogleFonts.roboto(
                      textStyle: h3.copyWith(
                        color: kWhite,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  );
          },
        ),
      );

  Widget _buildForgotPasswordText() => InkWell(
        borderRadius: BorderRadius.circular(50),
        onTap: () {
          Get.to(() => ForgotPasswordScreen());
        },
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: Dimensions.paddingSizeSmall,
            vertical: Dimensions.paddingSizeExtraSmall,
          ),
          child: Text(
            'Forgot Password?'.tr,
            style: h4.copyWith(
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
      );

  Widget _buildGoToRegisterText() => InkWell(
        borderRadius: BorderRadius.circular(50),
        onTap: () {
          Get.offAllNamed(RouteGenerator.register);
        },
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: Dimensions.paddingSizeSmall,
            vertical: Dimensions.paddingSizeExtraSmall,
          ),
          child: Column(
            children: [
              addVerticalSpace(Get.height * 0.02),
              Text(
                "Don't have an Account?".tr,
                style: h5,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    "Create a customer Account".tr,
                    style: h4.copyWith(
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  addHorizontalSpace(5),
                  const Icon(
                    Icons.arrow_forward,
                    size: 20,
                  )
                ],
              ),
            ],
          ),
        ),
      );

  Widget _buildLoginAsABarberText() => InkWell(
        borderRadius: BorderRadius.circular(50),
        onTap: () {
          Get.offAll(
            () => const LoginScreen(userType: UserType.barber),
          );
        },
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: Dimensions.paddingSizeSmall,
            vertical: Dimensions.paddingSizeExtraSmall,
          ),
          child: Column(
            children: [
              addVerticalSpace(Get.height * 0.02),
              Text(
                "Or".tr,
                style: h5,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    "Login as a Barber".tr,
                    style: h4.copyWith(
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  addHorizontalSpace(5),
                  const Icon(
                    Icons.arrow_forward,
                    size: 20,
                  )
                ],
              ),
            ],
          ),
        ),
      );

  void _loginMethod(BuildContext context) {
    context.unFocusKeyboard();
    if (_loginFormKey.currentState!.validate()) {
      authController.login(
        email: emailController.text.trim(),
        password: passwordController.text.trim(),
        deviceToken: deviceTokenToSendPushNotification,
      );
    }
  }
}
