import 'package:barber_shop/src/utils/asset_path.dart';
import 'package:barber_shop/src/utils/color.dart';
import 'package:barber_shop/src/utils/dimensions.dart';
import 'package:barber_shop/src/views/base/helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../../utils/styles.dart';
import '../../../base/k_button.dart';
import '../../../base/k_outlined_button.dart';

class LoginScreen2 extends StatelessWidget {
  const LoginScreen2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        alignment: Alignment.center,
        children: [
          Column(
            children: [
              Expanded(
                flex: 2,
                child: Container(
                  color: kBlack,
                  alignment: Alignment.center,
                  child: Text(
                    'freshup+',
                    style: GoogleFonts.racingSansOne(
                      fontWeight: FontWeight.w400,
                      fontSize: 50,
                      color: kWhite,
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 6,
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Container(
                      width: size.width,
                      decoration: BoxDecoration(
                        color: kBlack,
                        border: Border.all(color: kBlack)
                      ),
                      child: Image.asset(
                        AssetPath.barberShopLoginBg,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Positioned(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            flex: 5,
                            child: Container(
                              width: size.width,
                              decoration: BoxDecoration(
                                gradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [
                                    kBlack,
                                    kBlack.withOpacity(0.0)
                                  ]
                                )
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 5,
                            child: Container(
                              width: size.width,
                              decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                      begin: Alignment.bottomCenter,
                                      end: Alignment.topCenter,
                                      colors: [
                                        kBlack,
                                        kBlack.withOpacity(0.0)
                                      ]
                                  )
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 2,
                child: Container(
                  color: kBlack,
                ),
              ),
            ],
          ),
          Positioned(
            bottom: 50,
            child: Column(
              children: [
                SizedBox(
                  width: size.width - 40,
                  child: Text(
                    "Get the service from world best barber with a barbers who is only assigned to your dedicated time.",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.inter(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: kWhite,
                    ),
                  ),
                ),
                addVerticalSpace(Dimensions.paddingSizeDefault),
                _buildSignInButton(size),
                addVerticalSpace(Dimensions.paddingSizeDefault),
                _buildLogInWithGoogleButton(size),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildSignInButton(Size size) {
    return KButton(
      onPressed: () {},
      bgColor: kDeepRed,
      width: size.width - 30,
      height: 60,
      borderRadius: 16,
      child: Text(
        'Sign in',
        style: GoogleFonts.inter(
          textStyle: h3.copyWith(
            color: kWhite,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }

  Widget _buildLogInWithGoogleButton(Size size){
    return KOutlinedButton(
      onPressed: (){

      },
      borderColor: kWhite,
      width: size.width - 30,
      height: 60,
      borderRadius: 16,
      bgColor: Colors.transparent,
      child: Row(
        children: [
          Image.asset(AssetPath.googleIcon),
          addHorizontalSpace(Dimensions.paddingSizeDefault),
          Text(
            'Login with Google',
            style: GoogleFonts.inter(
              textStyle: h3.copyWith(
                fontWeight: FontWeight.w600,
                color: kWhite,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
