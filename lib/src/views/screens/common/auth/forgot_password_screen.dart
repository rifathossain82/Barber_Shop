import 'package:barber_shop/src/controllers/common/auth_controller.dart';
import 'package:barber_shop/src/services/extensions/build_context_extension.dart';
import 'package:barber_shop/src/services/extensions/string_extension.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../../utils/color.dart';
import '../../../../utils/dimensions.dart';
import '../../../../utils/styles.dart';
import '../../../base/helper.dart';
import '../../../base/k_button.dart';
import '../../../base/k_logo.dart';
import '../../../base/k_text_form_field.dart';

class ForgotPasswordScreen extends StatelessWidget {
  ForgotPasswordScreen({Key? key}) : super(key: key);

  final authController = Get.put(AuthController());
  final emailTextController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: Dimensions.paddingSizeDefault,
        ),
        child: Obx(() {
          return SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  /// logo and text
                  const KLogo(),
                  Text(
                    'To get OTP enter your Email'.tr,
                    style: GoogleFonts.roboto(
                      textStyle: h4.copyWith(
                        fontWeight: FontWeight.w300,
                        color: kBlackLight,
                      ),
                    ),
                  ),

                  /// email textField
                  addVerticalSpace(Get.height * 0.05),
                  _buildEmailTextFiled(),

                  /// send otp button
                  addVerticalSpace(Get.height * 0.05),
                  _buildSendOTPButton(context),
                ],
              ),
            ),
          );
        }),
      ),
    );
  }

  Widget _buildEmailTextFiled() => KTextFormFiled(
        controller: emailTextController,
        labelText: 'Email'.tr,
        inputAction: TextInputAction.done,
        inputType: TextInputType.emailAddress,
        validator: (value) {
          if (value.toString().isEmpty) {
            return Message.emptyEmail.tr;
          } else if (!value.toString().isValidEmail) {
            return Message.invalidEmail.tr;
          }
          return null;
        },
      );

  Widget _buildSendOTPButton(BuildContext context) => KButton(
        onPressed: () => _sendOTPMethod(context),
        child: authController.isLoading.value
            ? Container(
                height: 20,
                width: 20,
                alignment: Alignment.center,
                child: CircularProgressIndicator(
                  color: kWhite,
                ),
              )
            : Text(
                'Send OTP'.tr,
                style: GoogleFonts.roboto(
                  textStyle: h3.copyWith(
                    color: kWhite,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
      );

  void _sendOTPMethod(BuildContext context) {
    context.unFocusKeyboard();
    if (_formKey.currentState!.validate()) {
      authController.forgotPassword(email: emailTextController.text);
    }
  }
}
