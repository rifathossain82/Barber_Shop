import 'package:barber_shop/src/controllers/customers/customer_dashboard_controller.dart';
import 'package:barber_shop/src/utils/color.dart';
import 'package:barber_shop/src/views/screens/common/profile/profile_screen.dart';
import 'package:barber_shop/src/views/screens/customers/appointments/appointment_screen.dart';
import 'package:barber_shop/src/views/screens/customers/home/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CustomerDashboardScreen extends StatelessWidget {
  CustomerDashboardScreen({Key? key}) : super(key: key);

  final customerDashboardController = Get.put(CustomerDashboardController());

  final screens = [
    const HomeScreen(),
    const AppointmentScreen(),
    ProfileScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    return Obx(
            () {
          return Scaffold(
            body: screens[customerDashboardController.currentIndex.value],
            bottomNavigationBar: BottomNavigationBar(
              backgroundColor: kWhite,
              elevation: 0,
              currentIndex: customerDashboardController.currentIndex.value,
              onTap: customerDashboardController.changeIndex,
              items: [
                BottomNavigationBarItem(
                  icon: const Icon(Icons.home),
                  label: 'Home'.tr,
                  tooltip: 'Home'.tr,
                ),
                BottomNavigationBarItem(
                  icon: const Icon(Icons.ac_unit),
                  label: 'Appointments'.tr,
                  tooltip: 'Appointments'.tr,
                ),
                BottomNavigationBarItem(
                  icon: const Icon(Icons.person),
                  label: 'Profile'.tr,
                  tooltip: 'Profile'.tr,
                ),
              ],
            ),
          );
        }
    );
  }
}
