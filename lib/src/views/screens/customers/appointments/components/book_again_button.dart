import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../utils/color.dart';
import '../../../../../utils/styles.dart';
import '../../../../base/helper.dart';
import '../../../../base/k_button.dart';
import '../../../../base/k_outlined_button.dart';

class BookAgainButton extends StatelessWidget {
  final VoidCallback onPressed;
  final double width;

  const BookAgainButton({
    Key? key,
    required this.onPressed,
    required this.width,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return KOutlinedButton(
      onPressed: () => openConfirmDialog(context),
      width: width,
      borderColor: mainColor,
      child: Text(
        'Book Again'.tr,
        style: h3.copyWith(
          fontWeight: FontWeight.bold,
          color: mainColor,
        ),
      ),
    );
  }

  void openConfirmDialog(BuildContext context) {
    customDialog(
      context: context,
      title: "Are you sure?".tr,
      dialogPosition: Alignment.bottomCenter,
      actions: [
        KOutlinedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          width: MediaQuery.of(context).size.width * 0.4,
          borderColor: kGrey,
          bgColor: kWhite,
          child: Text(
            'Opps, No'.tr,
            style: h3.copyWith(
              fontWeight: FontWeight.bold,
              color: kBlackLight,
            ),
          ),
        ),
        KButton(
          onPressed: (){
            onPressed();
            Get..back()..back();
          },
          width: MediaQuery.of(context).size.width * 0.4,
          bgColor: mainColor,
          child: Text(
            'Yes'.tr,
            style: h3.copyWith(
              fontWeight: FontWeight.bold,
              color: kWhite,
            ),
          ),
        ),
      ],
    );
  }
}
