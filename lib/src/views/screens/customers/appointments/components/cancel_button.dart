import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../utils/color.dart';
import '../../../../../utils/styles.dart';
import '../../../../base/helper.dart';
import '../../../../base/k_outlined_button.dart';

class CancelButton extends StatelessWidget {
  final VoidCallback onCancelled;
  final double width;

  const CancelButton({
    Key? key,
    required this.onCancelled,
    required this.width,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return KOutlinedButton(
      onPressed: () => openCancelDialog(context),
      width: width,
      borderColor: kGrey,
      child: Text(
        'Cancel'.tr,
        style: h3.copyWith(
          fontWeight: FontWeight.bold,
          color: kGrey,
        ),
      ),
    );
  }

  void openCancelDialog(BuildContext context) {
    customDialog(
      context: context,
      title: "Are you sure?".tr,
      dialogPosition: Alignment.bottomCenter,
      actions: [
        KOutlinedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          width: MediaQuery.of(context).size.width * 0.4,
          borderColor: kGrey,
          bgColor: kWhite,
          child: Text(
            'Opps, No'.tr,
            style: h3.copyWith(
              fontWeight: FontWeight.bold,
              color: kBlackLight,
            ),
          ),
        ),
        KOutlinedButton(
          onPressed: (){
            onCancelled();
            Get..back()..back();
          },
          width: MediaQuery.of(context).size.width * 0.4,
          borderColor: kRed,
          bgColor: kWhite,
          child: Text(
            'Yes, Cancel It'.tr,
            style: h3.copyWith(
              fontWeight: FontWeight.bold,
              color: kRed,
            ),
          ),
        ),
      ],
    );
  }
}
