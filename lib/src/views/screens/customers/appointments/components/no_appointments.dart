import 'package:barber_shop/src/utils/color.dart';
import 'package:flutter/material.dart';

import '../../../../../utils/styles.dart';

class NoAppointments extends StatelessWidget {
  const NoAppointments({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Icon(
          Icons.calendar_month,
          size: 120,
          color: kGreyMedium,
        ),
        Text(
          "Scheduled Appointments",
          style: h1,
        ),
        Text(
          "Discover and book beauty & wellness professionals near you. Your scheduled appointments will show up here.",
          textAlign: TextAlign.center,
          style: h4.copyWith(
            color: kBlackLight.withOpacity(0.8),
          ),
        )
      ],
    );
  }
}
