import 'package:barber_shop/src/services/extensions/string_extension.dart';
import 'package:barber_shop/src/utils/app_constants.dart';
import 'package:barber_shop/src/utils/dimensions.dart';
import 'package:barber_shop/src/utils/styles.dart';
import 'package:barber_shop/src/views/base/appointment_status_widget.dart';
import 'package:barber_shop/src/views/base/helper.dart';
import 'package:barber_shop/src/views/screens/customers/appointments/appoinment_details.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../../../models/customers/appointment_data.dart';
import '../../../../../utils/color.dart';

class AppointmentItem extends StatelessWidget {
  final AppointmentData appointmentData;

  const AppointmentItem({
    Key? key,
    required this.appointmentData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.to(
          () => AppointmentDetails(appointmentData: appointmentData),
        );
      },
      child: Container(
        padding: EdgeInsets.all(Dimensions.paddingSizeDefault),
        decoration: BoxDecoration(
          color: kWhite,
          borderRadius: BorderRadius.circular(Dimensions.radiusDefault),
          boxShadow: [
            BoxShadow(
              offset: const Offset(0, 2),
              blurRadius: 4,
              spreadRadius: 0,
              color: kItemShadowColor,
            ),
          ],
        ),
        child: IntrinsicHeight(
          child: Row(
            children: [
              Expanded(
                flex: 8,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    AppointmentStatusWidget(
                      status: appointmentData.status ==
                              AppointmentStatus.Confirmed.name
                          ? AppointmentStatus.Confirmed
                          : appointmentData.status ==
                                  AppointmentStatus.Booked.name
                              ? AppointmentStatus.Booked
                              : AppointmentStatus.Canceled,
                    ),
                    addVerticalSpace(Dimensions.paddingSizeSmall),
                    Text(
                      appointmentData.service!.title!,
                      style: h2.copyWith(
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    addVerticalSpace(2),
                    Text(
                      '${appointmentData.service!.price!} ${AppConstants.currencySymbol}',
                      style: h3.copyWith(
                        color: kGreyTextColor,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ],
                ),
              ),
              const VerticalDivider(),
              Expanded(
                flex: 2,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      appointmentData.barber!.name ?? "",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.center,
                      style: h5,
                    ),
                    addVerticalSpace(Dimensions.paddingSizeExtraSmall),
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text: DateFormat("dd").format(
                              DateTime.parse(appointmentData.date!),
                            ),
                            style: h1.copyWith(
                              fontWeight: FontWeight.w700,
                              fontSize: 28,
                            ),
                          ),
                          const TextSpan(text: " "),
                          TextSpan(
                            text: DateFormat("MMM").format(
                              DateTime.parse(appointmentData.date!),
                            ),
                            style: h5,
                          ),
                        ],
                      ),
                    ),
                    addVerticalSpace(Dimensions.paddingSizeExtraSmall),
                    Text(
                      appointmentData.timeSlot!.start!.minuteAndSecond,
                      style: h4,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
