import 'package:barber_shop/src/controllers/customers/appointment_controller.dart';
import 'package:barber_shop/src/utils/color.dart';
import 'package:barber_shop/src/utils/dimensions.dart';
import 'package:barber_shop/src/utils/styles.dart';
import 'package:barber_shop/src/views/base/helper.dart';
import 'package:barber_shop/src/views/base/k_custom_loader.dart';
import 'package:barber_shop/src/views/base/no_data_found.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'components/appointment_item.dart';

class AppointmentScreen extends StatefulWidget {
  const AppointmentScreen({Key? key}) : super(key: key);

  @override
  State<AppointmentScreen> createState() => _AppointmentScreenState();
}

class _AppointmentScreenState extends State<AppointmentScreen> {
  final appointmentController = Get.put(AppointmentController());
  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      appointmentController.pageNumber.value = 1;
      appointmentController.getAppointmentList();
      scrollIndicator();
    });
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void scrollIndicator() {
    _scrollController.addListener(
      () {
        if (_scrollController.offset >=
                _scrollController.position.maxScrollExtent &&
            !_scrollController.position.outOfRange) {
          kPrint('reach to bottom');
          if (!appointmentController.loadedCompleted.value) {
            ++appointmentController.pageNumber.value;
            appointmentController.getAppointmentList();
          }
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Appointments'.tr),
      ),
      body: Obx(() {
        return appointmentController.isLoading.value
            ? const KCustomLoader()
            : appointmentController.appointmentList.isEmpty &&
                    appointmentController.oldAppointmentList.isEmpty
                ? const NoDataFound()
                : Stack(
                    children: [
                      SingleChildScrollView(
                        controller: _scrollController,
                        physics: const BouncingScrollPhysics(),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            _buildCurrentAppointmentList(),
                            appointmentController.oldAppointmentList.isEmpty
                                ? Container()
                                : _buildFinishedAppointmentText(),
                            _buildOldAppointmentList(),
                          ],
                        ),
                      ),
                      appointmentController.isLoadingMore.value
                          ? const Positioned(
                              child: KCustomLoader(),
                            )
                          : Container(),
                    ],
                  );
      }),
    );
  }

  Widget _buildCurrentAppointmentList() {
    return ListView.separated(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: appointmentController.appointmentList.length,
      padding: EdgeInsets.symmetric(
        horizontal: Dimensions.paddingSizeDefault,
      ),
      itemBuilder: (context, index) {
        return AppointmentItem(
          appointmentData: appointmentController.appointmentList[index],
        );
      },
      separatorBuilder: (context, index) =>
          addVerticalSpace(Dimensions.paddingSizeLarge),
    );
  }

  Widget _buildFinishedAppointmentText() {
    return Container(
      alignment: Alignment.centerLeft,
      margin: EdgeInsets.only(
        left: Dimensions.marginSizeLarge,
        right: Dimensions.marginSizeDefault,
        bottom: Dimensions.marginSizeSmall,
        top: appointmentController.appointmentList.isEmpty ? 0 : 30,
      ),
      child: Text(
        'Finished Appointments'.tr.toUpperCase(),
        style: h2.copyWith(
          color: kBlackLight.withOpacity(0.7),
        ),
      ),
    );
  }

  Widget _buildOldAppointmentList() {
    return ListView.separated(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: appointmentController.oldAppointmentList.length,
      padding: EdgeInsets.symmetric(
        horizontal: Dimensions.paddingSizeDefault,
      ),
      itemBuilder: (context, index) => AppointmentItem(
        appointmentData: appointmentController.oldAppointmentList[index],
      ),
      separatorBuilder: (context, index) =>
          addVerticalSpace(Dimensions.paddingSizeLarge),
    );
  }
}
