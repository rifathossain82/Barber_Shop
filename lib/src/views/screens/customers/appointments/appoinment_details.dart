import 'package:barber_shop/src/controllers/customers/appointment_controller.dart';
import 'package:barber_shop/src/network/api.dart';
import 'package:barber_shop/src/services/extensions/string_extension.dart';
import 'package:barber_shop/src/utils/app_constants.dart';
import 'package:barber_shop/src/utils/color.dart';
import 'package:barber_shop/src/utils/dimensions.dart';
import 'package:barber_shop/src/views/base/appointment_status_widget.dart';
import 'package:barber_shop/src/views/base/k_divider.dart';
import 'package:barber_shop/src/views/base/k_profile_image.dart';
import 'package:barber_shop/src/views/base/row_text.dart';
import 'package:barber_shop/src/views/screens/customers/appointments/components/book_again_button.dart';
import 'package:barber_shop/src/views/screens/customers/appointments/components/cancel_button.dart';
import 'package:barber_shop/src/views/screens/customers/appointments/components/confirm_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../../models/customers/appointment_data.dart';
import '../../../../utils/styles.dart';
import '../../../base/barber_info.dart';
import '../../../base/helper.dart';

class AppointmentDetails extends StatelessWidget {
  final AppointmentData appointmentData;

  AppointmentDetails({
    Key? key,
    required this.appointmentData,
  }) : super(key: key);

  final appointmentController = Get.put(AppointmentController());

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "${appointmentData.timeSlot!.start!.minuteAndSecond}, ${DateFormat("MMM dd").format(
            DateTime.parse(appointmentData.date!),
          )}",
        ),
      ),
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          SizedBox(
            height: size.height,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  addVerticalSpace(Dimensions.paddingSizeDefault),
                  AppointmentStatusWidget(
                    status: appointmentData.status ==
                            AppointmentStatus.Confirmed.name
                        ? AppointmentStatus.Confirmed
                        : appointmentData.status ==
                                AppointmentStatus.Booked.name
                            ? AppointmentStatus.Booked
                            : AppointmentStatus.Canceled,
                  ),
                  const BarberInfo(),
                  addVerticalSpace(Dimensions.paddingSizeSmall),
                  _buildAppointmentListTile(),
                  addVerticalSpace(Dimensions.paddingSizeSmall),
                  _buildAppointmentBills(),
                  addVerticalSpace(70),
                ],
              ),
            ),
          ),

          /// if the appointment is old from now then no action button will appear
          /// if the appointment is less then 24 hour then no action button will appear
          DateTime.parse(appointmentData.date!).isBefore(DateTime.now())
              ? _buildFooterText(size, "Finished Appointment!".tr)
              : _buildFooterButtons(size),
        ],
      ),
    );
  }

  Widget _buildAppointmentListTile() {
    return ListTile(
      title: Text(
        appointmentData.service!.title!,
        style: h3.copyWith(
          fontWeight: FontWeight.w500,
        ),
      ),
      subtitle: Row(
        children: [
          KProfileImage(
            height: 30,
            width: 30,
            imgURL: Api.getImageURL(appointmentData.barber!.avatar ?? ""),
          ),
          addHorizontalSpace(Dimensions.paddingSizeSmall),
          Text(appointmentData.barber!.name!),
        ],
      ),
      trailing: Text(
        '${appointmentData.timeSlot!.start!.minuteAndSecond} - ${appointmentData.timeSlot!.end!.minuteAndSecond}',
        style: h4.copyWith(
          color: kBlackLight.withOpacity(0.8),
        ),
      ),
    );
  }

  Widget _buildAppointmentBills() {
    return Container(
      padding: EdgeInsets.all(Dimensions.paddingSizeDefault),
      margin: EdgeInsets.all(Dimensions.paddingSizeDefault),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: BorderRadius.circular(Dimensions.radiusSmall),
        boxShadow: [
          BoxShadow(
            offset: const Offset(0, 0),
            blurRadius: 4,
            spreadRadius: 0,
            color: kItemShadowColor,
          ),
        ],
      ),
      child: Column(
        children: [
          RowText(
            title: 'Service Charge'.tr,
            value: '${AppConstants.currencySymbol} ${appointmentData.service!.price}',
          ),
          RowText(
            title: 'Tip'.tr,
            value: '${AppConstants.currencySymbol} ${appointmentData.tipAmount}',
          ),
          const KDivider(height: 0),
          RowText(
            title: 'Total'.tr,
            titleStyle: h3.copyWith(
              color: kGreyTextColor,
              fontWeight: FontWeight.w700,
            ),
            value: '${AppConstants.currencySymbol} ${appointmentData.total}',
          ),
          RowText(
            title: '${'Paid by'.tr} ${appointmentData.paymentMethod ?? 'Cash'}',
            value: '${AppConstants.currencySymbol} ${appointmentData.paid}',
          ),
          const KDivider(height: 0),
          RowText(
            title: 'Due'.tr,
            value: '${AppConstants.currencySymbol} ${appointmentData.dueAmount}',
          ),
        ],
      ),
    );
  }

  Widget _buildFooterText(Size size, String txt) {
    return Container(
      height: 50,
      width: size.width,
      alignment: Alignment.center,
      padding: EdgeInsets.symmetric(horizontal: Dimensions.paddingSizeDefault),
      decoration: BoxDecoration(color: kBlueGrey.withOpacity(0.1)),
      child: Text(
        txt,
        textAlign: TextAlign.center,
        style: h4.copyWith(
          fontWeight: FontWeight.w600,
          color: kBlueGrey,
        ),
      ),
    );
  }

  Widget _buildFooterButtons(Size size) {
    return Positioned(
      child: Container(
        height: 70,
        color: kWhite,
        padding: EdgeInsets.symmetric(
          vertical: Dimensions.paddingSizeDefault,
        ),
        child: _buttons(size),
      ),
    );
  }

  Widget _buttons(Size size) {
    if (DateTime.now().isBefore(DateTime.parse(appointmentData.date!)
        .subtract(const Duration(hours: 24)))) {
      if (appointmentData.status == AppointmentStatus.Canceled.name) {
        return BookAgainButton(
          onPressed: () {
            changeStatus(AppointmentStatus.Booked);
          },
          width: size.width - 30,
        );
      } else {
        return CancelButton(
          onCancelled: () {
            changeStatus(AppointmentStatus.Canceled);
          },
          width: size.width - 30,
        );
      }
    } else if (appointmentData.status == AppointmentStatus.Canceled.name) {
      return BookAgainButton(
        onPressed: () {
          changeStatus(AppointmentStatus.Booked);
        },
        width: size.width - 30,
      );
    } else if (appointmentData.status == AppointmentStatus.Confirmed.name) {
      return CancelButton(
        onCancelled: () {
          changeStatus(AppointmentStatus.Canceled);
        },
        width: size.width - 30,
      );
    } else {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          CancelButton(
            onCancelled: () {
              changeStatus(AppointmentStatus.Canceled);
            },
            width: size.width * 0.42,
          ),
          ConfirmButton(
            onConfirmed: () {
              changeStatus(AppointmentStatus.Confirmed);
            },
            width: size.width * 0.42,
          ),
        ],
      );
    }
  }

  void changeStatus(AppointmentStatus status) async {
    await appointmentController.changeAppointmentStatus(
      id: appointmentData.id!,
      status: status.name,
    );
    appointmentController.getAppointmentList();
  }
}
