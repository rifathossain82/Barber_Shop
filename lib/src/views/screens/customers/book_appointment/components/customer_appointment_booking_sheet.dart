import 'package:barber_shop/src/controllers/common/auth_controller.dart';
import 'package:barber_shop/src/controllers/customers/appointment_controller.dart';
import 'package:barber_shop/src/models/common/user_data_model.dart';
import 'package:barber_shop/src/services/extensions/date_time_extension.dart';
import 'package:barber_shop/src/services/stripe_payment_services.dart';
import 'package:barber_shop/src/utils/app_constants.dart';
import 'package:barber_shop/src/utils/asset_path.dart';
import 'package:barber_shop/src/utils/color.dart';
import 'package:barber_shop/src/utils/dimensions.dart';
import 'package:barber_shop/src/utils/styles.dart';
import 'package:barber_shop/src/views/base/helper.dart';
import 'package:barber_shop/src/views/base/k_button.dart';
import 'package:barber_shop/src/views/base/k_divider.dart';
import 'package:barber_shop/src/views/base/k_outlined_button.dart';
import 'package:barber_shop/src/views/base/k_text_form_field.dart';
import 'package:barber_shop/src/views/base/row_text.dart';
import 'package:barber_shop/src/views/base/selectable_container.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomerAppointmentBookingSheet {
  final _customerAppointmentController = Get.put(AppointmentController());

  /// to input tip
  final _tipTextController = TextEditingController();
  FocusNode _tipTextFocusNode = FocusNode();
  final _tipFormKey = GlobalKey<FormState>();

  /// to show appointment confirmation screen
  DateTime? _date;
  String? _time;
  int? _barberId;
  int? _timeSlotId;
  int? _serviceId;
  num? _serviceCharge;

  var _selectedTipsIndex = 0;
  var _tipPercentage = '20';
  num _tipAmount = 0;
  num _total = 0;
  num _payAmount = 0;

  UserData? _user;

  /// for Partially Pre Payment only
  bool isSelectedFullPayment = true;
  bool isSelectedPartiallyPayment = false;

  /// if book with pay, this is necessary, by default payment method is cash
  String _paymentMethod = 'Cash';
  String _transactionID = '';

  final List<String> _tips = [
    '20',
    '25',
    '30',
    '40',
    'None'.tr,
    'Custom'.tr,
  ];

  CustomerAppointmentBookingSheet({
    required DateTime date,
    required String time,
    required int barberId,
    required int timeSlotId,
    required int serviceId,
    required num serviceCharge,
  }) {
    _date = date;
    _time = time;
    _barberId = barberId;
    _timeSlotId = timeSlotId;
    _serviceId = serviceId;
    _serviceCharge = serviceCharge;
  }

  Future open(BuildContext context) {
    /// load user
    _user = Get.find<AuthController>().user.value;

    /// set initial tipAmount of 20% and calculate total
    _calculateTipAndTotalAmount();

    /// show bottom sheet
    return showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(top: Radius.circular(10)),
      ),
      builder: (context) {
        return StatefulBuilder(builder: (context, setState) {
          return Padding(
            padding: MediaQuery.of(context).viewInsets,
            child: Container(
              padding: EdgeInsets.all(Dimensions.paddingSizeDefault),
              height: MediaQuery.of(context).size.height * 0.95, // sheet size
              child: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    /// heading and back button
                    Row(
                      children: [
                        IconButton(
                          onPressed: () {
                            Get.back();
                          },
                          icon: Icon(
                            Icons.arrow_back_ios,
                            color: kBlackLight,
                            size: 20,
                          ),
                        ),
                        Text(
                          'Payment Information'.tr,
                          style: h2.copyWith(
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ],
                    ),

                    /// add a tip section
                    addVerticalSpace(Dimensions.paddingSizeDefault),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Add a tip'.tr,
                          style: h3,
                        ),
                        Text(
                          '${AppConstants.currencySymbol}$_tipAmount',
                          style: h3.copyWith(
                            color: kGreyTextColor,
                          ),
                        ),
                      ],
                    ),
                    addVerticalSpace(Dimensions.paddingSizeSmall),
                    GridView.count(
                      scrollDirection: Axis.vertical,
                      physics: const ScrollPhysics(),
                      childAspectRatio: 2.5,
                      mainAxisSpacing: 10,
                      crossAxisSpacing: 10,
                      shrinkWrap: true,
                      crossAxisCount: 3,
                      children: List.generate(
                        _tips.length,
                        (index) {
                          return SelectableContainer(
                            onTap: () {
                              setState(() {
                                _selectedTipsIndex = index;
                                _tipPercentage = '0';

                                /// when selected is none
                                if (index == 4) {
                                  _tipTextController.clear();
                                  _calculateTipAndTotalAmount();
                                }

                                /// when selected is custom
                                else if (index == 5) {
                                  _showDialogToAddTip(context);
                                }

                                /// otherwise
                                else {
                                  _tipPercentage = _tips[index];
                                  _tipTextController.clear();
                                  _calculateTipAndTotalAmount();
                                }
                              });
                            },
                            isSelected: _selectedTipsIndex == index,
                            child: Text(
                              /// ignore % when select none or custom
                              '${_tips[index]} ${index == 4 || index == 5 ? '' : '%'}',
                              style: h4.copyWith(
                                fontWeight: FontWeight.w600,
                                color: _selectedTipsIndex == index
                                    ? kWhite
                                    : kBlackLight,
                              ),
                            ),
                          );
                        },
                      ),
                    ),

                    /// amounts overview
                    addVerticalSpace(Dimensions.paddingSizeDefault),
                    RowText(
                      title: 'Service Charge'.tr,
                      value: '${AppConstants.currencySymbol}  $_serviceCharge',
                    ),
                    RowText(
                      title: 'Tip'.tr,
                      value: '${AppConstants.currencySymbol} $_tipAmount',
                    ),
                    const KDivider(),
                    RowText(
                      title: 'Total'.tr,
                      value: '${AppConstants.currencySymbol} $_total',
                    ),

                    /// NOTE: user payment system
                    /// 1. Full Post Payment
                    /// 2. Full Pre Payment
                    /// 3. Partially Pre Payment

                    /// pay in advance
                    _user!.paymentSystem == 'Partially Pre Payment'
                        ? Row(
                            children: [
                              Expanded(
                                child: Text(
                                  'Pay in advance'.tr,
                                  style: h3.copyWith(
                                    color: kGreyTextColor,
                                  ),
                                ),
                              ),
                              _total < _user!.minimumPaymentAmount!
                                  ? Container()
                                  : SelectableContainer(
                                      onTap: () {
                                        setState(() {
                                          if (!isSelectedPartiallyPayment) {
                                            isSelectedPartiallyPayment = true;
                                            isSelectedFullPayment = false;
                                            _calculateTipAndTotalAmount();
                                          }
                                        });
                                      },
                                      isSelected: isSelectedPartiallyPayment,
                                      width: 80,
                                      child: Text(
                                        '${AppConstants.currencySymbol} ${_user!.minimumPaymentAmount!}',
                                        style: h4.copyWith(
                                          fontWeight: FontWeight.w600,
                                          color: isSelectedPartiallyPayment
                                              ? kWhite
                                              : kBlackLight,
                                        ),
                                      ),
                                    ),
                              addHorizontalSpace(Dimensions.paddingSizeSmall),
                              SelectableContainer(
                                onTap: () {
                                  setState(() {
                                    if (!isSelectedFullPayment) {
                                      isSelectedPartiallyPayment = false;
                                      isSelectedFullPayment = true;
                                      _calculateTipAndTotalAmount();
                                    }
                                  });
                                },
                                isSelected: isSelectedFullPayment,
                                width: 80,
                                child: Text(
                                  '${AppConstants.currencySymbol} $_total',
                                  style: h4.copyWith(
                                    fontWeight: FontWeight.w600,
                                    color: isSelectedFullPayment
                                        ? kWhite
                                        : kBlackLight,
                                  ),
                                ),
                              ),
                            ],
                          )
                        : Container(),

                    addVerticalSpace(30),

                    _user!.paymentSystem == 'Full Post Payment'
                        ? _buildAllPaymentMethods(context)
                        : _buildBookWithStripeButton(context),

                    /// terms and policy text
                    _buildTermsAndPolicyText(context),
                  ],
                ),
              ),
            ),
          );
        });
      },
    );
  }

  void _calculateTipAndTotalAmount() {
    _tipAmount = _serviceCharge! * num.parse(_tipPercentage) / 100;
    if (_tipTextController.text.isNotEmpty) {
      _tipAmount = _tipAmount + num.parse(_tipTextController.text);
    }
    _total = _serviceCharge! + _tipAmount;

    /// define pay amount
    if (_user!.paymentSystem == 'Full Pre Payment') {
      _payAmount = _total;
    } else if (_user!.paymentSystem == 'Partially Pre Payment') {
      if (isSelectedPartiallyPayment) {
        _payAmount = _user!.minimumPaymentAmount!;
        if (_total < _user!.minimumPaymentAmount!) {
          _payAmount = _total;
        }
      } else {
        _payAmount = _total;
      }
    } else{
      // for customer of full post payment:
      // if customer pay by stripe or paypal then _payAmount is _total
      // if customer book with cash then payAmount = 0 for request body, for that check _buildBookWithoutPayButton() => onPressed()
      _payAmount = _total;
    }
  }

  void _showDialogToAddTip(BuildContext context) {
    /// again initial focusNode and then request to focus
    _tipTextFocusNode = FocusNode();
    _tipTextFocusNode.requestFocus();

    /// show a dialog to input custom _tips
    showDialog(
      context: context,
      builder: (context) {
        return StatefulBuilder(
          builder: (context, setState) {
            return AlertDialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(Dimensions.radiusDefault),
              ),
              insetPadding: EdgeInsets.all(
                Dimensions.paddingSizeDefault,
              ),
              alignment: Alignment.bottomCenter,
              actionsAlignment: MainAxisAlignment.spaceEvenly,
              title: Text(
                'Add a custom tip'.tr,
                textAlign: TextAlign.center,
                style: h2.copyWith(
                  fontWeight: FontWeight.w500,
                ),
              ),
              content: Form(
                key: _tipFormKey,
                child: KTextFormFiled(
                  controller: _tipTextController,
                  focusNode: _tipTextFocusNode,
                  labelText: '${'Tip'.tr} ( ${AppConstants.currency} )',
                  inputAction: TextInputAction.done,
                  inputType: TextInputType.number,
                  validator: (value) {
                    if (value.toString().isEmpty) {
                      return Message.emptyAmount.tr;
                    }
                    return null;
                  },
                ),
              ),
              actions: [
                KOutlinedButton(
                  onPressed: () {
                    setState((){
                      _calculateTipAndTotalAmount();
                      Navigator.pop(context);
                    });
                  },
                  width: MediaQuery.of(context).size.width * 0.4,
                  borderColor: kGrey,
                  child: Text(
                    'Cancel'.tr,
                    style: h3.copyWith(
                      fontWeight: FontWeight.bold,
                      color: kGrey,
                    ),
                  ),
                ),
                KButton(
                  onPressed: () {
                    if (_tipFormKey.currentState!.validate()) {
                      setState((){
                        _calculateTipAndTotalAmount();
                        Navigator.pop(context);
                      });
                    }
                  },
                  width: MediaQuery.of(context).size.width * 0.4,
                  child: Text(
                    'Add'.tr,
                    style: GoogleFonts.roboto(
                      textStyle: h3.copyWith(
                        color: kWhite,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                ),
              ],
            );
          },
        );
      },
    );
  }

  Widget _buildAllPaymentMethods(BuildContext context) {
    return Column(
      children: [
        _buildBookWithoutPayButton(),
        addVerticalSpace(Dimensions.paddingSizeDefault),
        _buildBookWithStripeButton(context),
        // addVerticalSpace(Dimensions.paddingSizeDefault),
        // _buildBookWithPaypalButton(context),
      ],
    );
  }

  Widget _buildBookWithoutPayButton() {
    return KButton(
      height: 50,
      onPressed: () => _bookAppointment(payAmount: 0),
      child: Text(
        'Book with cash'.tr,
        style: h3.copyWith(
          fontWeight: FontWeight.bold,
          color: kWhite,
        ),
      ),
    );
  }

  Widget _buildBookWithStripeButton(BuildContext context) {
    return KButton(
      height: 50,
      bgColor: kBlack,
      onPressed: () async {
        if (_payAmount == 0) {
          kSnackBar(
            message: 'Please enter the payment amount!'.tr,
            bgColor: failedColor,
          );
        } else {
          _paymentMethod = "Stripe";
          var result = await StripePaymentServices().makePayment(
            context: context,
            amount: "${_payAmount.round()}",
          );

          if (result.isNotEmpty) {
            _transactionID = result;
            kPrint('Stripe Transaction Id: $_transactionID');
            _bookAppointment(payAmount: _payAmount);
          }
        }
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Book with '.tr,
            style: h2.copyWith(
              color: kWhite,
              fontWeight: FontWeight.w700,
            ),
          ),
          addHorizontalSpace(Dimensions.paddingSizeExtraSmall),
          Image.asset(
            AssetPath.stripeIcon,
            height: 40,
            alignment: Alignment.center,
          ),
        ],
      ),
    );
  }

  // Widget _buildBookWithPaypalButton(BuildContext context) {
  //   return KButton(
  //     height: 50,
  //     bgColor: warningColor,
  //     onPressed: () async {
  //       if (_payAmount == 0) {
  //         kSnackBar(
  //           message: 'Please enter the payment amount!'.tr,
  //           bgColor: failedColor,
  //         );
  //       } else {
  //         _paymentMethod = "Paypal";
  //         PaypalPaymentServices.makePayment(
  //             context: context,
  //             amount: _payAmount,
  //             onSuccess: (Map response) async {
  //               kPrint('Paypal Response: $response');
  //               kSnackBar(
  //                 message: 'Payment Successful!',
  //                 bgColor: successColor,
  //               );
  //
  //               /// save transactionId and then book appointment
  //               _transactionID = response['paymentId'];
  //               _bookAppointment(payAmount: _payAmount);
  //             });
  //       }
  //     },
  //     child: Row(
  //       mainAxisAlignment: MainAxisAlignment.center,
  //       children: [
  //         Text(
  //           'Book with '.tr,
  //           style: h2.copyWith(
  //             color: kWhite,
  //             fontWeight: FontWeight.w700,
  //           ),
  //         ),
  //         addHorizontalSpace(Dimensions.paddingSizeExtraSmall),
  //         Image.asset(
  //           AssetPath.paypalIcon,
  //           height: 40,
  //           alignment: Alignment.center,
  //         ),
  //       ],
  //     ),
  //   );
  // }
  //
  // Widget _buildOrText() {
  //   return Container(
  //     padding: EdgeInsets.symmetric(
  //       vertical: Dimensions.paddingSizeDefault,
  //     ),
  //     child: Row(
  //       children: [
  //         Expanded(child: KDivider(color: kBlackLight)),
  //         addHorizontalSpace(Dimensions.paddingSizeExtraSmall),
  //         Text(
  //           'OR'.tr,
  //           textAlign: TextAlign.center,
  //           style: h4.copyWith(
  //             fontWeight: FontWeight.w700,
  //           ),
  //         ),
  //         addHorizontalSpace(Dimensions.paddingSizeExtraSmall),
  //         Expanded(child: KDivider(color: kBlackLight)),
  //       ],
  //     ),
  //   );
  // }

  Widget _buildTermsAndPolicyText(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        top: Dimensions.paddingSizeDefault,
        left: MediaQuery.of(context).size.width * 0.1,
        right: MediaQuery.of(context).size.width * 0.1,
      ),
      child: Text(
        'Payment will not be refunded unless cancelled before 24 hours!'.tr,
        textAlign: TextAlign.center,
        style: h3.copyWith(
          color: kRedLight,
        ),
      ),
    );
  }

  void _bookAppointment({required num payAmount}) {
    Map<String, dynamic> requestBody = {
      'date': _date!.yyyyMMddWithSlash,
      'barber_id': '$_barberId',
      'time_slot_id': '$_timeSlotId',
      'service_id': '$_serviceId',
      'service_charge': '$_serviceCharge',
      'tip_amount': '$_tipAmount',
      'tax_amount': '0', // useless
      'total': '$_total',
      'paid': '$payAmount',
      'payment_method': _paymentMethod,
      'tnx_id': _transactionID,
    };

    _customerAppointmentController.bookAppointment(
      date: _date!,
      time: _time!,
      body: requestBody,
    );
  }
}
