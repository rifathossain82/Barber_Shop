import 'package:barber_shop/src/controllers/common/barber_list_controller.dart';
import 'package:barber_shop/src/controllers/customers/appointment_controller.dart';
import 'package:barber_shop/src/controllers/customers/time_slot_controller.dart';
import 'package:barber_shop/src/models/common/barber_data.dart';
import 'package:barber_shop/src/models/common/service_data_model.dart';
import 'package:barber_shop/src/models/customers/time_slot_data.dart';
import 'package:barber_shop/src/network/api.dart';
import 'package:barber_shop/src/services/extensions/date_time_extension.dart';
import 'package:barber_shop/src/services/extensions/string_extension.dart';
import 'package:barber_shop/src/utils/app_constants.dart';
import 'package:barber_shop/src/utils/color.dart';
import 'package:barber_shop/src/utils/dimensions.dart';
import 'package:barber_shop/src/utils/styles.dart';
import 'package:barber_shop/src/views/base/barber_image.dart';
import 'package:barber_shop/src/views/base/calendar_widget.dart';
import 'package:barber_shop/src/views/base/helper.dart';
import 'package:barber_shop/src/views/base/k_button.dart';
import 'package:barber_shop/src/views/base/k_custom_loader.dart';
import 'package:barber_shop/src/views/base/k_divider.dart';
import 'package:barber_shop/src/views/base/k_profile_image.dart';
import 'package:barber_shop/src/views/base/time_slot_item.dart';
import 'package:barber_shop/src/views/screens/customers/book_appointment/components/customer_appointment_booking_sheet.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../controllers/common/holiday_controller.dart';
import '../../../base/k_outlined_button.dart';

class BookAppointmentScreen extends StatefulWidget {
  final ServiceData service;

  const BookAppointmentScreen({
    Key? key,
    required this.service,
  }) : super(key: key);

  @override
  State<BookAppointmentScreen> createState() => _BookAppointmentScreenState();
}

class _BookAppointmentScreenState extends State<BookAppointmentScreen> {
  final barberController = Get.put(BarberListController());
  final timeSlotController = Get.put(TimeSlotController());
  final appointmentController = Get.put(AppointmentController());
  final holidayController = Get.put(HolidayController());

  // Initially stripe is selected
  int paymentMethod = 1;
  int? selectedBarberIndex;
  int? selectedTimeIndex;
  Barber? selectedBarber;
  TimeSlots? selectedTimeSlot;
  late DateTime selectedDay;
  late DateTime focusDay;
  late DateTime lastDay;
  List<int> weekendDays = [];
  List<DateTime> holidays = [];

  @override
  void initState() {
    selectedDay = DateTime.now();
    focusDay = DateTime.now();
    lastDay = DateTime.now().add(const Duration(days: 21));
    setHolidaysAndWeekend();
    super.initState();
  }

  void setHolidaysAndWeekend() async {
    await holidayController.getHolidayList();

    /// set weekend day
    for (var element in holidayController.holidays.value.weeklyHoliday!) {
      weekendDays.add(int.parse(element));
    }

    /// set holiday
    for (var element in holidayController.holidays.value.publicHolidays!) {
      var day = DateTime.parse(element);
      holidays.add(DateTime.utc(day.year, day.month, day.day));
    }

    /// set selected day in the next day of all weekend day and holiday
    for (var element in holidays) {
      if (DateFormat('yyyy-MM-dd').format(element) ==
          DateFormat('yyyy-MM-dd').format(selectedDay)) {
        selectedDay = selectedDay.add(const Duration(days: 1));
        focusDay = focusDay.add(const Duration(days: 1));
      }
    }
    for(int i=1; i<7; i++){
      if(weekendDays.contains(selectedDay.weekday)){
        selectedDay = selectedDay.add(const Duration(days: 1));
        focusDay = focusDay.add(const Duration(days: 1));
      }
    }

    /// fetch barber list based on selected day
    barberController.getBarberList(date: selectedDay.yyyyMMddWithSlash);
    if (!mounted) {
      return;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('Book an Appointment'.tr),
      ),
      body: Obx(() {
        return appointmentController.isLoading.value
            ? const KCustomLoader()
            : Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  _buildBody(size),
                  Positioned(
                    child: _buildBottomSection(size),
                  ),
                ],
              );
      }),
    );
  }

  Widget _buildBody(Size size) {
    return SizedBox(
      height: size.height,
      child: SingleChildScrollView(
        child: Column(
          children: [
            _buildCalender(),
            const KDivider(),
            _buildBarber(),
            const KDivider(),
            _buildTimeSlot(),
            const KDivider(),
            _buildFinalAppointment(),
            // _buildPaymentMethods(),
            addVerticalSpace(90),
          ],
        ),
      ),
    );
  }

  Widget _buildBottomSection(Size size) {
    return Container(
      height: 90,
      width: size.width,
      padding: EdgeInsets.all(Dimensions.paddingSizeDefault),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: const BorderRadius.only(
          topRight: Radius.circular(Dimensions.radiusLarge),
          topLeft: Radius.circular(Dimensions.radiusLarge),
        ),
      ),
      child: Row(
        children: [
          Expanded(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "${widget.service.price} ${AppConstants.currencySymbol}",
                  style: h1.copyWith(fontWeight: FontWeight.w700),
                ),
                // Text(
                //   "45min",
                //   style: h4,
                // ),
              ],
            ),
          ),
          Expanded(
            child: KButton(
              height: 50,
              onPressed: () {
                if (selectedBarber == null) {
                  kSnackBar(
                      message: "Please, Select a Barber!",
                      bgColor: failedColor);
                } else if (selectedTimeSlot == null) {
                  kSnackBar(
                      message: "Please, Select a Time-Slot",
                      bgColor: failedColor);
                } else {
                  openBookingConfirmDialog(context);
                }
              },
              child: Text(
                'Continue'.tr,
                style: h3.copyWith(
                  fontWeight: FontWeight.bold,
                  color: kWhite,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildCalender() {
    return CalendarWidget(
      focusDay: focusDay,
      selectedDay: selectedDay,
      lastDay: lastDay,
      weekendDays: weekendDays,
      holidays: holidays,
      onDaySelected: (currentDay, focusDay_) {
        if (currentDay != selectedDay) {
          setState(() {
            selectedDay = currentDay;
            focusDay = focusDay_;
            selectedBarberIndex = null;
            selectedTimeIndex = null;
            barberController.getBarberList(
              date: selectedDay.yyyyMMddWithSlash,
            );
          });
        }
      },
    );
  }

  Widget _buildBarber() {
    return Obx(() {
      if (barberController.isLoading.value) {
        return const KCustomLoader();
      } else if (barberController.barberData.value.barbers!.isEmpty) {
        return Container(
          height: 50,
          alignment: Alignment.center,
          child: Text(
            'No Barber Found!'.tr,
            style: h3,
          ),
        );
      } else {
        final barberData = barberController.barberData.value;
        return Container(
          height: 80,
          alignment: Alignment.centerLeft,
          margin: EdgeInsets.symmetric(
            horizontal: Dimensions.marginSizeDefault,
            vertical: Dimensions.marginSizeSmall,
          ),
          child: ListView.separated(
            itemCount: barberData.barbers!.length,
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) => GestureDetector(
              onTap: () {
                setState(() {
                  if (selectedBarberIndex != index) {
                    selectedBarberIndex = index;
                    selectedBarber = barberData.barbers![index];
                    timeSlotController.getTimeSlotList(
                      date: selectedDay.yyyyMMddWithSlash,
                      barberId: selectedBarber!.id!,
                    );
                  }
                });
              },
              child: BarberProfileItem(
                isSelected: selectedBarberIndex == index ? true : false,
                imgURL: Api.getImageURL(barberData.barbers![index].avatar),
                name: barberData.barbers![index].name ?? "",
              ),
            ),
            separatorBuilder: (context, index) => addHorizontalSpace(
              Dimensions.paddingSizeExtraLarge,
            ),
          ),
        );
      }
    });
  }

  Widget _buildTimeSlot() {
    return selectedBarberIndex == null
        ? Container(
            height: 50,
            alignment: Alignment.center,
            child: Text(
              'Select a Barber!'.tr,
              style: h3,
            ),
          )
        : Obx(() {
            if (timeSlotController.isLoading.value) {
              return const KCustomLoader();
            } else if (timeSlotController
                .timeSlotData.value.timeSlots!.isEmpty) {
              return Center(child: Text('No Time Slot Found!'.tr));
            } else {
              var timeSlotData = timeSlotController.timeSlotData.value;
              return Container(
                height: 35,
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.symmetric(
                  horizontal: Dimensions.marginSizeDefault,
                  vertical: Dimensions.marginSizeSmall,
                ),
                child: ListView.separated(
                  itemCount: timeSlotData.timeSlots!.length,
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) => GestureDetector(
                    onTap: () {
                      setState(() {
                        selectedTimeIndex = index;
                        selectedTimeSlot = timeSlotData.timeSlots![index];
                      });
                    },
                    child: TimeSlotItem(
                      isSelected: selectedTimeIndex == index ? true : false,
                      starTime: timeSlotData.timeSlots![index].start ?? "",
                      endTime: timeSlotData.timeSlots![index].end ?? "",
                    ),
                  ),
                  separatorBuilder: (context, index) => addHorizontalSpace(
                    Dimensions.paddingSizeDefault,
                  ),
                ),
              );
            }
          });
  }

  Widget _buildFinalAppointment() {
    return selectedBarberIndex == null && selectedTimeIndex == null
        ? Container()
        : selectedTimeIndex == null
            ? Container(
                height: 50,
                alignment: Alignment.center,
                child: Text(
                  'Select a time slot!'.tr,
                  style: h3,
                ),
              )
            : Container(
                margin: EdgeInsets.symmetric(
                  horizontal: Dimensions.marginSizeDefault,
                  vertical: Dimensions.marginSizeSmall,
                ),
                padding: EdgeInsets.all(
                  Dimensions.paddingSizeDefault,
                ),
                decoration: BoxDecoration(
                    color: kGreyMedium.withOpacity(0.08),
                    borderRadius: BorderRadius.circular(12)),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Text(
                            widget.service.title ?? "",
                            style: h2.copyWith(
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                        addHorizontalSpace(Dimensions.paddingSizeSmall),
                        Column(
                          children: [
                            Text(
                              "${widget.service.price} ${AppConstants.currencySymbol}",
                              style: h2.copyWith(
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            Text(
                              selectedTimeSlot!.start!.substring(0, 5),
                              style: h4,
                            ),
                          ],
                        ),
                      ],
                    ),
                    const KDivider(),
                    Row(
                      children: [
                        Text(
                          "${'Staff'.tr}:",
                          style: h3.copyWith(
                            color: kGreyTextColor,
                          ),
                        ),
                        KProfileImage(
                          height: 30,
                          width: 30,
                          imgURL: Api.getImageURL(selectedBarber!.avatar),
                          margin: EdgeInsets.symmetric(
                            horizontal: Dimensions.marginSizeSmall,
                          ),
                        ),
                        Text(
                          selectedBarber!.name ?? "",
                          style: h4,
                        ),
                      ],
                    ),
                  ],
                ),
              );
  }

  void openBookingConfirmDialog(BuildContext context) {
    CustomerAppointmentBookingSheet appointmentBookingSheet = CustomerAppointmentBookingSheet(
      date: selectedDay,
      time: selectedTimeSlot!.start.toString().minuteAndSecond,
      barberId: selectedBarber!.id!,
      timeSlotId: selectedTimeSlot!.id!,
      serviceId: widget.service.id!,
      serviceCharge: widget.service.price!,
    );

    appointmentBookingSheet.open(context);
  }
}
