import 'package:barber_shop/src/controllers/common/service_controller.dart';
import 'package:barber_shop/src/utils/color.dart';
import 'package:barber_shop/src/utils/dimensions.dart';
import 'package:barber_shop/src/views/base/helper.dart';
import 'package:barber_shop/src/views/base/k_custom_loader.dart';
import 'package:barber_shop/src/views/base/k_divider.dart';
import 'package:barber_shop/src/views/base/k_search_field.dart';
import 'package:barber_shop/src/views/base/no_data_found.dart';
import 'package:barber_shop/src/views/base/service_item.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BarberServices extends StatefulWidget {
  const BarberServices({Key? key}) : super(key: key);

  @override
  State<BarberServices> createState() => _BarberServicesState();
}

class _BarberServicesState extends State<BarberServices> {
  final searchController = TextEditingController();
  final serviceController = Get.put(ServiceController());
  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    serviceController.pageNumber.value = 1;
    serviceController.getServiceList();
    scrollIndicator();
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _searchMethod(String value) {
    DeBouncer.run(() {
      if (value.isNotEmpty) {
        setState(() {
          serviceController.getServiceList(
            serviceName: searchController.text,
          );
        });
      } else {
        serviceController.getServiceList();
      }
    });
  }

  void scrollIndicator() {
    _scrollController.addListener(
      () {
        if (_scrollController.offset >=
                _scrollController.position.maxScrollExtent &&
            !_scrollController.position.outOfRange) {
          kPrint('reach to bottom');
          if (!serviceController.loadedCompleted.value) {
            ++serviceController.pageNumber.value;
            serviceController.getServiceList();
          }
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      controller: _scrollController,
      physics: const BouncingScrollPhysics(),
      child: Column(
        children: [
          const KDivider(height: 0),

          /// services search field
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: Dimensions.paddingSizeDefault,
              vertical: Dimensions.paddingSizeDefault,
            ),
            child: KSearchField(
              controller: searchController,
              hintText: 'Search for service'.tr,
              onChanged: _searchMethod,
            ),
          ),
          const KDivider(height: 0),

          /// services list
          Obx(() {
            return serviceController.isLoading.value
                ? const KCustomLoader()
                : serviceController.serviceList.isEmpty
                    ? const NoDataFound()
                    : ListView.separated(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: serviceController.serviceList.length + 1,
                      itemBuilder: (context, index) {
                        if (index == serviceController.serviceList.length &&
                            !serviceController.loadedCompleted.value) {
                          return const Center(child: CircularProgressIndicator());
                        } else if (index ==
                                serviceController.serviceList.length &&
                            serviceController.loadedCompleted.value) {
                          return Container();
                        }

                        return ServiceItem(
                          service: serviceController.serviceList[index],
                          userType: UserType.customer,
                        );
                      },
                      separatorBuilder: (context, index) =>
                          Divider(color: kDividerColor),
                    );
          }),
        ],
      ),
    );
  }
}
