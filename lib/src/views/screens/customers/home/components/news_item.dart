import 'package:barber_shop/src/models/customers/news_data_model.dart';
import 'package:barber_shop/src/network/api.dart';
import 'package:barber_shop/src/utils/styles.dart';
import 'package:barber_shop/src/views/screens/customers/home/news/news_details.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../utils/asset_path.dart';
import '../../../../../utils/color.dart';
import '../../../../../utils/dimensions.dart';

class NewsItem extends StatelessWidget {
  final News news;

  const NewsItem({Key? key, required this.news}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Get.to(() => NewsDetails(news: news)),
      child: Container(
        height: 240,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: kBlackLight,
          borderRadius: BorderRadius.circular(Dimensions.radiusDefault),
          boxShadow: [
            BoxShadow(
              offset: const Offset(0, 2),
              blurRadius: 4,
              spreadRadius: 0,
              color: kItemShadowColor,
            ),
          ]
        ),
        child: Column(
          children: [
            Container(
              alignment: Alignment.center,
              height: 40,
              child: Text(
                news.title ?? "",
                textAlign: TextAlign.left,
                maxLines: 1,
                style: h3.copyWith(
                    color: kWhite,
                    fontWeight: FontWeight.w500
                ),
              ),
            ),
            Expanded(
              child: Hero(
                tag: "${news.title}",
                child: CachedNetworkImage(
                  imageUrl: Api.getImageURL(news.image!),
                  imageBuilder: (context, imageProvider) => Container(
                    decoration: BoxDecoration(
                      borderRadius: const BorderRadius.only(
                        bottomRight: Radius.circular(Dimensions.radiusDefault),
                        bottomLeft: Radius.circular(Dimensions.radiusDefault),
                      ),
                      image: DecorationImage(
                        image: imageProvider,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  fit: BoxFit.cover,
                  placeholder: (context, url) => ClipRRect(
                    borderRadius: BorderRadius.circular(Dimensions.radiusDefault),
                    child: Image.asset(
                      AssetPath.placeholder,
                      fit: BoxFit.cover,
                    ),
                  ),
                  errorWidget: (context, url, error) => Icon(
                    Icons.image_not_supported,
                    color: kBlueGrey,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
