import 'package:barber_shop/src/controllers/common/barber_list_controller.dart';
import 'package:barber_shop/src/controllers/customers/shop_details_controller.dart';
import 'package:barber_shop/src/models/common/barber_data.dart';
import 'package:barber_shop/src/models/customers/shop_details_data.dart';
import 'package:barber_shop/src/models/customers/social_medial_model.dart';
import 'package:barber_shop/src/network/api.dart';
import 'package:barber_shop/src/services/extensions/string_extension.dart';
import 'package:barber_shop/src/services/url_launcher_service.dart';
import 'package:barber_shop/src/utils/asset_path.dart';
import 'package:barber_shop/src/utils/color.dart';
import 'package:barber_shop/src/utils/dimensions.dart';
import 'package:barber_shop/src/views/base/barber_info.dart';
import 'package:barber_shop/src/views/base/k_custom_loader.dart';
import 'package:barber_shop/src/views/base/k_divider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../utils/styles.dart';
import '../../../../base/helper.dart';
import '../../../../base/k_profile_image.dart';

class BarberDetails extends StatelessWidget {
  BarberDetails({Key? key}) : super(key: key);

  final shopDetailsController = Get.put(ShopDetailsController());
  final barberController = Get.put(BarberListController());

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      child: Center(
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Obx(() {
            return shopDetailsController.isLoading.value
                ? const KCustomLoader()
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const BarberInfo(),
                      _buildTitle("Staffers".tr.toUpperCase()),
                      _buildStaffers(barberController),
                      _buildTitle('Contact & Business Hours'.tr.toUpperCase()),
                      const KDivider(),
                      _buildContact(
                        shopDetailsController.shopDetails.value.info!.phone ??
                            "Not Set".tr,
                      ),
                      const KDivider(),
                      _buildDayScheduleList(
                        shopDetailsController.shopDetails.value.timeSlots ?? [],
                      ),
                      _buildTitle("Social Media & Share".tr.toUpperCase()),
                      _buildSocialMediaAndShare(),
                      _buildTitle("Venue Amenities".tr.toUpperCase()),
                      _buildVenueList(
                        shopDetailsController.shopDetails.value.facilities!,
                      ),
                      addVerticalSpace(Dimensions.paddingSizeDefault),
                    ],
                  );
          }),
        ),
      ),
      onRefresh: () async {
        shopDetailsController.getShopDetails();
        barberController.getBarberList();
      },
    );
  }

  Widget _buildTitle(String title) {
    return Padding(
      padding: EdgeInsets.only(
        left: Dimensions.paddingSizeDefault,
        right: Dimensions.paddingSizeDefault,
        top: 30,
        bottom: Dimensions.paddingSizeDefault,
      ),
      child: Text(
        title.toUpperCase(),
        style: h4.copyWith(
          fontWeight: FontWeight.w700,
        ),
      ),
    );
  }

  Widget _buildStaffers(BarberListController barberController) {
    return barberController.isLoading.value
        ? const Center(child: CircularProgressIndicator())
        : barberController.barberData.value.barbers!.isEmpty
            ? Container(
                margin: EdgeInsets.symmetric(
                  horizontal: Dimensions.marginSizeDefault,
                ),
                child: Text("There is no barber!".tr, style: h4,),
              )
            : Container(
                height: 90,
                margin: EdgeInsets.symmetric(
                  horizontal: Dimensions.marginSizeDefault,
                ),
                child: ListView.separated(
                  itemCount: barberController.barberData.value.barbers!.length,
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) => _buildStaffItem(
                    barberController.barberData.value.barbers![index],
                  ),
                  separatorBuilder: (context, index) => addHorizontalSpace(
                    Dimensions.paddingSizeExtraLarge,
                  ),
                ),
              );
  }

  Widget _buildStaffItem(Barber barber) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        KProfileImage(
          height: 60,
          width: 60,
          imgURL: Api.getImageURL(barber.avatar),
          borderColor: Colors.transparent,
        ),
        addVerticalSpace(5),
        Text(
          barber.name ?? "",
          style: h4.copyWith(
            fontWeight: FontWeight.w500,
          ),
        )
      ],
    );
  }

  Widget _buildContact(String phone) {
    return Padding(
      padding: EdgeInsets.symmetric(
        vertical: Dimensions.paddingSizeExtraSmall,
        horizontal: Dimensions.paddingSizeDefault,
      ),
      child: Row(
        children: [
          Icon(
            Icons.phone_android,
            color: kGrey,
            size: 30,
          ),
          addHorizontalSpace(16),
          Text(
            phone,
            style: h3.copyWith(
              fontWeight: FontWeight.w600,
              color: kBlackLight.withOpacity(0.8),
            ),
          ),
          const Spacer(),
          GestureDetector(
            onTap: () => LauncherServices.makeCall(phone),
            child: Container(
              alignment: Alignment.center,
              padding: EdgeInsets.symmetric(
                horizontal: Dimensions.paddingSizeLarge,
                vertical: Dimensions.paddingSizeExtraSmall,
              ),
              decoration: BoxDecoration(
                color: kWhite,
                borderRadius: BorderRadius.circular(8),
                border: Border.all(
                  color: kGreyMedium.withOpacity(0.5),
                ),
              ),
              child: Text(
                'Call'.tr,
                style: h4.copyWith(
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildDayScheduleList(List<ShopTimeSlots> slotList) {
    return ListView.separated(
      shrinkWrap: true,
      padding: EdgeInsets.symmetric(
        horizontal: Dimensions.paddingSizeDefault,
        vertical: Dimensions.paddingSizeDefault,
      ),
      physics: const NeverScrollableScrollPhysics(),
      itemCount: slotList.length,
      itemBuilder: (context, index) => _buildDayScheduleItem(slotList[index]),
      separatorBuilder: (context, index) =>
          addVerticalSpace(Dimensions.paddingSizeLarge),
    );
  }

  Widget _buildDayScheduleItem(ShopTimeSlots slot) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          slot.day.toString().trim().tr,
          style: h4,
        ),
        Text(
          slot.startTime == null || slot.endTime == null
              ? "Closed".tr
              : "${slot.startTime!.minuteAndSecond} - ${slot.endTime!.minuteAndSecond}",
          style: h4.copyWith(
            fontWeight: FontWeight.w700,
          ),
        ),
      ],
    );
  }

  Widget _buildSocialMediaAndShare() {
    final socialMediaList = [
      SocialMedia(
        name: 'Instagram'.tr,
        link: shopDetailsController.shopDetails.value.links!.instagramLink!,
        assetIconPath: AssetPath.instagram,
      ),
      SocialMedia(
        name: 'Facebook'.tr,
        link: shopDetailsController.shopDetails.value.links!.facebookLink!,
        assetIconPath: AssetPath.facebook,
      ),
      SocialMedia(
        name: 'Website'.tr,
        link: shopDetailsController.shopDetails.value.links!.websiteLink!,
        assetIconPath: AssetPath.website,
      ),
      SocialMedia(
        name: 'Share'.tr,
        link: shopDetailsController.shopDetails.value.links!.appLink!,
        assetIconPath: AssetPath.share,
      ),
    ];

    return SizedBox(
      height: 80,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: List.generate(
          socialMediaList.length,
          (index) => _buildSocialMediaItem(socialMediaList[index]),
        ),
      ),
    );
  }

  Widget _buildSocialMediaItem(SocialMedia media) {
    return GestureDetector(
      onTap: () => LauncherServices.openURL(media.link),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          CircleAvatar(
            radius: 25,
            backgroundColor: kGrey.withOpacity(0.8),
            child: Image.asset(
              media.assetIconPath,
              height: 22,
              width: 22,
              color: kWhite,
            ),
          ),
          addVerticalSpace(5),
          Text(
            media.name,
            style: h5,
          )
        ],
      ),
    );
  }

  Widget _buildVenueList(Facilities facilities) {
    return Column(
      children: [
        facilities.parkingSpace!
            ? _buildVenueItem(
                CupertinoIcons.car,
                'Parking Space'.tr,
              )
            : Container(),
        facilities.wifi!
            ? _buildVenueItem(Icons.wifi, "Wi-Fi".tr)
            : Container(),
        facilities.acceptCreditCard!
            ? _buildVenueItem(Icons.credit_card, "Credit cards accepted".tr)
            : Container(),
        facilities.accessible!
            ? _buildVenueItem(
                Icons.accessible,
                "Accessible for people with disabilities".tr,
              )
            : Container(),
      ],
    );
  }

  Widget _buildVenueItem(IconData icon, String title) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: Dimensions.paddingSizeDefault,
        vertical: Dimensions.paddingSizeSmall,
      ),
      child: Row(
        children: [
          Icon(
            icon,
            color: mainColor,
          ),
          addHorizontalSpace(Dimensions.paddingSizeDefault),
          Expanded(
            child: Text(
              title,
              style: h3.copyWith(
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
