import 'package:barber_shop/src/controllers/customers/slider_controller.dart';
import 'package:barber_shop/src/network/api.dart';
import 'package:barber_shop/src/utils/asset_path.dart';
import 'package:barber_shop/src/utils/color.dart';
import 'package:barber_shop/src/utils/styles.dart';
import 'package:barber_shop/src/views/screens/customers/home/details/barber_details.dart';
import 'package:barber_shop/src/views/screens/customers/home/news/barber_news.dart';
import 'package:barber_shop/src/views/screens/customers/home/services/barber_services.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import '../../../../services/local_storage.dart';
import '../../../base/helper.dart';
import 'components/sliver_appbar_deligate.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  late ScrollController _scrollController;
  bool isSliverCollapsed = false;
  int sliderIndex = 0;
  final sliderController = Get.put(SliderController());

  @override
  void initState() {
    super.initState();
    _tabController = TabController(initialIndex: 0, length: 3, vsync: this);
    _scrollController = ScrollController();

    _scrollController.addListener(() {
      if (_scrollController.offset > 150 &&
          !_scrollController.position.outOfRange) {
        if (!isSliverCollapsed) {
          isSliverCollapsed = true;
          setState(() {});
        }
      }
      if (_scrollController.offset <= 150 &&
          !_scrollController.position.outOfRange) {
        if (isSliverCollapsed) {
          isSliverCollapsed = false;
          setState(() {});
        }
      }
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: DefaultTabController(
        length: 3,
        child: NestedScrollView(
          controller: _scrollController,
          headerSliverBuilder: (context, value) {
            return [
              SliverAppBar(
                expandedHeight: 200.0,
                systemOverlayStyle: SystemUiOverlayStyle(
                  statusBarColor: Colors.transparent,
                  statusBarIconBrightness:
                      isSliverCollapsed ? Brightness.dark : Brightness.light,
                ),
                flexibleSpace: FlexibleSpaceBar(
                  background: _buildSlider(),
                ),
              ),
              SliverPersistentHeader(
                pinned: true,
                delegate: CustomSliverAppBarDelegate(
                  minHeight: 90,
                  maxHeight: 90,
                  child: Container(
                    width: double.infinity,
                    alignment: Alignment.bottomCenter,
                    color: kWhite,
                    child: ListTile(
                      title: Text(
                        LocalStorage.getData(
                              key: LocalStorageKey.shopName,
                            ) ??
                            "Freshup",
                        style: h2.copyWith(
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      subtitle: Text(
                        LocalStorage.getData(
                              key: LocalStorageKey.shopLocation,
                            ) ??
                            "56 cours berriat, Grenoble, France",
                        style: h5.copyWith(
                          color: kGreyTextColor,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              SliverPersistentHeader(
                pinned: true,
                delegate: CustomSliverAppBarDelegate(
                  minHeight: 40,
                  maxHeight: 40,
                  child: Container(
                    color: kWhite,
                    alignment: Alignment.center,
                    child: TabBar(
                      indicatorSize: TabBarIndicatorSize.label,
                      indicatorColor: kBlack,
                      labelColor: kBlackLight,
                      unselectedLabelColor: kGreyTextColor,
                      labelStyle: h5.copyWith(
                        fontWeight: FontWeight.w700,
                      ),
                      unselectedLabelStyle: h5.copyWith(
                        fontWeight: FontWeight.w700,
                      ),
                      controller: _tabController,
                      tabs: [
                        Tab(
                          icon: Text('Services'.tr.toUpperCase()),
                        ),
                        Tab(
                          icon: Text('News'.tr.toUpperCase()),
                        ),
                        Tab(
                          icon: Text('Details'.tr.toUpperCase()),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ];
          },
          body: TabBarView(
            controller: _tabController,
            children: [
              const BarberServices(),
              const BarberNews(),
              BarberDetails(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildSlider() {
    return Obx(
      () => sliderController.isLoading.value
          ? const Center(child: CircularProgressIndicator())
          : sliderController.slider.value.sliders!.isEmpty
              ? Center(
                  child: Text(
                    'No Slider Image Found!',
                    textAlign: TextAlign.center,
                    style: h2.copyWith(
                      fontWeight: FontWeight.w700,
                      color: kBlackLight.withOpacity(0.8),
                    ),
                  ),
                )
              : Stack(
                  alignment: Alignment.bottomRight,
                  children: [
                    CarouselSlider(
                      options: CarouselOptions(
                        height: MediaQuery.of(context).size.height * 0.45,
                        viewportFraction: 1,
                        initialPage: 0,
                        enlargeFactor: 0,
                        enableInfiniteScroll: true,
                        reverse: false,
                        autoPlay: true,
                        autoPlayInterval: const Duration(seconds: 5),
                        autoPlayAnimationDuration: const Duration(
                          milliseconds: 1000,
                        ),
                        autoPlayCurve: Curves.easeInCubic,
                        enlargeCenterPage: true,
                        scrollDirection: Axis.horizontal,
                        onPageChanged: (int index, reason) {
                          setState(() {
                            sliderIndex = index;
                          });
                        },
                      ),
                      items:
                          sliderController.slider.value.sliders!.map((slider) {
                        return Builder(
                          builder: (BuildContext context) {
                            return SizedBox(
                              width: Get.width,
                              child: CachedNetworkImage(
                                imageUrl: Api.getImageURL(slider.image),
                                fit: BoxFit.cover,
                                placeholder: (context, url) => Image.asset(
                                  AssetPath.placeholder,
                                  fit: BoxFit.cover,
                                ),
                                errorWidget: (context, url, error) =>
                                    Image.asset(
                                  AssetPath.placeholder,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            );
                          },
                        );
                      }).toList(),
                    ),
                    ListView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      itemCount: sliderController.slider.value.sliders!.length,
                      itemBuilder: (context, int index) {
                        return Align(
                          alignment: Alignment.bottomCenter,
                          child: Container(
                            width: sliderIndex == index ? 18 : 6,
                            height: 6,
                            margin: const EdgeInsets.symmetric(
                              vertical: 10.0,
                              horizontal: 5.0,
                            ),
                            decoration: BoxDecoration(
                              // shape: sliderIndex == index ? BoxShape.rectangle :  BoxShape.circle,
                              borderRadius: BorderRadius.circular(
                                sliderIndex == index ? 15 : 30,
                              ),
                              color: kWhite,
                            ),
                          ),
                        );
                      },
                    ),
                  ],
                ),
    );
  }
}
