import 'package:barber_shop/src/controllers/customers/news_controller.dart';
import 'package:barber_shop/src/utils/dimensions.dart';
import 'package:barber_shop/src/views/base/helper.dart';
import 'package:barber_shop/src/views/base/k_custom_loader.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../base/no_data_found.dart';
import '../components/news_item.dart';

class BarberNews extends StatelessWidget {
  const BarberNews({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final portfolioController = Get.put(NewsController());
    return RefreshIndicator(
      child: Obx(() {
        return portfolioController.isLoading.value
            ? const KCustomLoader()
            : portfolioController.newsData.value.news!.isEmpty
                ? const NoDataFound()
                : ListView.separated(
                    physics: const BouncingScrollPhysics(),
                    itemCount: portfolioController
                        .newsData.value.news!.length,
                    padding: EdgeInsets.all(
                      Dimensions.paddingSizeDefault,
                    ),
                    itemBuilder: (context, index) => NewsItem(
                      news: portfolioController
                          .newsData.value.news![index],
                    ),
                    separatorBuilder: (context, index) => addVerticalSpace(
                      Dimensions.paddingSizeLarge,
                    ),
                  );
      }),
      onRefresh: ()async{
        portfolioController.getPortfolioData();
      },
    );
  }
}
