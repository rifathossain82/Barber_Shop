import 'package:barber_shop/src/models/customers/news_data_model.dart';
import 'package:barber_shop/src/utils/styles.dart';
import 'package:barber_shop/src/views/base/helper.dart';
import 'package:barber_shop/src/views/base/k_divider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../network/api.dart';
import '../../../../../utils/asset_path.dart';
import '../../../../../utils/color.dart';
import '../../../../../utils/dimensions.dart';

class NewsDetails extends StatelessWidget {
  final News news;

  const NewsDetails({Key? key, required this.news}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(news.title ?? "News Details".tr),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 220,
              child: Hero(
                tag: "${news.title}",
                child: CachedNetworkImage(
                  imageUrl: Api.getImageURL(news.image!),
                  imageBuilder: (context, imageProvider) => Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: imageProvider,
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  placeholder: (context, url) => ClipRRect(
                    borderRadius: BorderRadius.circular(Dimensions.radiusDefault),
                    child: Image.asset(
                      AssetPath.placeholder,
                      fit: BoxFit.cover,
                    ),
                  ),
                  errorWidget: (context, url, error) => Image.asset(
                    AssetPath.placeholder,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            addVerticalSpace(Dimensions.paddingSizeDefault),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: Dimensions.paddingSizeDefault,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Description".tr,
                    style: h1.copyWith(
                      color: kBlackLight.withOpacity(0.8),
                      fontWeight: FontWeight.w700,
                      fontSize: 25,
                    ),
                  ),
                  const KDivider(),
                  Text(
                    news.description ?? "",
                    style: h4.copyWith(
                      color: kBlackLight.withOpacity(0.7),
                    ),
                  ),
                ],
              ),
            ),
            addVerticalSpace(Dimensions.paddingSizeDefault),
          ],
        ),
      ),
    );
  }
}
