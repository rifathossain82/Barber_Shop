import 'package:barber_shop/src/models/common/service_data_model.dart';
import 'package:barber_shop/src/utils/app_constants.dart';
import 'package:barber_shop/src/utils/color.dart';
import 'package:barber_shop/src/utils/dimensions.dart';
import 'package:barber_shop/src/views/base/helper.dart';
import 'package:barber_shop/src/views/base/html_view.dart';
import 'package:barber_shop/src/views/base/k_button.dart';
import 'package:barber_shop/src/views/screens/admin/book_appointment/book_appointment_screen_for_admin.dart';
import 'package:barber_shop/src/views/screens/barbers/book_appointment/book_appointment_screen_for_barber.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../utils/styles.dart';
import '../screens/customers/book_appointment/book_appointment_screen.dart';

class ServiceItem extends StatelessWidget {
  final ServiceData service;
  final UserType userType;

  const ServiceItem({
    Key? key,
    required this.service,
    required this.userType,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: Dimensions.marginSizeDefault,
        vertical: Dimensions.marginSizeSmall,
      ),
      child: Row(
        children: [
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        service.title ?? "",
                        style: h3.copyWith(
                          color: kBlackLight,
                        ),
                      ),
                      addVerticalSpace(Dimensions.paddingSizeExtraSmall),
                      HTMLView(htmlData: service.description ?? ""),
                    ],
                  ),
                ),
                addHorizontalSpace(Dimensions.paddingSizeDefault),
                Text(
                  "${service.price}${AppConstants.currencySymbol}",
                  style: h2.copyWith(
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ],
            ),
          ),
          addHorizontalSpace(Dimensions.paddingSizeSmall),
          KButton(
            width: Get.width * 0.1,
            height: 35,
            onPressed: () {
              if (userType == UserType.customer) {
                Get.to(() => BookAppointmentScreen(service: service));
              } else if (userType == UserType.barber) {
                Get.to(() => BookAppointmentScreenForBarber(service: service));
              } else {
                Get.to(() => BookAppointmentScreenForAdmin(service: service));
              }
            },
            child: Text(
              'Book'.tr,
              style: h4.copyWith(
                fontWeight: FontWeight.bold,
                color: kWhite,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
