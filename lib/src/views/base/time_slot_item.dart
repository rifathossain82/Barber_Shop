import 'package:barber_shop/src/services/extensions/string_extension.dart';
import 'package:flutter/material.dart';

import '../../utils/color.dart';
import '../../utils/dimensions.dart';
import '../../utils/styles.dart';

class TimeSlotItem extends StatelessWidget {
  final bool isSelected;
  final String starTime;
  final String endTime;

  const TimeSlotItem({
    Key? key,
    required this.isSelected,
    required this.starTime,
    required this.endTime,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.symmetric(
        horizontal: Dimensions.paddingSizeDefault,
      ),
      decoration: BoxDecoration(
        color: isSelected ? mainColor.withOpacity(0.08) : kWhite,
        borderRadius: BorderRadius.circular(8),
        border: Border.all(
          color: isSelected ? mainColor : kGreyMedium.withOpacity(0.3),
        ),
      ),
      child: Text(
        starTime.toString().minuteAndSecond,
        style: isSelected
            ? h4.copyWith(fontWeight: FontWeight.w600)
            : h4,
      ),
    );
  }
}
