import 'package:barber_shop/src/utils/color.dart';
import 'package:flutter/material.dart';

class Badge extends StatelessWidget {
  final Widget child;
  final Widget badgeContent;
  final Color? bgColor;
  final double? topPosition;
  final double? bottomPosition;
  final double? leftPosition;
  final double? rightPosition;
  final double badgeSize;

  const Badge({
    Key? key,
    required this.child,
    required this.badgeContent,
    this.bgColor,
    this.topPosition,
    this.bottomPosition,
    this.leftPosition,
    this.rightPosition,
    this.badgeSize = 16,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        child,
        Positioned(
          top: topPosition,
          bottom: bottomPosition,
          left: leftPosition,
          right: rightPosition,
          child: Container(
            alignment: Alignment.center,
            height: badgeSize,
            width: badgeSize,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: bgColor ?? mainColor,
            ),
            child: badgeContent,
          ),
        ),
      ],
    );
  }
}
