import 'package:barber_shop/src/views/base/helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../utils/color.dart';
import '../../utils/dimensions.dart';
import '../../utils/styles.dart';

class AppointmentStatusWidget extends StatelessWidget {
  final AppointmentStatus status;

  const AppointmentStatusWidget({
    Key? key,
    required this.status,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: Dimensions.paddingSizeSmall,
        vertical: 2,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(
          Dimensions.radiusDefault,
        ),
        color: status == AppointmentStatus.Booked
            ? mainColor.withOpacity(0.3)
            : status == AppointmentStatus.Confirmed
                ? kGreen.withOpacity(0.2)
                : kGreyMedium.withOpacity(0.2),
      ),
      child: Text(
        status == AppointmentStatus.Booked
            ? AppointmentStatus.Booked.name.tr
            : status == AppointmentStatus.Confirmed
                ? AppointmentStatus.Confirmed.name.tr
                : AppointmentStatus.Canceled.name.tr,
        style: h5.copyWith(
          color: status == AppointmentStatus.Booked
              ? mainColor
              : status == AppointmentStatus.Confirmed
                  ? kGreen
                  : kGreyTextColor,
          fontWeight: FontWeight.w600,
        ),
      ),
    );
  }
}
