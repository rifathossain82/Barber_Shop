import 'package:barber_shop/src/utils/color.dart';
import 'package:barber_shop/src/utils/styles.dart';
import 'package:barber_shop/src/views/base/helper.dart';
import 'package:flutter/material.dart';

class IconText extends StatelessWidget {
  final IconData icon;
  final String text;

  const IconText({
    Key? key,
    required this.icon,
    required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Icon(
          icon,
          color: kBlackLight.withOpacity(0.8),
          size: 20,
        ),
        addHorizontalSpace(2),
        Text(
          text,
          style: h4.copyWith(
            color: kBlackLight.withOpacity(0.8),
          ),
        ),
      ],
    );
  }
}
