import 'package:barber_shop/localization/app_localization.dart';
import 'package:barber_shop/src/controllers/common/locale_controller.dart';
import 'package:barber_shop/src/utils/dimensions.dart';
import 'package:barber_shop/src/utils/styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'helper.dart';

class LanguageSelectorWidget extends StatelessWidget {
  LanguageSelectorWidget({Key? key}) : super(key: key);
  final localeController = Get.put(LocaleController());

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return GestureDetector(
        onTap: () => openLanguageSelectorDialog(context),
        child: Container(
          margin: EdgeInsets.only(
            right: Dimensions.marginSizeDefault,
            left: Dimensions.marginSizeDefault,
          ),
          child: Row(
            children: [
              Text(
                localeController.selectedLocaleData.value.languageCode,
                style: h3.copyWith(
                  fontWeight: FontWeight.w700,
                ),
              ),
              addHorizontalSpace(Dimensions.paddingSizeSmall),
              Image.asset(
                localeController.selectedLocaleData.value.flag,
                height: 30,
                width: 30,
                fit: BoxFit.cover,
              ),
            ],
          ),
        ),
      );
    });
  }

  openLanguageSelectorDialog(BuildContext context) {
    customDialog(
      context: context,
      title: "Select your language",
      content: Obx(() {
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            RadioListTile(
              contentPadding: EdgeInsets.zero,
              title: Row(
                children: [
                  Text(AppLocalization.locals[0].languageName),
                  addHorizontalSpace(Dimensions.paddingSizeSmall),
                  Image.asset(
                    AppLocalization.locals[0].flag,
                    height: 30,
                    width: 30,
                    fit: BoxFit.cover,
                  ),
                ],
              ),
              value: AppLocalization.locals[0],
              groupValue: localeController.selectedLocaleData.value,
              onChanged: (value) {
                if (value != null) {
                  localeController.updateLocale(value);
                  Navigator.pop(context);
                }
              },
            ),
            RadioListTile(
              contentPadding: EdgeInsets.zero,
              title: Row(
                children: [
                  Text(AppLocalization.locals[1].languageName),
                  addHorizontalSpace(Dimensions.paddingSizeSmall),
                  Image.asset(
                    AppLocalization.locals[1].flag,
                    height: 30,
                    width: 30,
                    fit: BoxFit.cover,
                  ),
                ],
              ),
              value: AppLocalization.locals[1],
              groupValue: localeController.selectedLocaleData.value,
              onChanged: (value) {
                if (value != null) {
                  localeController.updateLocale(value);
                  Navigator.pop(context);
                }
              },
            ),
          ],
        );
      }),
      dialogPosition: Alignment.bottomCenter,
    );
  }
}
