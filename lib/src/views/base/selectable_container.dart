import 'package:barber_shop/src/utils/color.dart';
import 'package:flutter/material.dart';

class SelectableContainer extends StatelessWidget {
  final VoidCallback onTap;
  final bool isSelected;
  final Widget child;
  final double height;
  final double? width;

  const SelectableContainer({
    Key? key,
    required this.onTap,
    required this.isSelected,
    required this.child,
    this.height = 40,
    this.width,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: isSelected ? kBlack : kWhite,
          borderRadius: BorderRadius.circular(8),
          border: Border.all(
            color: isSelected ? Colors.transparent : kGrey,
            width: 1,
          ),
        ),
        height: height,
        width: width,
        child: child,
      ),
    );
  }
}
