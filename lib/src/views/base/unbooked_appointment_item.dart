import 'package:barber_shop/src/models/barbers/barber_appointment_data.dart';
import 'package:barber_shop/src/services/extensions/string_extension.dart';
import 'package:flutter/material.dart';

import '../../utils/color.dart';
import '../../utils/dimensions.dart';
import '../../utils/styles.dart';
import 'dashed_line_painter.dart';
import 'helper.dart';

class UnBookedAppointmentItem extends StatelessWidget {
  final AppointmentSchedules data;

  const UnBookedAppointmentItem({Key? key, required this.data})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(Dimensions.paddingSizeDefault),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: BorderRadius.circular(Dimensions.radiusDefault),
        boxShadow: [
          BoxShadow(
            offset: const Offset(0, 2),
            blurRadius: 4,
            spreadRadius: 0,
            color: kItemShadowColor,
          ),
        ],
      ),
      child: Column(
        children: [
          Row(
            children: [
              Text(
                data.start.toString().minuteAndSecond,
                style: h1.copyWith(
                  fontWeight: FontWeight.w700,
                ),
              ),
              addHorizontalSpace(Dimensions.paddingSizeDefault),
              Expanded(
                child: SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: CustomPaint(
                    painter: DashedLinePainter(),
                  ),
                ),
              ),
              Icon(
                Icons.lock_open,
                color: mainColor,
                size: 60,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
