import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:table_calendar/table_calendar.dart';

import '../../utils/color.dart';
import '../../utils/dimensions.dart';

class CalendarWidget extends StatelessWidget {
  final DateTime? firstDay;
  final DateTime? lastDay;
  final DateTime focusDay;
  final DateTime selectedDay;
  final List<int> weekendDays;
  final List<DateTime> holidays;
  final CalendarFormat? calendarFormat;
  final Function(DateTime currentDay, DateTime focusDay) onDaySelected;

  const CalendarWidget({
    Key? key,
    this.firstDay,
    this.lastDay,
    required this.focusDay,
    required this.selectedDay,
    required this.weekendDays,
    required this.holidays,
    this.calendarFormat,
    required this.onDaySelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    initializeDateFormatting('fr_FR', null);
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: Dimensions.paddingSizeDefault,
      ),
      child: TableCalendar(
        locale: Get.locale == const Locale('fr', 'FR') ? 'fr_FR' : 'en_US',
        firstDay: firstDay ?? DateTime.now(),
        lastDay: lastDay ?? DateTime(2100),
        focusedDay: focusDay,
        currentDay: selectedDay,
        weekendDays: weekendDays,
        holidayPredicate: (day) {
          return holidays.contains(day);
        },
        startingDayOfWeek: StartingDayOfWeek.monday,
        calendarStyle: CalendarStyle(
          weekendTextStyle: TextStyle(color: kRed),
          todayDecoration: BoxDecoration(
            color: kWhite,
            shape: BoxShape.circle,
            border: Border.all(color: mainColor),
          ),
          todayTextStyle: TextStyle(
            color: mainColor,
            fontWeight: FontWeight.w500,
          ),
          holidayTextStyle: TextStyle(color: kRed),
          holidayDecoration: const BoxDecoration(),
          isTodayHighlighted: true,
        ),
        calendarFormat: calendarFormat ?? CalendarFormat.twoWeeks,
        onDaySelected: (DateTime currentDay, DateTime focusDay) {
          var isWeekendDay = weekendDays.any((element) => element == focusDay.weekday);
          var isHoliday = holidays.any((element) => element == focusDay);
          if (isWeekendDay || isHoliday) {
            /// nothing
          } else{
            onDaySelected(currentDay, focusDay);
          }
        },
        onFormatChanged: (_) {},
      ),
    );
  }
}
