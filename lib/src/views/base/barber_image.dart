import 'package:flutter/material.dart';

import '../../utils/color.dart';
import '../../utils/styles.dart';
import 'helper.dart';
import 'k_profile_image.dart';

class BarberProfileItem extends StatelessWidget {
  final bool isSelected;
  final String imgURL;
  final String name;

  const BarberProfileItem({
    Key? key,
    required this.isSelected,
    required this.imgURL,
    required this.name,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.topRight,
      children: [
        Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            KProfileImage(
              height: 50,
              width: 50,
              imgURL: imgURL,
              borderColor: isSelected ? mainColor : kGreyMedium,
            ),
            addVerticalSpace(5),
            Text(
              name,
              style: h5,
            )
          ],
        ),
        isSelected
            ? Container(
                width: 16,
                height: 16,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: mainColor,
                  shape: BoxShape.circle,
                ),
                child: Icon(
                  Icons.check,
                  color: kWhite,
                  size: 12,
                ),
              )
            : Container()
      ],
    );
  }
}
