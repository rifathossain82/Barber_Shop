import 'package:barber_shop/src/models/common/customer_data_model.dart';
import 'package:barber_shop/src/network/api.dart';
import 'package:barber_shop/src/services/url_launcher_service.dart';
import 'package:barber_shop/src/views/base/k_profile_image.dart';
import 'package:flutter/material.dart';

import '../../utils/color.dart';
import '../../utils/dimensions.dart';
import '../../utils/styles.dart';

class CustomerWidget extends StatelessWidget {
  final CustomerData customerData;
  final bool isSelected;

  const CustomerWidget({
    Key? key,
    required this.customerData,
    this.isSelected = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(Dimensions.paddingSizeDefault),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: BorderRadius.circular(Dimensions.radiusDefault),
        boxShadow: [
          BoxShadow(
            offset: const Offset(0, 2),
            blurRadius: 4,
            spreadRadius: 0,
            color: kItemShadowColor,
          ),
        ],
      ),
      child: ListTile(
        contentPadding: EdgeInsets.zero,
        leading: isSelected
            ? Stack(
                alignment: Alignment.center,
                children: [
                  KProfileImage(
                    height: 50,
                    width: 50,
                    imgURL: Api.getImageURL(customerData.avatar),
                  ),
                  Positioned(
                    child: Container(
                      decoration: BoxDecoration(
                        color: kBlackLight.withOpacity(0.5),
                        shape: BoxShape.circle
                      ),
                      child: Icon(
                        Icons.check,
                        color: kWhite,
                        size: 50,
                      ),
                    ),
                  ),
                ],
              )
            : KProfileImage(
                height: 50,
                width: 50,
                imgURL: Api.getImageURL(customerData.avatar),
              ),
        title: Text(
          customerData.name ?? "",
          style: h1.copyWith(
            fontWeight: FontWeight.w700,
            color: mainColor,
          ),
        ),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              customerData.phone ?? "",
            ),
            Text(
              customerData.email ?? "",
            ),
          ],
        ),
        trailing: IconButton(
          onPressed: () {
            LauncherServices.makeCall(customerData.phone ?? "");
          },
          icon: Icon(Icons.phone, color: mainColor),
        ),
      ),
    );
  }
}
