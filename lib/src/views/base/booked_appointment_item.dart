import 'package:barber_shop/src/models/barbers/barber_appointment_data.dart';
import 'package:barber_shop/src/services/extensions/string_extension.dart';
import 'package:barber_shop/src/utils/app_constants.dart';
import 'package:barber_shop/src/views/base/row_text.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../utils/color.dart';
import '../../utils/dimensions.dart';
import '../../utils/styles.dart';
import 'appointment_status_widget.dart';
import 'dashed_line_painter.dart';
import 'helper.dart';
import 'icon_text.dart';

class BookedAppointmentItem extends StatelessWidget {
  final AppointmentSchedules data;
  final VoidCallback onTap;

  const BookedAppointmentItem({
    Key? key,
    required this.data,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.all(Dimensions.paddingSizeDefault),
        decoration: BoxDecoration(
          color: kWhite,
          borderRadius: BorderRadius.circular(Dimensions.radiusDefault),
          boxShadow: [
            BoxShadow(
              offset: const Offset(0, 2),
              blurRadius: 4,
              spreadRadius: 0,
              color: kItemShadowColor,
            ),
          ],
        ),
        child: Column(
          children: [
            Row(
              children: [
                Text(
                  data.start.toString().minuteAndSecond,
                  style: h1.copyWith(
                    fontWeight: FontWeight.w700,
                  ),
                ),
                addHorizontalSpace(Dimensions.paddingSizeDefault),
                Expanded(
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width,
                    child: CustomPaint(
                      painter: DashedLinePainter(),
                    ),
                  ),
                ),
                Icon(
                  Icons.lock_outline,
                  color: kGreen,
                  size: 60,
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  data.appointment!.customer!.name ?? "",
                  style: h1.copyWith(
                    fontWeight: FontWeight.w700,
                    color: mainColor,
                  ),
                ),
                AppointmentStatusWidget(
                  status: data.appointment!.status ==
                          AppointmentStatus.Confirmed.name
                      ? AppointmentStatus.Confirmed
                      : data.appointment!.status ==
                              AppointmentStatus.Booked.name
                          ? AppointmentStatus.Booked
                          : AppointmentStatus.Canceled,
                ),
              ],
            ),
            addVerticalSpace(Dimensions.paddingSizeDefault),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconText(
                  icon: Icons.calendar_month,
                  text: DateFormat('yyyy-MM-dd')
                      .format(DateTime.parse(data.appointment!.date!)),
                ),
                IconText(
                  icon: Icons.access_time_outlined,
                  text: data.start.toString().minuteAndSecond,
                ),
                RowText(
                  title: AppConstants.currencySymbol,
                  titleStyle: h4.copyWith(
                    color: kBlackLight.withOpacity(0.8),
                  ),
                  value: ' ${data.appointment!.total}',
                  valueStyle: h4.copyWith(
                    color: kBlackLight.withOpacity(0.8),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
