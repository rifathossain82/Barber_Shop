import 'package:barber_shop/src/network/api.dart';
import 'package:barber_shop/src/services/local_storage.dart';
import 'package:barber_shop/src/services/url_launcher_service.dart';
import 'package:barber_shop/src/views/base/helper.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../utils/asset_path.dart';
import '../../utils/color.dart';
import '../../utils/dimensions.dart';
import '../../utils/styles.dart';

class BarberInfo extends StatelessWidget {
  const BarberInfo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    String? bannerImage = LocalStorage.getData(key: LocalStorageKey.shopBanner);
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        Container(
          height: 220,
          width: size.width,
          margin: EdgeInsets.only(
            top: Dimensions.marginSizeDefault,
          ),
          child: Api.getImageURL(bannerImage) == Api.getImageURL('')
              ? Image.asset(
                  AssetPath.placeholder,
                  fit: BoxFit.cover,
                )
              : CachedNetworkImage(
                  imageUrl: Api.getImageURL(
                    bannerImage ?? "",
                  ),
                  fit: BoxFit.cover,
                  placeholder: (context, url) => Image.asset(
                    AssetPath.placeholder,
                    fit: BoxFit.cover,
                  ),
                  errorWidget: (context, url, error) => Image.asset(
                    AssetPath.placeholder,
                    fit: BoxFit.cover,
                  ),
                ),
        ),
        Positioned(
          bottom: 30,
          child: Container(
            width: size.width * 0.94,
            decoration: BoxDecoration(
              color: kWhite,
              borderRadius: BorderRadius.circular(15),
            ),
            child: ListTile(
              leading: CircleAvatar(
                radius: 25,
                backgroundColor: kBlueGrey.withOpacity(0.2),
                child: Image.asset(AssetPath.logo),
              ),
              horizontalTitleGap: 8,
              title: Text(
                ///show shop name
                LocalStorage.getData(key: LocalStorageKey.shopName) ??
                    "Freshup",
                style: h2.copyWith(
                  fontWeight: FontWeight.w700,
                ),
              ),
              subtitle: Text(
                /// show shop location
                LocalStorage.getData(key: LocalStorageKey.shopLocation) ??
                    "56 cours berriat, Grenoble, France.",
                textAlign: TextAlign.start,
                overflow: TextOverflow.ellipsis,
                maxLines: 3,
                style: h5.copyWith(
                  color: kGreyTextColor,
                ),
              ),
              trailing: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  const VerticalDivider(),
                  GestureDetector(
                    onTap: () async {
                      try {
                        await LauncherServices.openMap(
                            45.188233287144186, 5.717313149707014);
                      } catch (e) {
                        kSnackBar(
                            message: 'Failed to open map!',
                            bgColor: failedColor);
                      }
                    },
                    child: Transform.rotate(
                      angle: (110 + 90) % 360,
                      alignment: Alignment.center,
                      child: const Icon(Icons.send),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
