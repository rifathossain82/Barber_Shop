import 'package:barber_shop/src/utils/asset_path.dart';
import 'package:barber_shop/src/utils/color.dart';
import 'package:flutter/material.dart';

import 'helper.dart';

class KCustomLoader extends StatelessWidget {
  const KCustomLoader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: AbsorbPointer(
        child: Center(
          child: SizedBox(
            width: 200.0,
            height: 200.0,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Stack(
                  alignment: Alignment.center,
                  children: [
                    SizedBox(
                      height: 70,
                      width: 70,
                      child: CircularProgressIndicator(
                        color: mainColor,
                      ),
                    ),
                    Positioned(
                      child: CircleAvatar(
                        maxRadius: 30,
                        backgroundColor: Colors.transparent,
                        child: Image.asset(AssetPath.logo),
                      ),
                    ),
                  ],
                ),
                addVerticalSpace(20.0),
                const DefaultTextStyle(
                    style: TextStyle(),
                    child: Text('Please Wait...'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
