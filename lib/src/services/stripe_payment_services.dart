import 'dart:convert';

import 'package:barber_shop/src/controllers/common/auth_controller.dart';
import 'package:barber_shop/src/utils/app_constants.dart';
import 'package:barber_shop/src/utils/color.dart';
import 'package:barber_shop/src/views/base/helper.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter_stripe/flutter_stripe.dart';

class StripePaymentServices {
  Map<String, dynamic> paymentIntent = {};
  bool isSuccess = false;

  Future<String> makePayment({
    required BuildContext context,
    required String amount,
  }) async {
    try {
      paymentIntent = await createPaymentIntent(
        amount,
        AppConstants.stripeCurrency,
      );

      /// create billing details
      final user = Get.find<AuthController>().user.value;
      final billingDetails = BillingDetails(
        name: user.name ?? "",
        email: '',
        phone: user.phone ?? "",
      );

      /// Payment Sheet
      await Stripe.instance
          .initPaymentSheet(
            paymentSheetParameters: SetupPaymentSheetParameters(
              paymentIntentClientSecret: paymentIntent['client_secret'],
              // applePay: const PaymentSheetApplePay(merchantCountryCode: '+998',),
              // googlePay: const PaymentSheetGooglePay(testEnv: true, currencyCode: "UZ", merchantCountryCode: "+998"),
              style: ThemeMode.dark,
              billingDetails: billingDetails,
              merchantDisplayName: AppConstants.appName,
            ),
          )
          .then((value) {});

      await displayPaymentSheet(context);

      /// return transaction id if success
      if (isSuccess) {
        return paymentIntent['id'];
      } else {
        return '';
      }
    } catch (e, s) {
      kPrint('exception:$e$s');
      return '';
    }
  }

  Future displayPaymentSheet(BuildContext context) async {
    try {
      await Stripe.instance.presentPaymentSheet().then((value) {
        /// if success then set status true and show snackBar
        isSuccess = true;
        kSnackBar(message: 'Payment Successful', bgColor: successColor);
      }).onError((error, stackTrace) {
        kPrint('Error is:--->$error $stackTrace');
        kSnackBar(message: 'Payment Failed!', bgColor: failedColor);
      });
    } on StripeException catch (e) {
      /// if failed then print error and show a snackBar
      kPrint('Error is:---> $e');
      kSnackBar(message: 'Payment Cancelled!', bgColor: failedColor);
    } catch (e) {
      kPrint('$e');
      kSnackBar(message: 'Payment Failed!', bgColor: failedColor);
    }
  }

  //  Future<Map<String, dynamic>>
  createPaymentIntent(String amount, String currency) async {
    try {
      Map<String, dynamic> body = {
        'amount': calculateAmount(amount),
        'currency': currency,
        'payment_method_types[]': 'card'
      };

      var response = await http.post(
        Uri.parse('https://api.stripe.com/v1/payment_intents'),
        headers: {
          'Authorization': 'Bearer ${AppConstants.stripeSecretKey}',
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: body,
      );
      // ignore: avoid_print
      print('Payment Intent Body->>> ${response.body.toString()}');
      return jsonDecode(response.body);
    } catch (err) {
      // ignore: avoid_print
      print('err charging user: ${err.toString()}');
    }
  }

  calculateAmount(String amount) {
    final calculatedAmount = (int.parse(amount)) * 100;
    return calculatedAmount.toString();
  }
}
