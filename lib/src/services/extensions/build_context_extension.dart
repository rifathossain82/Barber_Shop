import 'package:flutter/cupertino.dart';

extension BuildContextExtension on BuildContext{

  void unFocusKeyboard() => FocusScope.of(this).unfocus();

}