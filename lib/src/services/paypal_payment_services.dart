import 'dart:developer';

import 'package:barber_shop/src/controllers/common/auth_controller.dart';
import 'package:barber_shop/src/models/common/user_data_model.dart';
import 'package:barber_shop/src/utils/app_constants.dart';
import 'package:barber_shop/src/utils/color.dart';
import 'package:barber_shop/src/views/base/helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_paypal/flutter_paypal.dart';
import 'package:get/get.dart';

class PaypalPaymentServices {
  static makePayment({
    required BuildContext context,
    required num amount,
    required Function(Map response) onSuccess,
  }) async {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (BuildContext context) {
          final UserData user = Get.find<AuthController>().user.value;
          return UsePaypal(
            sandboxMode: true,
            clientId: AppConstants.paypalClientId,
            secretKey: AppConstants.paypalSecretKey,
            returnURL: "https://samplesite.com/return",
            cancelURL: "https://samplesite.com/cancel",
            transactions: [
              {
                "amount": {
                  "total": '$amount',
                  "currency": AppConstants.paypalCurrency,
                  // "details": {
                  //   "subtotal": '10.12',
                  //   "shipping": '0',
                  //   "shipping_discount": 0
                  // }
                },
                "description": "The payment transaction description.",
                // "payment_options": {
                //   "allowed_payment_method":
                //       "INSTANT_FUNDING_SOURCE"
                // },
                "item_list": {
                  "items": [
                    // {
                    //   "name": "A demo product",
                    //   "quantity": 1,
                    //   "price": '10.12',
                    //   "currency": "EUR"
                    // }
                  ],

                  // shipping address is not required though
                  "shipping_address": {
                    "recipient_name": "${user.name}",
                    "line1": "Travis County",
                    "line2": "",
                    "city": "Austin",
                    "country_code": "US",
                    "postal_code": "73301",
                    "phone": "${user.phone}",
                    "state": "Texas"
                  },
                }
              }
            ],
            note: "Contact us for any questions on your order.",
            onSuccess: onSuccess,
            onError: (error) {
              log("onError: $error");
              kSnackBar(
                message: 'Failed to Pay!',
                bgColor: failedColor,
              );
            },
            onCancel: (params) {
              kSnackBar(
                message: 'Cancelled Paypal Payment!',
                bgColor: failedColor,
              );
            },
          );
        },
      ),
    );
  }
}
