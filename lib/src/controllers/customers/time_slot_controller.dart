import 'package:barber_shop/src/models/customers/time_slot_data.dart';
import 'package:get/get.dart';

import '../../network/api.dart';
import '../../network/network_utils.dart';
import '../../utils/color.dart';
import '../../views/base/helper.dart';

class TimeSlotController extends GetxController {
  var isLoading = false.obs;
  var timeSlotData = TimeSlotData(timeSlots: []).obs;

  void getTimeSlotList({required String date, required int barberId}) async {
    try {
      isLoading(true);

      var param = {
        "date" : date,
        "barber_id" : "$barberId"
      };

      dynamic responseBody = await Network.handleResponse(
        await Network.getRequest(api: Api.timeSlotList, params: param),
      );

      if (responseBody != null) {
        timeSlotData.value = TimeSlotData.fromJson(responseBody);
      } else {
        throw 'Unable to load timeSlot list!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }
}
