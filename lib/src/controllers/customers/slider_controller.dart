import 'package:barber_shop/src/models/customers/slider_data_model.dart';
import 'package:get/get.dart';

import '../../network/api.dart';
import '../../network/network_utils.dart';
import '../../utils/color.dart';
import '../../views/base/helper.dart';

class SliderController extends GetxController {
  var isLoading = false.obs;
  var slider = SliderData(sliders: []).obs;

  @override
  void onInit() {
    getSliderData();
    super.onInit();
  }

  void getSliderData() async {
    try {
      isLoading(true);

      dynamic responseBody = await Network.handleResponse(
        await Network.getRequest(api: Api.sliderList),
      );

      if (responseBody != null) {
        slider.value = SliderData.fromJson(responseBody);
      } else {
        throw 'Unable to load slider images!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }
}
