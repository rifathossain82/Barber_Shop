import 'package:barber_shop/src/models/customers/news_data_model.dart';
import 'package:get/get.dart';

import '../../network/api.dart';
import '../../network/network_utils.dart';
import '../../utils/color.dart';
import '../../views/base/helper.dart';

class NewsController extends GetxController {
  var isLoading = false.obs;
  var newsData = NewsData(news: []).obs;

  @override
  void onInit() {
    getPortfolioData();
    super.onInit();
  }

  void getPortfolioData() async {
    try {
      isLoading(true);

      dynamic responseBody = await Network.handleResponse(
        await Network.getRequest(api: Api.newsList),
      );

      if (responseBody != null) {
        newsData.value = NewsData.fromJson(responseBody);
      } else {
        throw 'Unable to load news data!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }
}
