import 'package:barber_shop/src/models/customers/shop_details_data.dart';
import 'package:barber_shop/src/services/local_storage.dart';
import 'package:get/get.dart';

import '../../network/api.dart';
import '../../network/network_utils.dart';
import '../../utils/color.dart';
import '../../views/base/helper.dart';

class ShopDetailsController extends GetxController {
  var isLoading = false.obs;
  var shopDetails = ShopDetailsData(timeSlots: []).obs;

  @override
  void onInit() {
    getShopDetails();
    super.onInit();
  }

  void getShopDetails() async {
    try {
      isLoading(true);

      dynamic responseBody = await Network.handleResponse(
        await Network.getRequest(api: Api.shopDetails),
      );

      if (responseBody != null) {
        shopDetails.value = ShopDetailsData.fromJson(responseBody);

        /// save shop name, location and banner to show banner info widget
        LocalStorage.saveData(
          key: LocalStorageKey.shopName,
          data: shopDetails.value.info!.shopName,
        );
        LocalStorage.saveData(
          key: LocalStorageKey.shopBanner,
          data: shopDetails.value.info!.banner,
        );
        LocalStorage.saveData(
          key: LocalStorageKey.shopLocation,
          data: shopDetails.value.info!.location,
        );

      } else {
        throw 'Unable to load shop details!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }
}
