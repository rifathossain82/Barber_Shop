import 'dart:convert';

import 'package:barber_shop/src/models/customers/appointment_data.dart';
import 'package:barber_shop/src/services/extensions/date_time_extension.dart';
import 'package:get/get.dart';

import '../../network/api.dart';
import '../../network/network_utils.dart';
import '../../utils/color.dart';
import '../../views/base/helper.dart';
import '../../views/screens/customers/appointments_confirmation/appointments_confirmation_screen.dart';

class AppointmentController extends GetxController {
  var isLoading = false.obs;
  var isLoadingMore = false.obs;
  var appointmentList = <AppointmentData>[].obs;
  var oldAppointmentList = <AppointmentData>[].obs;
  var pageNumber = 1.obs;
  var loadedCompleted = false.obs;

  Future<void> getAppointmentList() async {
    try {
      if (pageNumber.value == 1) {
        appointmentList.value = [];
        oldAppointmentList.value = [];
        isLoading(true);
        loadedCompleted(false);
      }

      if (!loadedCompleted.value && pageNumber.value != 1) {
        isLoadingMore(true);
      }

      var param = <String, dynamic>{};
      param['page'] = pageNumber.value.toString();
      param['show'] = "10";

      dynamic responseBody = await Network.handleResponse(
        await Network.getRequest(api: Api.appointmentList, params: param),
      );

      if (responseBody != null) {
        if (responseBody["appointments"]["next_page_url"] == null) {
          loadedCompleted(true);
        } else {
          loadedCompleted(false);
        }

        var data = responseBody['appointments']['data'];
        if (data != null) {
          for (Map<String, dynamic> i in data) {
            /// if appointment date is before from current date
            /// then appointment add on oldAppointmentList other appointmentList
            if (DateTime.parse(i['date']).isBefore(DateTime.now())) {
              oldAppointmentList.add(AppointmentData.fromJson(i));
              oldAppointmentList.refresh();
            } else {
              appointmentList.add(AppointmentData.fromJson(i));
              appointmentList.refresh();
            }
          }
        }
      }
    } finally {
      isLoading(false);
      isLoadingMore(false);
    }
  }

  void bookAppointment({
    required DateTime date,
    required String time,
    required Map<String, dynamic> body,
  }) async {
    try {
      isLoading(true);

      dynamic responseBody = await Network.handleResponse(
        await Network.postRequest(
          api: Api.bookAppointment,
          body: body,
        ),
      );

      if (responseBody != null) {
        kSnackBar(
          message: "Appointment added successfully!",
          bgColor: successColor,
        );

        /// then go to appointment confirmation screen
        Get.to(
          () => AppointmentConfirmationScreen(date: date, time: time),
        );
      } else {
        throw 'Book Appointment Failed!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }

  Future changeAppointmentStatus({
    required int id,
    required String status,
  }) async {
    try {
      isLoading(true);

      var body = {
        "id": "$id",
        "status": status,
      };

      dynamic responseBody = await Network.handleResponse(
        await Network.postRequest(
          api: Api.changeAppointmentStatus,
          body: body,
        ),
      );

      if (responseBody != null) {
        kSnackBar(
          message: responseBody['message'] ?? "Appointment status updated!",
          bgColor: successColor,
        );
      } else {
        throw 'Failed to change appointment status!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }
}
