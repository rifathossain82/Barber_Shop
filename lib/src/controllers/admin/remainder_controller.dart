import 'package:get/get.dart';

import '../../models/admin/remainder_data_model.dart';
import '../../network/api.dart';
import '../../network/network_utils.dart';
import '../../utils/color.dart';
import '../../views/base/helper.dart';

class RemainderController extends GetxController {
  var isLoading = false.obs;
  var phoneNumberList = <String>[].obs;
  var remainderData = RemainderData(appointments: []).obs;

  Future<void> getRemainderDataList({
    required String date,
    required String status,
  }) async {
    try {
      isLoading(true);

      var param = <String, dynamic>{};
      param['date'] = date;
      param['status'] = status;

      dynamic responseBody = await Network.handleResponse(
        await Network.getRequest(api: Api.remainder, params: param),
      );

      if (responseBody != null) {
        remainderData.value = RemainderData.fromJson(responseBody);

        /// clear old list then add all customers phone number
        phoneNumberList.value = [];
        for (var element in remainderData.value.appointments!) {
          phoneNumberList.add(element.customer!.phone ?? "");
        }

      } else {
        throw 'Unable to load Remainder Data!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }
}
