import 'package:barber_shop/src/models/barbers/barber_appointment_data.dart';
import 'package:barber_shop/src/views/screens/barbers/appointments_confirmation/appointments_confrimation_for_barber.dart';
import 'package:get/get.dart';

import '../../network/api.dart';
import '../../network/network_utils.dart';
import '../../utils/color.dart';
import '../../views/base/helper.dart';

class BarberAppointmentController extends GetxController {
  var isLoading = false.obs;
  var appointmentData = BarberAppointmentData(appointmentSchedules: []).obs;

  Future<void> getBarberAppointmentList({String? date}) async {
    try {
      isLoading(true);

      var param = <String, dynamic>{};
      param['date'] = date;

      dynamic responseBody = await Network.handleResponse(
        await Network.getRequest(api: Api.barberAppointmentList, params: param),
      );

      if (responseBody != null) {
        appointmentData.value = BarberAppointmentData.fromJson(responseBody);
      } else {
        throw 'Unable to load barber appointment list!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }

  Future changeBarberAppointmentStatus({
    required int id,
    required String status,
  }) async {
    try {
      isLoading(true);

      var body = {
        "id": "$id",
        "status": status,
      };

      dynamic responseBody = await Network.handleResponse(
        await Network.postRequest(
          api: Api.changeBarberAppointmentStatus,
          body: body,
        ),
      );

      if (responseBody != null) {
        kSnackBar(
          message: responseBody['message'] ?? "Appointment status updated successfully!",
          bgColor: successColor,
        );
      } else {
        throw 'Failed to change appointment status!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }

  void bookAppointmentForBarber({
    required DateTime date,
    required String time,
    required Map<String, dynamic> body,
  }) async {
    try {
      isLoading(true);

      dynamic responseBody = await Network.handleResponse(
        await Network.postRequest(
          api: Api.bookAppointmentForBarber,
          body: body,
        ),
      );

      if (responseBody != null) {
        kSnackBar(
          message: responseBody['message'] ?? "Appointment added successfully!",
          bgColor: successColor,
        );

        /// then go to appointment confirmation screen
        Get.to(
          () => AppointmentConfirmationScreenForBarber(date: date, time: time),
        );
      } else {
        throw 'Book Appointment Failed!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }
}
