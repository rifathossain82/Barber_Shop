import 'package:barber_shop/src/models/common/user_data_model.dart';
import 'package:barber_shop/src/views/screens/common/auth/login_screen.dart';
import 'package:get/get.dart';

import '../../../routes/route.dart';
import '../../network/api.dart';
import '../../network/network_utils.dart';
import '../../services/local_storage.dart';
import '../../utils/color.dart';
import '../../views/base/helper.dart';
import '../../views/screens/common/auth/forgot_password_update_screen.dart';

class AuthController extends GetxController {
  var isLoading = false.obs;
  Rx<UserData> user = UserData().obs;

  Future login({
    required String email,
    required String password,
    required String deviceToken,
  }) async {
    try {
      isLoading(true);

      var map = <String, dynamic>{};
      map['email'] = email;
      map['password'] = password;
      map['device_token'] = deviceToken;

      dynamic responseBody = await Network.handleResponse(
        await Network.postRequest(
          api: Api.login,
          body: map,
        ),
      );

      if (responseBody != null) {
        kSnackBar(message: responseBody['message'], bgColor: successColor);
        user.value = UserData.fromJson(responseBody['user']);
        saveTokenAndUserType(
          token: responseBody['token'],
          userType: getUserTypeValue(user.value.userType!),
        );
        user.value.userType == UserType.customer
            ? Get.offAllNamed(RouteGenerator.customerDashboard)
            : user.value.userType == UserType.barber
                ? Get.offAllNamed(RouteGenerator.barberDashboard)
                : Get.offAllNamed(RouteGenerator.adminDashboard);
      } else {
        throw 'Logged in Failed!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }

  Future register({
    required String name,
    required String email,
    required String phone,
    required String password,
    required String userType,
    required String deviceToken,
  }) async {
    try {
      isLoading(true);

      var map = <String, dynamic>{};
      map['name'] = name;
      map['email'] = email;
      map['phone'] = phone;
      map['password'] = password;
      map['user_type'] = userType;
      map['device_token'] = deviceToken;

      dynamic responseBody = await Network.handleResponse(
        await Network.postRequest(
          api: Api.register,
          body: map,
        ),
      );

      if (responseBody != null) {
        kSnackBar(message: responseBody['message'], bgColor: successColor);
        user.value = UserData.fromJson(responseBody['user']);
        saveTokenAndUserType(
          token: responseBody['token'],
          userType: getUserTypeValue(user.value.userType!),
        );
        user.value.userType == UserType.customer
            ? Get.offAllNamed(RouteGenerator.customerDashboard)
            : user.value.userType == UserType.barber
                ? Get.offAllNamed(RouteGenerator.barberDashboard)
                : Get.offAllNamed(RouteGenerator.adminDashboard);
      } else {
        throw 'Registration Failed!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }

  Future logout() async {
    try {
      isLoading(true);

      dynamic responseBody = await Network.handleResponse(
        await Network.postRequest(
          api: Api.logout,
        ),
      );

      if (responseBody != null) {
        LocalStorage.removeData(key: LocalStorageKey.token);
        LocalStorage.removeData(key: LocalStorageKey.userType);
        kSnackBar(message: responseBody['message'], bgColor: successColor);
        Get.offAll(() => const LoginScreen());
      } else {
        kSnackBar(message: 'Logout Failed!', bgColor: failedColor);
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }

  void saveTokenAndUserType({required String token, required String userType}) {
    LocalStorage.saveData(
      key: LocalStorageKey.token,
      data: token,
    );
    LocalStorage.saveData(
      key: LocalStorageKey.userType,
      data: userType,
    );
  }

  Future changePassword({
    required String oldPassword,
    required String newPassword,
    required String confirmPassword,
  }) async {
    try {
      isLoading(true);

      var map = <String, dynamic>{};
      map['old_password'] = oldPassword;
      map['password'] = newPassword;
      map['password_confirmation'] = confirmPassword;

      dynamic responseBody = await Network.handleResponse(
        await Network.postRequest(
          api: Api.changePassword,
          body: map,
        ),
      );

      if (responseBody != null) {
        LocalStorage.removeData(key: LocalStorageKey.token);
        LocalStorage.removeData(key: LocalStorageKey.userType);
        kSnackBar(
          message: 'Password updated successfully.',
          bgColor: successColor,
        );
        Get.offAll(() => const LoginScreen());
      } else {
        kSnackBar(message: 'Change Password Failed!', bgColor: failedColor);
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }

  Future getUserData() async {
    try {
      // isLoading(true);

      dynamic responseBody = await Network.handleResponse(
        await Network.getRequest(api: Api.getUser),
      );

      if (responseBody != null) {
        user.value = UserData.fromJson(responseBody['user']);
      } else {
        throw 'Failed To Load User!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }

  Future updateUserData({
    String? name,
    String? phone,
    String? filepath,
  }) async {
    try {
      // isLoading(true);

      var body = <String, String>{};
      body['name'] = name ?? "";
      body['phone'] = phone ?? "";

      dynamic responseBody = await Network.handleResponse(
        await Network.multipartAddRequest(
          api: Api.updateUserProfile,
          body: body,
          fileKeyName: 'avatar',
          filePath: filepath ?? "",
        ),
      );

      if (responseBody != null) {
        user.value = UserData.fromJson(responseBody['user']);
      } else {
        throw 'Failed To update user profile!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }

  Future forgotPassword({required String email}) async {
    try {
      isLoading(true);

      var body = <String, String>{};
      body['email'] = email;

      dynamic responseBody = await Network.handleResponse(
        await Network.postRequest(api: Api.forgotPassword, body: body),
      );

      if (responseBody != null) {
        kSnackBar(message: responseBody['message'], bgColor: successColor);
        Get.to(() => ForgotPasswordUpdateScreen(email: email));
      } else {
        throw 'Failed to send OTP!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }

  Future forgotPasswordUpdate({
    required String otp,
    required String email,
    required String password,
    required String confirmPassword,
  }) async {
    try {
      isLoading(true);

      var body = <String, String>{};
      body['otp'] = otp;
      body['email'] = email;
      body['password'] = password;
      body['password_confirmation'] = confirmPassword;

      dynamic responseBody = await Network.handleResponse(
        await Network.postRequest(api: Api.forgotPasswordUpdate, body: body),
      );

      if (responseBody != null) {
        kSnackBar(
            message: "Password Update Successfully!", bgColor: successColor);
        Get.offAll(() => const LoginScreen());
      } else {
        throw 'Failed to update password!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }
}
