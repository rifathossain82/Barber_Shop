import 'package:barber_shop/src/models/common/country_code_data.dart';
import 'package:get/get.dart';

import '../../network/api.dart';
import '../../network/network_utils.dart';
import '../../utils/color.dart';
import '../../views/base/helper.dart';

class CountryCodeController extends GetxController {
  var isLoading = false.obs;
  var countryCodeData = CountryCodeData(countries: []).obs;

  Future<void> getCountryCodeList({String? countryName}) async {
    try {
      isLoading(true);

      var param = <String, dynamic>{};
      param['search'] = countryName;

      dynamic responseBody = await Network.handleResponse(
        await Network.getRequest(api: Api.getCountryCodeList, params: param),
      );

      if (responseBody != null) {
        countryCodeData.value = CountryCodeData.fromJson(responseBody);
      } else {
        throw 'Unable to load country code!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }
}
