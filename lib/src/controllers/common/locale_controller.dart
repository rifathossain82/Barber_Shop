import 'package:barber_shop/localization/app_localization.dart';
import 'package:barber_shop/src/models/common/locale_data.dart';
import 'package:barber_shop/src/services/local_storage.dart';
import 'package:barber_shop/src/views/base/helper.dart';
import 'package:get/get.dart';

class LocaleController extends GetxController {
  var selectedLocaleData = AppLocalization.locals.first.obs;

  @override
  void onInit() {
    setInitialLocale();
    super.onInit();
  }

  setInitialLocale() {
    var languageCode = LocalStorage.getData(
      key: LocalStorageKey.localeLanguageCode,
    );

    if (languageCode != null) {
      for (var localData in AppLocalization.locals) {
        if(localData.locale.languageCode == languageCode){
          selectedLocaleData.value = localData;
        }
      }
    }
  }

  updateLocale(LocaleData localeData) {
    selectedLocaleData.value = localeData;
    Get.updateLocale(selectedLocaleData.value.locale);

    /// to save in local storage
    LocalStorage.saveData(
      key: LocalStorageKey.localeLanguageCode,
      data: selectedLocaleData.value.locale.languageCode,
    );
  }
}
