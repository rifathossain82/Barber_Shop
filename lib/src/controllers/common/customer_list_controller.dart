import 'package:barber_shop/src/models/common/customer_data_model.dart';
import 'package:barber_shop/src/utils/color.dart';
import 'package:barber_shop/src/views/base/helper.dart';
import 'package:get/get.dart';

import '../../network/api.dart';
import '../../network/network_utils.dart';

class CustomerListController extends GetxController {
  /// to show all customer for admin
  var customerList = <CustomerData>[].obs;
  var customersPhoneList = <String>[].obs;

  /// to load customer in dropdown, to book appointment for admin and barber
  /// only id and name are available
  var customerListLite = <CustomerData>[].obs;

  var isLoading = false.obs;
  var pageNumber = 1.obs;
  var loadedCompleted = false.obs;

  Future<void> getCustomerList({
    String? searchText,
    String? show,
    String? doesNotBookStatus,
    String? start,
    String? end,
  }) async {
    try {
      if (pageNumber.value == 1) {
        customerList.value = [];
        customersPhoneList.value = [];
        isLoading(true);
        loadedCompleted(false);
      }
      if (searchText != null) {
        pageNumber.value = 1;
        customerList.value = [];
        customersPhoneList.value = [];
        isLoading(true);
        loadedCompleted(false);
      }

      var param = <String, dynamic>{};
      param['page'] = pageNumber.value.toString();
      param['show'] = show ?? '10';

      dynamic responseBody = await Network.handleResponse(
        await Network.getRequest(api: Api.customerList, params: param),
      );

      if (responseBody != null) {
        if (responseBody["customers"]["next_page_url"] == null) {
          loadedCompleted(true);
        } else {
          loadedCompleted(false);
        }

        var data = responseBody['customers']['data'];
        if (data != null) {
          for (Map<String, dynamic> i in data) {
            customerList.add(CustomerData.fromJson(i));
            customerList.refresh();

            /// store phone number
            customersPhoneList.add(CustomerData.fromJson(i).phone ?? "");
            customersPhoneList.refresh();
          }
        }
      }
    } finally {
      isLoading(false);
    }
  }

  Future<void> filterCustomerList({
    required String doesNotBookStatus,
    required String startIndex,
    required String endIndex,
    String? searchText,
  }) async {
    try {
      /// clear customer list and customerPhoneList
      customerList.value = [];
      customersPhoneList.value = [];


      isLoading(true);

      var param = <String, dynamic>{};
      param['search'] = searchText ?? "";
      param['doesnt_book_status'] = doesNotBookStatus.toLowerCase();
      param['start'] = startIndex;
      param['end'] = endIndex;

      dynamic responseBody = await Network.handleResponse(
        await Network.getRequest(api: Api.customerList, params: param),
      );

      var data = responseBody['customers']['data'];
      if (data != null) {
        for (Map<String, dynamic> i in data) {
          customerList.add(CustomerData.fromJson(i));
          customerList.refresh();

          /// store phone number
          customersPhoneList.add(CustomerData.fromJson(i).phone ?? "");
          customersPhoneList.refresh();
        }
      }
    } finally {
      isLoading(false);
    }
  }

  Future<void> getCustomerListLite({String? searchText}) async {
    try {
      isLoading(true);

      var param = <String, dynamic>{};
      param['search'] = searchText ?? "";

      dynamic responseBody = await Network.handleResponse(
        await Network.getRequest(api: Api.customerListLite, params: param),
      );

      if (responseBody != null) {
        var data = responseBody['customers'];
        if (data != null) {
          for (Map<String, dynamic> i in data) {
            customerListLite.add(CustomerData.fromJson(i));
            customerListLite.refresh();
          }
        }
      }
    } finally {
      isLoading(false);
    }
  }

  Future<void> addNewCustomer({required String name}) async {
    try {
      var body = {
        "name": name,
      };

      dynamic responseBody = await Network.handleResponse(
        await Network.postRequest(
          api: Api.addCustomer,
          body: body,
        ),
      );

      if (responseBody != null) {
        kSnackBar(
          message: "Customer added successfully!",
          bgColor: successColor,
        );
      } else {
        throw 'Customer Appointment Failed!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    }
  }
}
