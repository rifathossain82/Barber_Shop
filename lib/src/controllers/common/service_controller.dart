import 'package:barber_shop/src/models/common/service_data_model.dart';
import 'package:get/get.dart';

import '../../network/api.dart';
import '../../network/network_utils.dart';

class ServiceController extends GetxController {
  var serviceList = <ServiceData>[].obs;
  var isLoading = false.obs;
  var pageNumber = 1.obs;
  var loadedCompleted = false.obs;

  Future<void> getServiceList({String? serviceName}) async {
    try {
      if (pageNumber.value == 1) {
        serviceList.value = [];
        isLoading(true);
        loadedCompleted(false);
      }
      if(serviceName != null){
        pageNumber.value = 1;
        serviceList.value = [];
        isLoading(true);
        loadedCompleted(false);
      }

      var param = <String, dynamic>{};
      param['page'] = pageNumber.value.toString();
      param['show'] = "10";
      param['search'] = serviceName ?? "";

      dynamic responseBody = await Network.handleResponse(
        await Network.getRequest(api: Api.serviceList, params: param),
      );

      if (responseBody != null) {
        if (responseBody["services"]["next_page_url"] == null) {
          loadedCompleted(true);
        } else {
          loadedCompleted(false);
        }

        var data = responseBody['services']['data'];
        if (data != null) {
          for (Map<String, dynamic> i in data) {
            serviceList.add(ServiceData.fromJson(i));
            serviceList.refresh();
          }
        }
      }
    } finally {
      isLoading(false);
    }
  }
}
