import 'package:barber_shop/src/models/common/holidays_data.dart';
import 'package:get/get.dart';

import '../../network/api.dart';
import '../../network/network_utils.dart';
import '../../utils/color.dart';
import '../../views/base/helper.dart';

class HolidayController extends GetxController {
  var isLoading = false.obs;
  var holidays = HolidaysData(weeklyHoliday: [], publicHolidays: []).obs;

  Future<void> getHolidayList() async {
    try {
      isLoading(true);

      dynamic responseBody = await Network.handleResponse(
        await Network.getRequest(api: Api.holidays),
      );

      if (responseBody != null) {
        holidays.value = HolidaysData.fromJson(responseBody);
      } else {
        throw 'Unable to load holidays!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }
}
