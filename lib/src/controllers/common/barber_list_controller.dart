import 'package:barber_shop/src/models/common/barber_data.dart';
import 'package:barber_shop/src/services/extensions/date_time_extension.dart';
import 'package:get/get.dart';

import '../../network/api.dart';
import '../../network/network_utils.dart';
import '../../utils/color.dart';
import '../../views/base/helper.dart';

class BarberListController extends GetxController {
  var isLoading = false.obs;
  var barberData = BarberData(barbers: []).obs;

  @override
  void onInit() {
    getBarberList(date: DateTime.now().yyyyMMddWithSlash);
    super.onInit();
  }

  Future<void> getBarberList({String? date}) async {
    try {
      isLoading(true);

      var param = {
        "date" : date,
      };

      dynamic responseBody = await Network.handleResponse(
        await Network.getRequest(api: Api.barberList, params: param),
      );

      if (responseBody != null) {
        barberData.value = BarberData.fromJson(responseBody);
      } else {
        throw 'Unable to load barber list!';
      }
    } catch (e) {
      kSnackBar(message: e.toString(), bgColor: failedColor);
    } finally {
      isLoading(false);
    }
  }
}
