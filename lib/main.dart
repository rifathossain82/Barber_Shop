import 'dart:io';

import 'package:barber_shop/routes/route.dart';
import 'package:barber_shop/src/controllers/common/locale_controller.dart';
import 'package:barber_shop/src/services/firebase_push_notification_services.dart';
import 'package:barber_shop/src/utils/app_constants.dart';
import 'package:barber_shop/src/utils/app_theme.dart';
import 'package:barber_shop/src/views/base/k_scroll_behavior.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import 'localization/app_localization.dart';
import 'src/services/local_notification_services.dart';
import 'src/views/base/helper.dart';

Future<void> backgroundHandler(RemoteMessage message) async {
  kPrint(message.data.toString());
  kPrint(message.notification!.title);
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  HttpOverrides.global = MyHttpOverrides();

  /// initial stripe
  Stripe.publishableKey = AppConstants.stripePublishableKey;
  Stripe.merchantIdentifier = 'App Identifier';
  Stripe.urlScheme = 'flutterstripe';
  await Stripe.instance.applySettings();

  await GetStorage.init();
  await Firebase.initializeApp();
  LocalNotificationService.initialize();

  FirebaseMessaging.onBackgroundMessage(backgroundHandler);
  FirebasePushNotificationServices().handleNotification();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);
  final localController = Get.put(LocaleController());

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return GetMaterialApp(
        builder: (context, child) {
          return ScrollConfiguration(
            behavior: KScrollBehavior(),
            child: child!,
          );
        },
        debugShowCheckedModeBanner: false,
        title: AppConstants.appName,
        initialRoute: RouteGenerator.splash,
        getPages: RouteGenerator.routes,
        theme: AppThemeData.lightTheme,

        /// localization
        translations: AppLocalization(),
        locale: localController.selectedLocaleData.value.locale,
        fallbackLocale: AppLocalization.locals.first.locale,
      );
    });
  }
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}
