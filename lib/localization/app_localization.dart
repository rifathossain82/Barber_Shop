import 'dart:ui';

import 'package:barber_shop/localization/language/english.dart';
import 'package:barber_shop/localization/language/france.dart';
import 'package:barber_shop/src/models/common/locale_data.dart';
import 'package:barber_shop/src/utils/asset_path.dart';
import 'package:get/get.dart';

class AppLocalization extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
        "fr_FR": franceLanguage,
        "en_US": englishLanguage,
      };

  static final locals = [
    LocaleData(
      locale: const Locale('fr', 'FR'),
      languageName: 'French',
      countryName: 'France',
      languageCode: 'FR',
      flag: AssetPath.franceFlag,
    ),
    LocaleData(
      locale: const Locale('en', 'US'),
      languageName: 'English',
      countryName: 'United English',
      languageCode: 'EN',
      flag: AssetPath.usaFlag,
    ),
  ];
}
