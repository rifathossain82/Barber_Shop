import 'package:barber_shop/src/views/screens/admin/dashboard/admin_dashboard_screen.dart';
import 'package:barber_shop/src/views/screens/common/auth/change_password_screen.dart';
import 'package:get/get.dart';

import '../src/views/screens/barbers/dashboard/barber_dashboard_screen.dart';
import '../src/views/screens/common/auth/register_screen.dart';
import '../src/views/screens/common/splash/splash_screen.dart';
import '../src/views/screens/customers/dashboard/customer_dashboard_screen.dart';


class RouteGenerator {

  /// common
  static const String splash = '/';
  static const String register = '/register';
  static const String changePassword = '/changePassword';

  /// barbers
  static const String barberDashboard = '/barber-dashboard';

  /// customers
  static const String customerDashboard = '/customer-dashboard';

  /// admin
  static const String adminDashboard = '/admin-dashboard';

  static final routes=[

    /// common
    GetPage(name: RouteGenerator.splash, page: ()=> const SplashScreen()),
    GetPage(name: RouteGenerator.register, page: ()=> const RegisterScreen()),
    GetPage(name: RouteGenerator.changePassword, page: ()=> const ChangePasswordScreen()),

    /// barbers
    GetPage(name: RouteGenerator.barberDashboard, page: ()=> const BarberDashboardScreen()),

    /// customers
    GetPage(name: RouteGenerator.customerDashboard, page: ()=> CustomerDashboardScreen()),

    /// admin
    GetPage(name: RouteGenerator.adminDashboard, page: ()=> const AdminDashboardScreen()),
  ];

}